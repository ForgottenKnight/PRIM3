﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboIcon : MonoBehaviour {
	public Text UIhit;
	public Text UItext;

	public void SetHit(string aHit) {
		UIhit.text = aHit;
	}
	public void SetText(string aText) {
		UItext.text = aText;
	}
	public void SetColor(Color aColor) {
		UIhit.color = aColor;
		UItext.color = aColor;
	}
}

