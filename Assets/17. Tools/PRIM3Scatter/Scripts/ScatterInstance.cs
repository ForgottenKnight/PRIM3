﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterInstance : MonoBehaviour {
	[HideInInspector]
	public List<Vector4> m_PointsList;

	public ScatterPointsData m_ScatterPointsData;
	private ScatterPointsData m_ScatterPointsTemp;

	public Material baseMaterial;
	private Material m_Material;
	public Mesh mesh;
	public string layer = "Grass";
	public UnityEngine.Rendering.ShadowCastingMode shadowType = UnityEngine.Rendering.ShadowCastingMode.On;
	public Vector3 offset;
	[Range(0f, 10f)]
	public float meshSize = 0.15f;
	public Vector3 boundsSize = new Vector3 (100.0f, 100.0f, 100.0f);
	private Bounds meshBounds;
	private ComputeBuffer m_PositionBuffer;
	private ComputeBuffer m_ArgsBuffer;
	private ComputeBuffer m_ColorBuffer;
	private uint[] m_Args = new uint[5] { 0, 0, 0, 0, 0 };
	private int cachedInstanceCount = -1;
	MaterialPropertyBlock m_MPB;

	private bool pointsEnabled = false;
	private Vector3 lastPosition;

	public bool disableOnMove = false;
	public bool updatable = false;

	int index = 0;

	void Start() {
		if (m_ScatterPointsData != null) {
			lastPosition = transform.position;

			/*m_Material = new Material(baseMaterial);
			m_Material.shader = (Shader)Instantiate (baseMaterial.shader);
			meshBounds = new Bounds (transform.position, boundsSize);
			m_PointsList = m_ScatterPointsData.pointsList;
			m_ArgsBuffer = new ComputeBuffer (1, m_Args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
			UpdateBuffers();
			m_MPB = new MaterialPropertyBlock ();
			m_MPB.SetFloat ("_ASD", 0f); // Propiedad dummy. Con esto se fuerza a hacer una llamada diferente por cada material, arreglando el problema de las sombras.*/
			//DoEnable ();
		}
	}

	void Update() {
		/*if (m_PointsList != null) {
			if (m_ScatterPointsData != null) {
				if (layer != "") {
					Graphics.DrawMeshInstancedIndirect (mesh, 0, m_Material, meshBounds, m_ArgsBuffer, 0, m_MPB, shadowType, true, LayerMask.NameToLayer (layer));
				} else {
					Graphics.DrawMeshInstancedIndirect (mesh, 0, m_Material, meshBounds, m_ArgsBuffer, 0, m_MPB, shadowType);
				}
			}
		}*/
		if (pointsEnabled) {
			if (lastPosition != transform.position) {
				if (disableOnMove) {
					DisablePoints ();
				} else if (updatable) {
					Vector3 delta = transform.position - lastPosition;
					UpdatePosition (delta);
					lastPosition = transform.position;
				}
			}
		}
	}

	void UpdatePosition(Vector3 delta) {
		for (int i = 0; i < m_ScatterPointsTemp.pointsList.Count; ++i) {
			Vector4 temp = m_ScatterPointsTemp.pointsList [i];
			temp.x += delta.x;
			temp.y += delta.y;
			temp.z += delta.z;
			m_ScatterPointsTemp.pointsList [i] = temp;
			UpdatableScatterRenderer.instance.UpdatePoints (index, m_ScatterPointsTemp);
		}
		UpdatableScatterRenderer.instance.PostUpdateBuffers (index, m_ScatterPointsData.pointsList.Count);
	}

	void OnDisable() {
		DoDestroy ();
	}

	void OnDestroy() {
		DoDestroy ();
	}

	void OnEnable() {
		DoEnable ();
	}

	public void DisablePoints() {
		DoDestroy ();
	}

	void DoEnable() {
		if (m_ScatterPointsData != null && !pointsEnabled) {
			pointsEnabled = true;
			if (!updatable) {
				if (ScatterRenderer.instance != null && ScatterRenderer.instance.pointsData == null) {
					ScatterRenderer.instance.pointsData = new List<ScatterPointsData> ();
				}
			} else {
				if (UpdatableScatterRenderer.instance != null && UpdatableScatterRenderer.instance.pointsData == null) {
					UpdatableScatterRenderer.instance.pointsData = new List<ScatterPointsData> ();
				}
			}
			if (m_ScatterPointsTemp == null) {
				m_ScatterPointsTemp = ScriptableObject.CreateInstance<ScatterPointsData> ();
				m_ScatterPointsTemp.pointsList = new List<Vector4> (m_ScatterPointsData.pointsList);
			}

			if (!updatable) {
				if (ScatterRenderer.instance != null) {
					ScatterRenderer.instance.pointsData.Add (m_ScatterPointsTemp);
				}
			} else {
				if (UpdatableScatterRenderer.instance != null) {
					index = UpdatableScatterRenderer.instance.AddPoints (m_ScatterPointsData);
					//UpdatableScatterRenderer.instance.pointsData.Add (m_ScatterPointsTemp);
				}
			}
		}
	}

	void DoDestroy() {
		if (!updatable) {
			if (ScatterRenderer.instance != null && ScatterRenderer.instance.pointsData != null && pointsEnabled) {
				pointsEnabled = false;
				ScatterRenderer.instance.pointsData.Remove (m_ScatterPointsTemp);

				if (m_PositionBuffer != null)
					m_PositionBuffer.Release ();
				m_PositionBuffer = null;

				if (m_ArgsBuffer != null)
					m_ArgsBuffer.Release ();
				m_ArgsBuffer = null;
			}
		} else {
			if (UpdatableScatterRenderer.instance != null && UpdatableScatterRenderer.instance.pointsData != null && pointsEnabled) {
				pointsEnabled = false;
				UpdatableScatterRenderer.instance.pointsData.Remove (m_ScatterPointsTemp);

				if (m_PositionBuffer != null)
					m_PositionBuffer.Release ();
				m_PositionBuffer = null;

				if (m_ArgsBuffer != null)
					m_ArgsBuffer.Release ();
				m_ArgsBuffer = null;
			}
		}
	}

	void UpdateBuffers() {
		// positions
		if (m_PositionBuffer != null)
			m_PositionBuffer.Release();
		m_PositionBuffer = new ComputeBuffer(m_PointsList.Count, 16);
		m_ColorBuffer = new ComputeBuffer (m_PointsList.Count, 4);
		float l_MeshHeightHalf = (mesh.bounds.max.y - mesh.bounds.min.y) / 2.0f;
		Vector4[] positions = new Vector4[m_PointsList.Count];
		float[] colors = new float[m_PointsList.Count];
		for (int i = 0; i < m_PointsList.Count; i++) {
			positions [i].w = meshSize * m_PointsList[i].w;
			positions [i].x = m_PointsList [i].x + offset.x;
			positions [i].y = m_PointsList [i].y + offset.y + positions[i].w * l_MeshHeightHalf;
			positions [i].z = m_PointsList [i].z + offset.z;
			colors [i] = Random.Range	(0f, 1f);
		}
		m_PositionBuffer.SetData(positions);
		m_ColorBuffer.SetData (colors);
		m_Material.SetBuffer("positionBuffer", m_PositionBuffer);
		m_Material.SetBuffer ("colorBuffer", m_ColorBuffer);

		// indirect args
		uint numIndices = (mesh != null) ? (uint)mesh.GetIndexCount(0) : 0;
		m_Args[0] = numIndices;
		m_Args[1] = (uint)m_PointsList.Count;
		m_ArgsBuffer.SetData(m_Args);

		cachedInstanceCount = m_PointsList.Count;
	}
}
