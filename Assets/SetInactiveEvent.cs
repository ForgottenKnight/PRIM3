﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInactiveEvent : MonoBehaviour {

	public void SetInactive() {
		gameObject.SetActive(false);
	}
}
