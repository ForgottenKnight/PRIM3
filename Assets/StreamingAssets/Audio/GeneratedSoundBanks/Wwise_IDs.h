/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTIVATEMARK = 2667218305U;
        static const AkUniqueID BOOKDIES = 2497328087U;
        static const AkUniqueID BOOKEXPLOSION = 1599291647U;
        static const AkUniqueID BOOKFLAPPING = 3476413527U;
        static const AkUniqueID BOOKHURT = 2286796421U;
        static const AkUniqueID BOOKICECAST = 1849491478U;
        static const AkUniqueID BOOKICEIMPACT = 3537399153U;
        static const AkUniqueID BOOKLIGHTNING = 3892048616U;
        static const AkUniqueID BOOKSTOPFLAPPING = 1177853695U;
        static const AkUniqueID BOOKTHUNDERAREA = 2115812019U;
        static const AkUniqueID BRIDGEBREAKS = 2028612630U;
        static const AkUniqueID BRIDGEFALLS = 681005512U;
        static const AkUniqueID CENTRALMARK = 3332977535U;
        static const AkUniqueID COMBATMUSIC = 3733692670U;
        static const AkUniqueID ENDJUMP = 3212140712U;
        static const AkUniqueID FALLINGTREE = 144218938U;
        static const AkUniqueID FEATHERS = 3612300133U;
        static const AkUniqueID G_E1_1 = 3528695477U;
        static const AkUniqueID G_P0_1 = 3821530263U;
        static const AkUniqueID G_P0_2 = 3821530260U;
        static const AkUniqueID G_P0_3 = 3821530261U;
        static const AkUniqueID G_P0_4 = 3821530258U;
        static const AkUniqueID G_P0_5 = 3821530259U;
        static const AkUniqueID G_P0_6 = 3821530256U;
        static const AkUniqueID G_P0_7 = 3821530257U;
        static const AkUniqueID G_P1_1 = 3821383230U;
        static const AkUniqueID G_P1_2 = 3821383229U;
        static const AkUniqueID G_P1_3 = 3821383228U;
        static const AkUniqueID G_P1_4 = 3821383227U;
        static const AkUniqueID G_P1_5 = 3821383226U;
        static const AkUniqueID G_P1_6 = 3821383225U;
        static const AkUniqueID G_P1_7 = 3821383224U;
        static const AkUniqueID G_P2_1 = 600036361U;
        static const AkUniqueID G_P2_2 = 600036362U;
        static const AkUniqueID G_P2_3 = 600036363U;
        static const AkUniqueID G_P2_4 = 600036364U;
        static const AkUniqueID G_P2_5 = 600036365U;
        static const AkUniqueID G_P2_6 = 600036366U;
        static const AkUniqueID G_P2_7 = 600036367U;
        static const AkUniqueID GOLEMATTACK = 4272317039U;
        static const AkUniqueID GOLEMCHARGES = 2178908738U;
        static const AkUniqueID GOLEMDIES = 2100688098U;
        static const AkUniqueID GOLEMENDSCHARGE = 1744892281U;
        static const AkUniqueID GOLEMGRABSSTONE = 3605283467U;
        static const AkUniqueID GOLEMHURT = 2573158568U;
        static const AkUniqueID GOLEMQUAKE = 417214282U;
        static const AkUniqueID GOLEMROCKBREAKS = 2490432288U;
        static const AkUniqueID GOLEMSTAND = 2940453745U;
        static const AkUniqueID GOLEMSTOPCHARGE = 1020139275U;
        static const AkUniqueID GOLEMTHROWSSTONE = 2402958267U;
        static const AkUniqueID GOLEMWALKS = 1915762083U;
        static const AkUniqueID L0_D1 = 3728481577U;
        static const AkUniqueID L0_D2 = 3728481578U;
        static const AkUniqueID L0_D3 = 3728481579U;
        static const AkUniqueID L1_D1_1 = 488746010U;
        static const AkUniqueID L1_D1_2 = 488746009U;
        static const AkUniqueID L1_D1_3 = 488746008U;
        static const AkUniqueID L1_D1_4 = 488746015U;
        static const AkUniqueID L1_D1_5 = 488746014U;
        static const AkUniqueID L1_D1_6 = 488746013U;
        static const AkUniqueID L1_D1_7 = 488746012U;
        static const AkUniqueID L1_D1_8 = 488746003U;
        static const AkUniqueID L1_D1_9 = 488746002U;
        static const AkUniqueID L1_D2_1 = 488598949U;
        static const AkUniqueID L1_D2_2 = 488598950U;
        static const AkUniqueID L1_D2_3 = 488598951U;
        static const AkUniqueID L1_D2_4 = 488598944U;
        static const AkUniqueID L1_D3_1 = 1562219404U;
        static const AkUniqueID L1_D3_2 = 1562219407U;
        static const AkUniqueID L1_D3_3 = 1562219406U;
        static const AkUniqueID L1_D3_4 = 1562219401U;
        static const AkUniqueID L1_D3_5 = 1562219400U;
        static const AkUniqueID L1_D3_6 = 1562219403U;
        static const AkUniqueID L1_D3_7 = 1562219402U;
        static const AkUniqueID L1_D4_1 = 1563352303U;
        static const AkUniqueID L1_D4_2 = 1563352300U;
        static const AkUniqueID L1_D4_3 = 1563352301U;
        static const AkUniqueID L1_D4_4 = 1563352298U;
        static const AkUniqueID L1_D4_5 = 1563352299U;
        static const AkUniqueID L1_D5_1 = 1563205238U;
        static const AkUniqueID L1_D5_2 = 1563205237U;
        static const AkUniqueID L1_D5_3 = 1563205236U;
        static const AkUniqueID L1_D5_4 = 1563205235U;
        static const AkUniqueID L1_D5_5 = 1563205234U;
        static const AkUniqueID L1_D5_6 = 1563205233U;
        static const AkUniqueID L1_D5_7 = 1563205232U;
        static const AkUniqueID L1_E1_1 = 4035921013U;
        static const AkUniqueID L1_E1_2 = 4035921014U;
        static const AkUniqueID L1_E2_1 = 2962403626U;
        static const AkUniqueID L1_E2_2 = 2962403625U;
        static const AkUniqueID L1_E3_1 = 2962550659U;
        static const AkUniqueID L1_E3_2 = 2962550656U;
        static const AkUniqueID L1_E3_3 = 2962550657U;
        static const AkUniqueID L1_E4_1 = 4036465464U;
        static const AkUniqueID L1_E4_2 = 4036465467U;
        static const AkUniqueID L1_E4_3 = 4036465466U;
        static const AkUniqueID L1_E4_4 = 4036465469U;
        static const AkUniqueID L1_E5_1 = 4036612497U;
        static const AkUniqueID L1_E5_2 = 4036612498U;
        static const AkUniqueID L1_E5_3 = 4036612499U;
        static const AkUniqueID L1_E5_4 = 4036612500U;
        static const AkUniqueID L1_E6_1 = 2962991942U;
        static const AkUniqueID L1_E7_1 = 4036906751U;
        static const AkUniqueID L1_E7_2 = 4036906748U;
        static const AkUniqueID L1_E7_3 = 4036906749U;
        static const AkUniqueID L1_E8_1 = 4037053780U;
        static const AkUniqueID L1_E8_2 = 4037053783U;
        static const AkUniqueID L1_E8_3 = 4037053782U;
        static const AkUniqueID L1_E9_1 = 2963536397U;
        static const AkUniqueID L2_D1_1 = 3192323931U;
        static const AkUniqueID L2_D1_2 = 3192323928U;
        static const AkUniqueID L2_D1_3 = 3192323929U;
        static const AkUniqueID L2_D2_1 = 3191882644U;
        static const AkUniqueID L2_D2_2 = 3191882647U;
        static const AkUniqueID L2_D2_3 = 3191882646U;
        static const AkUniqueID L2_D2_4 = 3191882641U;
        static const AkUniqueID L2_D3_1 = 2118262093U;
        static const AkUniqueID L2_D3_2 = 2118262094U;
        static const AkUniqueID L2_D3_3 = 2118262095U;
        static const AkUniqueID L2_D3_4 = 2118262088U;
        static const AkUniqueID L2_D3_5 = 2118262089U;
        static const AkUniqueID L2_D3_6 = 2118262090U;
        static const AkUniqueID L2_D3_7 = 2118262091U;
        static const AkUniqueID L2_D3_8 = 2118262084U;
        static const AkUniqueID L2_D4_1 = 3192868382U;
        static const AkUniqueID L2_D4_2 = 3192868381U;
        static const AkUniqueID L2_D4_3 = 3192868380U;
        static const AkUniqueID L2_D4_4 = 3192868379U;
        static const AkUniqueID L2_D4_5 = 3192868378U;
        static const AkUniqueID L2_D4_6 = 3192868377U;
        static const AkUniqueID L2_D5_1 = 3193015543U;
        static const AkUniqueID L2_D5_2 = 3193015540U;
        static const AkUniqueID L2_D5_3 = 3193015541U;
        static const AkUniqueID L2_D5_4 = 3193015538U;
        static const AkUniqueID L2_D5_5 = 3193015539U;
        static const AkUniqueID L2_D5_6 = 3193015536U;
        static const AkUniqueID L2_D5_7 = 3193015537U;
        static const AkUniqueID L2_D5_8 = 3193015550U;
        static const AkUniqueID L2_D5_9 = 3193015551U;
        static const AkUniqueID L2_D6_1 = 2118806640U;
        static const AkUniqueID L2_D6_2 = 2118806643U;
        static const AkUniqueID L2_D6_3 = 2118806642U;
        static const AkUniqueID L2_D7_1 = 2118953705U;
        static const AkUniqueID L2_D7_2 = 2118953706U;
        static const AkUniqueID L2_D7_3 = 2118953707U;
        static const AkUniqueID L2_D8_1 = 2117129322U;
        static const AkUniqueID L2_D8_2 = 2117129321U;
        static const AkUniqueID L2_D8_3 = 2117129320U;
        static const AkUniqueID L2_D8_4 = 2117129327U;
        static const AkUniqueID L2_D8_5 = 2117129326U;
        static const AkUniqueID L2_D8_6 = 2117129325U;
        static const AkUniqueID L2_D9_1 = 2117276355U;
        static const AkUniqueID L2_D9_2 = 2117276352U;
        static const AkUniqueID L2_D9_3 = 2117276353U;
        static const AkUniqueID L2_D10_1 = 2129836501U;
        static const AkUniqueID L2_D10_2 = 2129836502U;
        static const AkUniqueID L2_D10_3 = 2129836503U;
        static const AkUniqueID L2_D10_4 = 2129836496U;
        static const AkUniqueID L2_D10_5 = 2129836497U;
        static const AkUniqueID L2_D11_1 = 2129689468U;
        static const AkUniqueID L2_D11_2 = 2129689471U;
        static const AkUniqueID L2_E1_1 = 3940116320U;
        static const AkUniqueID L2_E1_2 = 3940116323U;
        static const AkUniqueID L2_E1_3 = 3940116322U;
        static const AkUniqueID L2_E1_4 = 3940116325U;
        static const AkUniqueID L2_E2_1 = 3940660775U;
        static const AkUniqueID L2_E2_2 = 3940660772U;
        static const AkUniqueID L2_E2_3 = 3940660773U;
        static const AkUniqueID L2_E2_4 = 3940660770U;
        static const AkUniqueID L2_E2_5 = 3940660771U;
        static const AkUniqueID L2_E2_6 = 3940660768U;
        static const AkUniqueID L2_E3_1 = 719210766U;
        static const AkUniqueID L2_E3_2 = 719210765U;
        static const AkUniqueID L2_E3_3 = 719210764U;
        static const AkUniqueID L2_E4_1 = 3939675069U;
        static const AkUniqueID L2_E4_2 = 3939675070U;
        static const AkUniqueID L2_E4_3 = 3939675071U;
        static const AkUniqueID L2_E4_4 = 3939675064U;
        static const AkUniqueID L2_E5_1 = 3939527876U;
        static const AkUniqueID L2_E5_2 = 3939527879U;
        static const AkUniqueID L2_E6_1 = 3939969163U;
        static const AkUniqueID L2_E6_2 = 3939969160U;
        static const AkUniqueID L2_E7_1 = 718622418U;
        static const AkUniqueID L2_E8_1 = 3938983425U;
        static const AkUniqueID L2_E8_2 = 3938983426U;
        static const AkUniqueID L2_E8_3 = 3938983427U;
        static const AkUniqueID L2_E9_1 = 717636712U;
        static const AkUniqueID L2_E9_2 = 717636715U;
        static const AkUniqueID L2_E9_3 = 717636714U;
        static const AkUniqueID L2_E10_1 = 52025720U;
        static const AkUniqueID L2_E10_2 = 52025723U;
        static const AkUniqueID L3_D1_1 = 1031016296U;
        static const AkUniqueID L3_D1_2 = 1031016299U;
        static const AkUniqueID L3_D1_3 = 1031016298U;
        static const AkUniqueID L3_D1_4 = 1031016301U;
        static const AkUniqueID L3_D1_5 = 1031016300U;
        static const AkUniqueID L3_D1_6 = 1031016303U;
        static const AkUniqueID L3_D2_1 = 1031457583U;
        static const AkUniqueID L3_D2_2 = 1031457580U;
        static const AkUniqueID L3_D2_3 = 1031457581U;
        static const AkUniqueID L3_D3_1 = 1031310518U;
        static const AkUniqueID L3_D3_2 = 1031310517U;
        static const AkUniqueID L3_D3_3 = 1031310516U;
        static const AkUniqueID L3_D3_4 = 1031310515U;
        static const AkUniqueID L3_D3_5 = 1031310514U;
        static const AkUniqueID L3_D3_6 = 1031310513U;
        static const AkUniqueID L3_D3_7 = 1031310512U;
        static const AkUniqueID L3_D4_1 = 4251774693U;
        static const AkUniqueID L3_D4_2 = 4251774694U;
        static const AkUniqueID L3_D4_2B = 3632010352U;
        static const AkUniqueID L3_D4_2C = 3632010353U;
        static const AkUniqueID L3_D4_3 = 4251774695U;
        static const AkUniqueID L3_D4_4 = 4251774688U;
        static const AkUniqueID L3_D4_4B = 3531344578U;
        static const AkUniqueID L3_D4_5 = 4251774689U;
        static const AkUniqueID L3_D4_6 = 4251774690U;
        static const AkUniqueID L3_D4_7 = 4251774691U;
        static const AkUniqueID L3_D4_7B = 3581677371U;
        static const AkUniqueID L3_D4_8 = 4251774700U;
        static const AkUniqueID L3_D4_8B = 3732676070U;
        static const AkUniqueID L3_D4_9 = 4251774701U;
        static const AkUniqueID L3_D4_10 = 3615232591U;
        static const AkUniqueID L3_D4_11 = 3615232590U;
        static const AkUniqueID L3_D4_12 = 3615232589U;
        static const AkUniqueID L3_D4_13 = 3615232588U;
        static const AkUniqueID L3_D4_14 = 3615232587U;
        static const AkUniqueID L3_D4_15 = 3615232586U;
        static const AkUniqueID L3_D4_16 = 3615232585U;
        static const AkUniqueID L3_D4_17 = 3615232584U;
        static const AkUniqueID L3_D4_18 = 3615232583U;
        static const AkUniqueID L3_D4_19 = 3615232582U;
        static const AkUniqueID L3_E1_1 = 2430655939U;
        static const AkUniqueID L3_E1_2 = 2430655936U;
        static const AkUniqueID L3_E1_3 = 2430655937U;
        static const AkUniqueID L3_E1_4 = 2430655942U;
        static const AkUniqueID L3_E1_5 = 2430655943U;
        static const AkUniqueID L3_E1_6 = 2430655940U;
        static const AkUniqueID L3_E1_7 = 2430655941U;
        static const AkUniqueID L3_E2_1 = 3503982300U;
        static const AkUniqueID L3_E2_2 = 3503982303U;
        static const AkUniqueID L3_E3_1 = 3504129461U;
        static const AkUniqueID L3_E3_2 = 3504129462U;
        static const AkUniqueID L3_E3_3 = 3504129463U;
        static const AkUniqueID L3_E3_4 = 3504129456U;
        static const AkUniqueID L3_E3_5 = 3504129457U;
        static const AkUniqueID L3_E3_6 = 3504129458U;
        static const AkUniqueID L3_E3_7 = 3504129459U;
        static const AkUniqueID L3_E3_8 = 3504129468U;
        static const AkUniqueID L3_E4_1 = 2431200390U;
        static const AkUniqueID L3_E4_2 = 2431200389U;
        static const AkUniqueID L3_E4_3 = 2431200388U;
        static const AkUniqueID L3_E5_1 = 3505115199U;
        static const AkUniqueID L3_E6_1 = 3504570744U;
        static const AkUniqueID L3_E6_2 = 3504570747U;
        static const AkUniqueID L3_E7_1 = 3504717777U;
        static const AkUniqueID L3_E7_2 = 3504717778U;
        static const AkUniqueID L3_E7_3 = 3504717779U;
        static const AkUniqueID L3_E8_1 = 2431788834U;
        static const AkUniqueID L3_E9_1 = 3505703515U;
        static const AkUniqueID L3_E9_2 = 3505703512U;
        static const AkUniqueID L3_E9_3 = 3505703513U;
        static const AkUniqueID L3_E10_1 = 2146591693U;
        static const AkUniqueID L3_E10_2 = 2146591694U;
        static const AkUniqueID L3_E11_1 = 3220212244U;
        static const AkUniqueID L3_E11_2 = 3220212247U;
        static const AkUniqueID L3_E12_1 = 3220653531U;
        static const AkUniqueID LEVEL1 = 2678230382U;
        static const AkUniqueID LEVEL2 = 2678230381U;
        static const AkUniqueID LIBRARYWATER = 1896386335U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID MERCURYABILITY = 2729961418U;
        static const AkUniqueID METEOR = 1599898841U;
        static const AkUniqueID METEORIMPACT = 2102709867U;
        static const AkUniqueID MISSEDATTACK = 1104727554U;
        static const AkUniqueID MOVINGPLATFORM = 1948881314U;
        static const AkUniqueID NEWMENU = 2849146544U;
        static const AkUniqueID NOXATTACKS = 2403436307U;
        static const AkUniqueID NOXFLY = 3389589911U;
        static const AkUniqueID NOXINTERRUPTED = 1497598018U;
        static const AkUniqueID NOXLAND = 463179643U;
        static const AkUniqueID NOXMOVES = 1098788530U;
        static const AkUniqueID NOXMUSIC = 2856500789U;
        static const AkUniqueID NOXWING = 1261776957U;
        static const AkUniqueID NOXWINGATTACK = 2059659317U;
        static const AkUniqueID PAUSEALLEVENTS = 3805746773U;
        static const AkUniqueID PISADASNOX = 2574438359U;
        static const AkUniqueID PORTAL = 3118032615U;
        static const AkUniqueID RESUMEALLEVENTS = 3345100366U;
        static const AkUniqueID RUN = 712161704U;
        static const AkUniqueID SALTABILITYEND = 1261900568U;
        static const AkUniqueID SALTABILITYSTART = 2013433027U;
        static const AkUniqueID STOPALL = 3086540886U;
        static const AkUniqueID STOPL0_D1 = 1642902161U;
        static const AkUniqueID STOPL0_D2 = 1642902162U;
        static const AkUniqueID STOPL0_D3 = 1642902163U;
        static const AkUniqueID STOPL1_D1_1 = 3425607442U;
        static const AkUniqueID STOPL1_D1_2 = 3425607441U;
        static const AkUniqueID STOPL1_D1_3 = 3425607440U;
        static const AkUniqueID STOPL1_D1_4 = 3425607447U;
        static const AkUniqueID STOPL1_D1_5 = 3425607446U;
        static const AkUniqueID STOPL1_D1_6 = 3425607445U;
        static const AkUniqueID STOPL1_D1_7 = 3425607444U;
        static const AkUniqueID STOPL1_D1_8 = 3425607451U;
        static const AkUniqueID STOPL1_D1_9 = 3425607450U;
        static const AkUniqueID STOPL1_D2_1 = 2351692797U;
        static const AkUniqueID STOPL1_D2_2 = 2351692798U;
        static const AkUniqueID STOPL1_D2_3 = 2351692799U;
        static const AkUniqueID STOPL1_D2_4 = 2351692792U;
        static const AkUniqueID STOPL1_D3_1 = 2351545604U;
        static const AkUniqueID STOPL1_D3_2 = 2351545607U;
        static const AkUniqueID STOPL1_D3_3 = 2351545606U;
        static const AkUniqueID STOPL1_D3_4 = 2351545601U;
        static const AkUniqueID STOPL1_D3_5 = 2351545600U;
        static const AkUniqueID STOPL1_D3_6 = 2351545603U;
        static const AkUniqueID STOPL1_D3_7 = 2351545602U;
        static const AkUniqueID STOPL1_D4_1 = 2352678503U;
        static const AkUniqueID STOPL1_D4_2 = 2352678500U;
        static const AkUniqueID STOPL1_D4_3 = 2352678501U;
        static const AkUniqueID STOPL1_D4_4 = 2352678498U;
        static const AkUniqueID STOPL1_D4_5 = 2352678499U;
        static const AkUniqueID STOPL1_D5_1 = 3426298958U;
        static const AkUniqueID STOPL1_D5_2 = 3426298957U;
        static const AkUniqueID STOPL1_D5_3 = 3426298956U;
        static const AkUniqueID STOPL1_D5_4 = 3426298955U;
        static const AkUniqueID STOPL1_D5_5 = 3426298954U;
        static const AkUniqueID STOPL1_D5_6 = 3426298953U;
        static const AkUniqueID STOPL1_D5_7 = 3426298952U;
        static const AkUniqueID STOPL2_D1_1 = 781925299U;
        static const AkUniqueID STOPL2_D1_2 = 781925296U;
        static const AkUniqueID STOPL2_D1_3 = 781925297U;
        static const AkUniqueID STOPL2_D2_1 = 781483916U;
        static const AkUniqueID STOPL2_D2_2 = 781483919U;
        static const AkUniqueID STOPL2_D2_3 = 781483918U;
        static const AkUniqueID STOPL2_D2_4 = 781483913U;
        static const AkUniqueID STOPL2_D3_1 = 4002830757U;
        static const AkUniqueID STOPL2_D3_2 = 4002830758U;
        static const AkUniqueID STOPL2_D3_3 = 4002830759U;
        static const AkUniqueID STOPL2_D3_4 = 4002830752U;
        static const AkUniqueID STOPL2_D3_5 = 4002830753U;
        static const AkUniqueID STOPL2_D3_6 = 4002830754U;
        static const AkUniqueID STOPL2_D3_7 = 4002830755U;
        static const AkUniqueID STOPL2_D3_8 = 4002830764U;
        static const AkUniqueID STOPL2_D4_1 = 782469750U;
        static const AkUniqueID STOPL2_D4_2 = 782469749U;
        static const AkUniqueID STOPL2_D4_3 = 782469748U;
        static const AkUniqueID STOPL2_D4_4 = 782469747U;
        static const AkUniqueID STOPL2_D4_5 = 782469746U;
        static const AkUniqueID STOPL2_D4_6 = 782469745U;
        static const AkUniqueID STOPL2_D5_1 = 782616815U;
        static const AkUniqueID STOPL2_D5_2 = 782616812U;
        static const AkUniqueID STOPL2_D5_3 = 782616813U;
        static const AkUniqueID STOPL2_D5_4 = 782616810U;
        static const AkUniqueID STOPL2_D5_5 = 782616811U;
        static const AkUniqueID STOPL2_D5_6 = 782616808U;
        static const AkUniqueID STOPL2_D5_7 = 782616809U;
        static const AkUniqueID STOPL2_D5_8 = 782616806U;
        static const AkUniqueID STOPL2_D5_9 = 782616807U;
        static const AkUniqueID STOPL2_D6_1 = 782072360U;
        static const AkUniqueID STOPL2_D6_2 = 782072363U;
        static const AkUniqueID STOPL2_D6_3 = 782072362U;
        static const AkUniqueID STOPL2_D7_1 = 4003522241U;
        static const AkUniqueID STOPL2_D7_2 = 4003522242U;
        static const AkUniqueID STOPL2_D7_3 = 4003522243U;
        static const AkUniqueID STOPL2_D8_1 = 783058066U;
        static const AkUniqueID STOPL2_D8_2 = 783058065U;
        static const AkUniqueID STOPL2_D8_3 = 783058064U;
        static const AkUniqueID STOPL2_D8_4 = 783058071U;
        static const AkUniqueID STOPL2_D8_5 = 783058070U;
        static const AkUniqueID STOPL2_D8_6 = 783058069U;
        static const AkUniqueID STOPL2_D9_1 = 4004507979U;
        static const AkUniqueID STOPL2_D9_2 = 4004507976U;
        static const AkUniqueID STOPL2_D9_3 = 4004507977U;
        static const AkUniqueID STOPL2_D10_1 = 1001761469U;
        static const AkUniqueID STOPL2_D10_2 = 1001761470U;
        static const AkUniqueID STOPL2_D10_3 = 1001761471U;
        static const AkUniqueID STOPL2_D10_4 = 1001761464U;
        static const AkUniqueID STOPL2_D10_5 = 1001761465U;
        static const AkUniqueID STOPL2_D11_1 = 1001614276U;
        static const AkUniqueID STOPL2_D11_2 = 1001614279U;
        static const AkUniqueID STOPL3_D1_1 = 2788021536U;
        static const AkUniqueID STOPL3_D1_2 = 2788021539U;
        static const AkUniqueID STOPL3_D1_3 = 2788021538U;
        static const AkUniqueID STOPL3_D1_4 = 2788021541U;
        static const AkUniqueID STOPL3_D1_5 = 2788021540U;
        static const AkUniqueID STOPL3_D1_6 = 2788021543U;
        static const AkUniqueID STOPL3_D2_1 = 2788462823U;
        static const AkUniqueID STOPL3_D2_2 = 2788462820U;
        static const AkUniqueID STOPL3_D2_3 = 2788462821U;
        static const AkUniqueID STOPL3_D3_1 = 3862083278U;
        static const AkUniqueID STOPL3_D3_2 = 3862083277U;
        static const AkUniqueID STOPL3_D3_3 = 3862083276U;
        static const AkUniqueID STOPL3_D3_4 = 3862083275U;
        static const AkUniqueID STOPL3_D3_5 = 3862083274U;
        static const AkUniqueID STOPL3_D3_6 = 3862083273U;
        static const AkUniqueID STOPL3_D3_7 = 3862083272U;
        static const AkUniqueID STOPL3_D4_1 = 2787477117U;
        static const AkUniqueID STOPL3_D4_2 = 2787477118U;
        static const AkUniqueID STOPL3_D4_2B = 185776184U;
        static const AkUniqueID STOPL3_D4_2C = 185776185U;
        static const AkUniqueID STOPL3_D4_3 = 2787477119U;
        static const AkUniqueID STOPL3_D4_4 = 2787477112U;
        static const AkUniqueID STOPL3_D4_4B = 85110410U;
        static const AkUniqueID STOPL3_D4_5 = 2787477113U;
        static const AkUniqueID STOPL3_D4_6 = 2787477114U;
        static const AkUniqueID STOPL3_D4_7 = 2787477115U;
        static const AkUniqueID STOPL3_D4_7B = 135443395U;
        static const AkUniqueID STOPL3_D4_8 = 2787477108U;
        static const AkUniqueID STOPL3_D4_8B = 18000126U;
        static const AkUniqueID STOPL3_D4_9 = 2787477109U;
        static const AkUniqueID STOPL3_D4_10 = 168998647U;
        static const AkUniqueID STOPL3_D4_11 = 168998646U;
        static const AkUniqueID STOPL3_D4_12 = 168998645U;
        static const AkUniqueID STOPL3_D4_13 = 168998644U;
        static const AkUniqueID STOPL3_D4_14 = 168998643U;
        static const AkUniqueID STOPL3_D4_15 = 168998642U;
        static const AkUniqueID STOPL3_D4_16 = 168998641U;
        static const AkUniqueID STOPL3_D4_17 = 168998640U;
        static const AkUniqueID STOPL3_D4_18 = 168998655U;
        static const AkUniqueID STOPL3_D4_19 = 168998654U;
        static const AkUniqueID STOPLEVEL1 = 3077303046U;
        static const AkUniqueID STOPLEVEL2 = 3077303045U;
        static const AkUniqueID STOPLIBRARYWATER = 726881927U;
        static const AkUniqueID STOPMENU = 2273031176U;
        static const AkUniqueID STOPMETEOR = 170434545U;
        static const AkUniqueID STOPMOVINGPLATFORM = 26862282U;
        static const AkUniqueID STOPNEWMENU = 2853327464U;
        static const AkUniqueID STOPPORTAL = 918984447U;
        static const AkUniqueID STOPRUN = 2971613536U;
        static const AkUniqueID SULPHURABILITY = 369425044U;
        static const AkUniqueID TRANSITIONTOCOMBAT = 930838687U;
        static const AkUniqueID TRANSITIONTOMUSIC = 2785677392U;
        static const AkUniqueID UI_BACK_1 = 3850258839U;
        static const AkUniqueID UI_BACK_2 = 3850258836U;
        static const AkUniqueID UI_CLICK = 2249769530U;
        static const AkUniqueID UI_SELECT_1 = 3061041898U;
        static const AkUniqueID UI_SELECT_2 = 3061041897U;
        static const AkUniqueID WATERFALL = 2074477625U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace LEVEL
        {
            static const AkUniqueID GROUP = 2782712965U;

            namespace SWITCH
            {
                static const AkUniqueID LEVEL1_2 = 2867782685U;
                static const AkUniqueID LEVEL3 = 2678230380U;
            } // namespace SWITCH
        } // namespace LEVEL

        namespace NOXSTEPS
        {
            static const AkUniqueID GROUP = 2414533941U;

            namespace SWITCH
            {
                static const AkUniqueID HUMAN = 3887404748U;
                static const AkUniqueID OWL = 679047689U;
            } // namespace SWITCH
        } // namespace NOXSTEPS

        namespace RUNSWITCH
        {
            static const AkUniqueID GROUP = 3764495980U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID STONE = 1216965916U;
            } // namespace SWITCH
        } // namespace RUNSWITCH

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
