﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildrenToFade : MonoBehaviour {
	Renderer[] rendererChild;
	public Material materialToChange;

	// Use this for initialization
	void Start () {
		rendererChild = GetComponentsInChildren<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Activate() {
		foreach (Renderer r in rendererChild) {
			r.material = materialToChange;
		}
	}
}
