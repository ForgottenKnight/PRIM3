﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class MSMS_Nox_Active : MonoStateMachineState
{
    private Animator l_Anim;

    public override void StateUpdate()
    {
    }

    public override void OnEnter()
    {
        AkSoundEngine.SetSwitch("NoxSteps", "Owl", gameObject);
        ChangeState("Active_Chase");
    }

    public override void OnExit()
    {
    }

    public override void OnStart()
    {
    }

    public override void OnFirstEnter()
    {
    }
}
