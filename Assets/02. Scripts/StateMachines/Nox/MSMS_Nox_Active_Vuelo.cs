﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MSMS_Nox_Active_Vuelo : MonoStateMachineState
{
    [Header("Flight parameters")]
    public int numberOfEnemies = 5;
    public Transform possiblePositions;
    public GameObject enemyPrefab;
    public SimpleEvent simpleEvent;

    [Header("Push parameters")]
    public float pushTime = 1.5f;
    public float pushForce = 2f;
    public float pushRadius = 15f;

    private Animator m_Anim;
    private TargetSelector m_Target;
    private Health m_Health;
    private Canvas m_Canvas;
    private Target m_IsTarget;
    private int[] m_UsedPositions;

    public override void StateUpdate()
    {

    }

    public override void OnEnter()
    {
		SimpleEvent.CallEvent ("NOX_BATTLE_6");
        m_Anim.SetTrigger("fly");
        m_Anim.speed = 1.0f;
        m_Health.invincible = true;
        if(!m_Canvas)
        {
            m_Canvas = GetComponentInChildren<Canvas>();
        }
        m_IsTarget.SetTargeteable(false);
    }

    public void Flying()
    {
        gameObject.layer = LayerMask.NameToLayer("Teleporting");
        SpawnEnemies();
        m_Canvas.enabled = false;
        List<GameObject> players = CustomTagManager.GetObjectsByTag("Player");
        for (int i = 0; i < players.Count; ++i)
        {
            if (players[i].activeSelf)
            {
                staticAttackCheck.CheckPush(players[i].transform, transform.position, pushRadius, pushForce, pushTime);
            }
        }
    }

    public void Land()
    {
        gameObject.layer = LayerMask.NameToLayer("Enemy");
        m_Canvas.enabled = true;
        m_Anim.SetTrigger("land");
    }

    public void DoneFlying()
    {
        m_Parent.ChangeState("Active_Chase");
    }

    public override void OnExit()
    {
        m_Health.invincible = false;
        m_IsTarget.SetTargeteable(true);
    }

    public override void OnStart()
    {
        m_Anim = GetComponentInChildren<Animator>();
        m_Target = GetComponent<TargetSelector>();
        m_Health = GetComponent<Health>();
        m_IsTarget = GetComponent<Target>();
    }

    private bool CheckSpawn(int index)
    {
        for (int i = 0; i < m_UsedPositions.Length; ++i)
        {
            if(m_UsedPositions[i] == index)
            {
                return false;
            }
        }
        return true;
    }

    private void SpawnEnemies()
    {
        List<Vector3> l_EnemySpawnPoints = new List<Vector3>();
        float beginTime = Time.time;
        float securityTime = 2f;
        m_UsedPositions = new int[numberOfEnemies];

        while (l_EnemySpawnPoints.Count < numberOfEnemies && Time.time < beginTime + securityTime)
        {
            int RandIndex = Random.Range(0, possiblePositions.childCount);
            while(!CheckSpawn(RandIndex))
            {
                RandIndex = Random.Range(0, possiblePositions.childCount);
            }
            m_UsedPositions[l_EnemySpawnPoints.Count] = RandIndex;
            l_EnemySpawnPoints.Add(possiblePositions.GetChild(RandIndex).position);
        }
        StartCoroutine(CreateLightnings(l_EnemySpawnPoints));
    }

    IEnumerator CreateLightnings(List<Vector3> l_Points)
    {

        List<GameObject> enemies = new List<GameObject>();
        GameObject enemy;
        MSMS_Book1_Active_Flee flee;
        for (int i = 0; i < l_Points.Count; ++i)
        {
            enemy = Instantiate(enemyPrefab, l_Points[i], Quaternion.identity);
            flee = enemy.GetComponent<MSMS_Book1_Active_Flee>();
            if (flee)
            {
                flee.distanceToFlee = 0;
            }
            enemy.transform.parent = transform.parent;
            enemies.Add(enemy);

            //yield return new WaitForSeconds(delayBetweenEnemies);
        }

        simpleEvent.resolvedQuantity = 0;
        simpleEvent.quantityToResolve = l_Points.Count;
        simpleEvent.enemiesList = new Damageable[l_Points.Count];


        for (int i = 0; i < enemies.Count; ++i)
        {
            simpleEvent.enemiesList[i] = enemies[i].GetComponent<Damageable>();
        }

        simpleEvent.RestartEvent();

        yield return null;
    }
}
