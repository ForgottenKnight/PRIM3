﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSMS_Nox_Active_Chase : MonoStateMachineState {

	private UnityEngine.AI.NavMeshAgent m_Agent;
	private Animator m_Anim;
	private TargetSelector m_Target;
	private CharacterController m_Character;
	private Transform m_RaycastSource;
	private BoxCollider m_BoxCollider;
    private List<string> m_possibleActions = new List<string>();
    private float m_lastActionTime;
    private Health m_Health;

	public bool canMove = true;

    public float movementSpeed = 4.5f;

    public float delayBetweenActions = 2.0f;

    [HideInInspector]
    public bool m_canHeal = true;

    private bool m_firstFlight = true;
    private bool m_secondFlight = true;
    public float maxDifferenceY = 2.0f;

    [Header("Picotazo")]
    public float picotazoRange = 10.0f;
    public float maxAngle = 60.0f;

    [Header("Ataque Ala")]
    public float wingRange = 3.0f;
    public float maxWingAngle = 180.0f;


    [Header("Ataque Plumas")]
    public float featherMinRange = 5.0f;
    public float featherMaxRange = 10.0f;
    public float maxFeatherAngle = 90.0f;

	[Header("Rotation parameters")]
	public float rotationSpeed = 5.0f;
	public float threshold = 0.5f;

	[System.Serializable]
	public struct LifePercentEvent {
		public float percent;
		public string eventName;
	}

	[SerializeField]
	public List<LifePercentEvent> lifePercentEvents;

	public override void StateUpdate() {
        bool l_Grounded = CheckGrounded();
        if (m_Agent.enabled == false && l_Grounded && canMove)
        {
            m_Agent.enabled = true;
        }else if (!l_Grounded)
        {
            m_Agent.enabled = false;
        }

		if (m_Agent.speed == 0.0f && canMove == true) {
			if (m_Character.isGrounded) {
				m_Agent.speed = movementSpeed;
			}
		}
		if (m_Target.target != null) {
			Vector3 target2D = m_Target.target.transform.position;
			target2D.y = 0f;
			Vector3 position2D = transform.position;
			position2D.y = 0f;
			float distance = Vector3.Distance(target2D, position2D);
			float angle = Vector3.Angle(transform.forward, (target2D - position2D));
        
        if(m_firstFlight && CheckFlight())
        {
            ActionStopRotating();
            ActionStop();
            ActionFly();
            m_firstFlight = false;
        }
        else if (m_secondFlight && CheckFlight())
        {
            ActionStopRotating();
            ActionStop();
            ActionFly();
            m_secondFlight = false;
        }else if (m_canHeal && CheckHealth())
        {
            ActionStopRotating();
            ActionStop();
            ActionHeal();
        }else if (CheckTime() && CheckAllActions(distance, angle))
        {
            ActionStop();
            ActionStopRotating();
            DoRandomAction();
        }else if (CheckStopDistance(distance) || !CheckGroundInFront())
        {
            ActionStop();
            if (!CheckAngle(angle))
            { // Caso no esta encarado
                ActionRotate();
            }
            else
            {
                ActionStopRotating();
            }
        }else if (CheckCanMove()) { // Caso sigue persiguiendo y tiene nav agent

                ActionStopRotating();
				ActionMove ();
			}
		} else {
            ActionStopRotating();
			ActionStop ();
			// Change state to wander
		}
	}
	
	public override void OnEnter() {
		//m_LastAttackTime = Time.time;
		//m_LastThrowTime = Time.time;
		m_Anim.SetBool("walking", false);
		m_Anim.speed = 1.0f;
	}
	
	public override void OnExit() {
	}
	
	public override void OnStart() {
		m_Anim = GetComponentInChildren<Animator> ();
		m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		m_Target = GetComponent<TargetSelector> ();
		m_Character = GetComponent<CharacterController> ();
        m_Health = GetComponent<Health>();
		m_BoxCollider = GetComponent<BoxCollider> ();
		m_RaycastSource = GetComponentInChildren<RaycastObject> ().transform;
		if (canMove == false) {
			m_Agent.speed = 0.0f;
		}
	}

    void DoRandomAction()
    {
        int l_actionIndex = Random.Range(0, m_possibleActions.Count);
        string l_actionName = m_possibleActions[l_actionIndex];
        m_possibleActions.Clear();
        m_Parent.ChangeState(l_actionName);
        m_lastActionTime = Time.time;
    }

	// Checks

    bool CheckTime()
    {
        return m_lastActionTime + delayBetweenActions < Time.time;
    }

    bool CheckFlight()
    {
        if(m_firstFlight)
        {
            return m_Health.GetHealthAsPercent() <= 75.0f;
        }
        else if (m_secondFlight)
        {
            return m_Health.GetHealthAsPercent() <= 25.0f;
        }

        return false;
    }

    bool CheckAllActions(float distance, float angle)
    {
        bool l_canDoAction = false;

        if(CheckPicotazo(distance,angle))
        {
            l_canDoAction = true;
            m_possibleActions.Add("Active_Picotazo");
        }

        if(CheckWingAttack(distance,angle))
        {
            l_canDoAction = true;
            m_possibleActions.Add("Active_WingAttack");
        }

        if (CheckFeatherAttack(distance,angle))
        {
            l_canDoAction = true;
            m_possibleActions.Add("Active_Feathers");
        }

        return l_canDoAction;
    }

    bool CheckFeatherAttack(float distance, float angle)
    {
        return angle <= maxFeatherAngle && distance <= featherMaxRange && distance >= featherMinRange && ((Mathf.Abs(m_Target.target.transform.position.y - transform.position.y) <= maxDifferenceY) || maxDifferenceY == 0.0f);
    }

    bool CheckPicotazo(float distance, float angle)
    {
        return angle <= maxAngle && distance <= picotazoRange && ((Mathf.Abs(m_Target.target.transform.position.y - transform.position.y) <= maxDifferenceY) || maxDifferenceY == 0.0f);
    }

    bool CheckWingAttack(float distance, float angle)
    {
        return angle <= maxWingAngle && distance <= wingRange && ((Mathf.Abs(m_Target.target.transform.position.y - transform.position.y) <= maxDifferenceY) || maxDifferenceY == 0.0f);
    }

    bool CheckHealth()
    {
        return m_Health.GetHealthAsPercent() <= 50.0f;
    }

	bool CheckStopDistance(float distance) {
		return distance <= m_Agent.stoppingDistance;
	}

	bool CheckAngle(float angle) {
       // Debug.Log(angle <= threshold);
		return angle <= threshold;
	}

	bool CheckGroundInFront() {
		return Physics.Raycast (m_RaycastSource.position, -Vector3.up, 0.5f);
	}

    bool CheckGrounded()
    {
        Vector3 lastPos = transform.position;
        Vector3 up = transform.up;
        Ray ray = new Ray(lastPos, -up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 2f))
        {
            if (hit.collider.gameObject.layer != gameObject.layer)
            {
                return true;
            }
        }
		if (m_BoxCollider != null) {
			Bounds b = m_BoxCollider.bounds;
			Vector3 p1 = b.min;
			Vector3 p2 = b.max;
			p2.y = p1.y;
			if (Physics.Raycast(p1, -up, out hit, 2f)) {
				if (hit.collider.gameObject.layer != gameObject.layer)
				{
					return true;
				}
			}
			if (Physics.Raycast(p2, -up, out hit, 2f)) {
				if (hit.collider.gameObject.layer != gameObject.layer)
				{
					return true;
				}
			}
		}
        return false;
    }

	bool CheckCanMove() {
		return (m_Agent.speed > 0.0f) && canMove;
	}

	// Actions
	void ActionStop() {
		if (CheckCanMove ()) {
            if (m_Agent.enabled)
            {
                m_Agent.ResetPath();
                m_Agent.isStopped = true;
            }
			m_Anim.SetBool ("walking", false);
		}
	}

    void ActionRotate()
    {
        Quaternion lookRotation;
        Vector3 direction;
        direction = (m_Target.target.transform.position - transform.position).normalized;
        direction.y = transform.forward.y;
        lookRotation = Quaternion.LookRotation(direction);
        Vector3 cross = Vector3.Cross(transform.rotation * Vector3.forward, lookRotation * Vector3.forward);

        if (cross.y < 0)
        {
            m_Anim.SetBool("rotatingLeft", true);
            m_Anim.SetBool("rotatingRight", false);
        }
        else
        {
            m_Anim.SetBool("rotatingRight", true);
            m_Anim.SetBool("rotatingLeft", false);
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
    }

    void ActionStopRotating()
    {
        m_Anim.SetBool("rotatingRight", false);
        m_Anim.SetBool("rotatingLeft", false);
    }


    void ActionHeal()
    {
        m_Parent.ChangeState("Active_Heal");
    }

    void ActionFly()
    {
        m_Parent.ChangeState("Active_Vuelo");
    }

	void ActionMove() {
		if (CheckCanMove ()) {
			m_Anim.SetBool ("walking", true);
            m_Agent.isStopped = false;
			m_Agent.SetDestination (m_Target.target.transform.position);
		}
	}

	public override void OnFirstEnter() {
		SimpleEvent.CallEvent ("NOX_BATTLE_1");
		m_Health.RegisterOnDamage (DamageReceived);
	}

	public void DamageReceived() {
		if (lifePercentEvents.Count > 0) {
			LifePercentEvent lpe = lifePercentEvents [0];
			if (m_Health.GetHealthAsPercent () <= lpe.percent) {
				SimpleEvent.CallEvent (lpe.eventName);
				lifePercentEvents.RemoveAt (0);
			}
		}
	}

}
