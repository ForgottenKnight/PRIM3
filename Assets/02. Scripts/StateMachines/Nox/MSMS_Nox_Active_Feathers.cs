﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSMS_Nox_Active_Feathers : MonoStateMachineState
{


    [Header("Lightning parameters")]
    public int numberOfFeathers = 5;
    public float feathersScatteringArea = 15f;
    public float maxYDifference = 3.0f;
    public float maxAngle = 90f;

    [Tooltip("Este parámetro controla el tiempo entre que empieza a caer un relámpago y empieza a caer otro.")]
    public float delayBetweenFeathers = 1f;
    public GameObject feathersPrefab;
    public LayerMask ignoreLayers;
    //[Tooltip("Este parámetro controla el tiempo entre que se ve dónde caerá el relámpago y cae.")]
    //public float delayBeforeLightning = 0.5f; // En el prefab!!!

    private Animator m_Anim;
    private TargetSelector m_Target;
    private bool isDone = false;

    public override void StateUpdate()
    {

    }

    public override void OnEnter()
    {
        m_Anim.SetBool("feathers", true);
        m_Anim.speed = 1.0f;
        isDone = false;
      //  DoFeathers();
    }

    public override void OnExit()
    {
    }

    public override void OnStart()
    {
        m_Anim = GetComponentInChildren<Animator>();
        m_Target = GetComponent<TargetSelector>();
    }

    public void Land()
    {
        m_Parent.ChangeState("Active_Chase");
    }

    public void DoFeathers()
    {
        if (!isDone)
        {
            List<Vector3> l_FeathersPoints = new List<Vector3>();
            float l_HalfAngle = maxAngle * 0.5f;
            float beginTime = Time.time;
            float securityTime = 2f;

			int index = 0;
            while (index < numberOfFeathers && Time.time < beginTime + securityTime)
            {
                Vector3 l_Pos = m_Target.target.transform.position;
                if (l_FeathersPoints.Count > 0)
                {
                    l_Pos.x += feathersScatteringArea * Random.Range(-1f, 1f);
                    l_Pos.z += feathersScatteringArea * Random.Range(-1f, 1f);
                }
                Ray ray = new Ray(l_Pos + Vector3.up, -Vector3.up);
                if (Physics.Raycast(ray, maxYDifference, ignoreLayers))
                {
                    l_FeathersPoints.Add(l_Pos);
                }
				++index;
            }
            StartCoroutine(CreateFeathers(l_FeathersPoints));
            isDone = true;
        }
    }

    IEnumerator CreateFeathers(List<Vector3> l_Points)
    {
        for (int i = 0; i < l_Points.Count; ++i)
        {
            Instantiate(feathersPrefab, l_Points[i], Quaternion.identity);
            yield return new WaitForSeconds(delayBetweenFeathers);
        }

        m_Anim.SetBool("feathers", false);
        yield return null;
    }
	
}
