﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MSMS_Nox_Dying : MonoStateMachineState, DieReceiver
{
	private Animator l_Anim;
	private NavMeshAgent l_Agent;
    private TargetSelector m_Target;
    private Target m_IsTarget;
    private CombatManager m_CombatManager;

	public override void StateUpdate() {
	}

	public override void OnEnter() {
		l_Anim.SetTrigger ("die");
        m_IsTarget.SetTargeteable(false);
        GameObject targ = m_Target.target;
        if (targ)
        {
            GeneralPlayerController targ_GPC = targ.GetComponent<GeneralPlayerController>();
            if (targ_GPC)
            {
                targ_GPC.EnemiesTargeting.Remove(m_Target); 
                if (m_CombatManager)
                {
                    m_CombatManager.CheckEnemyTargets();
                   // m_CombatManager.SetBossActive(false);
                }
            }

        }
		l_Agent.enabled = false;
	}

	public override void OnExit() {
	}

	public override void OnStart() {
		l_Anim = GetComponentInChildren<Animator> ();
		l_Agent = GetComponent<NavMeshAgent> ();
        m_Target = GetComponent<TargetSelector>();
        m_IsTarget = GetComponent<Target>();
        m_CombatManager = CustomTagManager.GetObjectByTag("CombatManager").GetComponent<CombatManager>();
	}

	#region DieReceiver implementation
	public void Die ()
	{
		m_Parent.ChangeState (stateName);
	}
	#endregion
}
