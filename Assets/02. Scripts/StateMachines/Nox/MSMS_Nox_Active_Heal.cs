﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSMS_Nox_Active_Heal : MonoStateMachineState
{
    [Header("Heal parameters")]
    private Animator m_Anim;
    private Health m_Health;
    private float m_currentTimer = 0.0f;


    public MSMS_Nox_Active_Chase chaseScript;
    public float healing = 5.0f;
    public float healTimer = 1.5f;
    public int damageThreshold = 50;
    public ParticleSystem healingEffect;

    [HideInInspector]
    public bool m_interrupted = false;
    
    public bool m_done = false;
    private int m_previousDamageThreshoold; 

    public override void StateUpdate()
    {
        if (m_Health.interrupted && !m_interrupted)
        {
            m_interrupted = true;
        }

        if (!m_done)
        {
            m_currentTimer += Time.deltaTime;
            if (!m_interrupted && m_currentTimer >= healTimer)
            {
                m_Health.Heal(healing * StaticParemeters.numPlayers);
                if (m_Health.health == m_Health.maxHealth)
                {
                    m_Anim.SetTrigger("healFinished");
                    m_done = true;
					SimpleEvent.CallEvent ("NOX_BATTLE_8_2");
                }
                m_currentTimer = 0.0f;
            }
            else if (m_interrupted)
            {
				SimpleEvent.CallEvent ("NOX_BATTLE_8");
                m_Anim.SetTrigger("healInterrupted");
                m_done = true;
            }
        }
    }


    public void DoneHealing()
    {
        m_Parent.ChangeState("Active_Chase");
        if (m_interrupted)
        {
            chaseScript.m_canHeal = false;
        }

    }

    public override void OnEnter()
    {
        m_Anim.SetTrigger("heal");
        m_done = false;
        m_interrupted = false;
        m_previousDamageThreshoold = m_Health.damageThreshold;
        m_Health.damageThreshold = damageThreshold;
        m_Health.interrupted = false;
        healingEffect.Play();
		SimpleEvent.CallEvent ("NOX_BATTLE_7");
        //healingEffect.gameObject.SetActive(true);
    }
   
    public override void OnExit()
    {
        m_Health.damageThreshold = m_previousDamageThreshoold;
        healingEffect.Stop();
        //healingEffect.gameObject.SetActive(false);
    }

    public override void OnStart()
    {
        m_Anim = GetComponentInChildren<Animator>();
        m_Health = GetComponent<Health>();
        chaseScript = GetComponent<MSMS_Nox_Active_Chase>();
    }
}
