﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSMS_Nox_Active_WingAttack : MonoStateMachineState
{
    [Header("Attack parameters")]
    private Animator m_Anim;
    private bool m_Attacked;
    private bool m_CoroutineFinished;

    [Tooltip("Tiempo antes de ejecutar el ataque")]
    public float preWaitTime = 1.5f;

    [Tooltip("Tiempo para cambiar de estado tras ejecutar el ataque")]
    public float postWaitTime = 1.15f;
    public float attackLength = 2.5f;
    public float attackHeight = 2.0f;
    public int attackForce = 25;
    public float attackAngle = 90.0f;

	public float dialogProbability = 0.25f;

    public override void StateUpdate()
    {
        if (!m_Anim.GetCurrentAnimatorStateInfo(0).IsName("ataque ala") && !m_Anim.IsInTransition(0) && m_Attacked == false && m_CoroutineFinished == true)
        {
            m_Attacked = true;
            StartCoroutine(ChangeStateWaitTime());
        }
    }

    public override void OnEnter()
    {
        m_Attacked = false;
        m_CoroutineFinished = false;
        m_Anim.SetTrigger("wingAttack");
		if (Random.value < dialogProbability) {
			SimpleEvent.CallEvent ("NOX_BATTLE_9_2");
		}
        StartCoroutine(AttackWaitTime());
    }

    IEnumerator AttackWaitTime()
    {
        yield return new WaitForSeconds(preWaitTime);
        m_CoroutineFinished = true;
       // DoAttack();
    }

    IEnumerator ChangeStateWaitTime()
    {
        yield return new WaitForSeconds(postWaitTime);
        m_Parent.ChangeState("Active_Chase");
    }

    public void DoAttack()
    {
        List<GameObject> players = CustomTagManager.GetObjectsByTag("Player");
        for (int i = 0; i < players.Count; ++i)
        {
            staticAttackCheck.checkAttack(players[i].transform, transform, attackLength, attackAngle, attackHeight, (float)attackForce);
        }
        GameObject saltShield = GameObject.FindGameObjectWithTag("SaltShield");
        if (saltShield)
        {
            staticAttackCheck.checkAttack(saltShield.transform, transform, attackLength, attackAngle, attackHeight, (float)attackForce);
        }
        m_CoroutineFinished = true;
    }

    public override void OnExit()
    {
        StopAllCoroutines();
        m_Attacked = true;
    }

    public override void OnStart()
    {
        m_Anim = GetComponentInChildren<Animator>();
    }
}
