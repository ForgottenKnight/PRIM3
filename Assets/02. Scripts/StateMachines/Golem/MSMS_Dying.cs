﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class MSMS_Dying : MonoStateMachineState, DieReceiver {
	[Header("Die attributes")]
	public string dieLayer = "Event";
	[Header("Vaporize attributes")]
	public bool vaporize = true;
	public Shader vaporizeShader;
	public float vaporizeSpeed = 0.3f;
	public Texture2D noiseTexture;
	[Range(0.0f, 0.1f)]
	public float burntThreshold = 0.0242f;
	public Color burntStartColor = Color.white;
	public Color burntEndColor = Color.black;
	public bool destroyColliders = true;
	private Material m_Material;
	private bool m_Vaporizing;
	private float m_Threshold;
    private Target m_IsTarget;

    private TargetSelector m_Target;
	private Animator l_Anim;
	private UnityEngine.AI.NavMeshAgent l_Agent;
    private CombatManager m_CombatManager;

	public override void StateUpdate() {
		if (l_Anim.GetCurrentAnimatorStateInfo (0).normalizedTime >= 1f && l_Anim.GetCurrentAnimatorStateInfo (0).IsName ("Death") && !m_Vaporizing) {
			if (vaporize == true) {
				BeginVaporize ();
			} else {
				Destroy (gameObject);
			}
		} else if (m_Vaporizing == true) {
			m_Material.SetFloat ("_Threshold", m_Threshold);
			m_Threshold -= vaporizeSpeed * Time.deltaTime;
			if (m_Threshold <= 0f) {
                GameObject targ = m_Target.target;
                if (targ)
                {
                    GeneralPlayerController targ_GPC = targ.GetComponent<GeneralPlayerController>();
                    if (targ_GPC)
                    {
                        targ_GPC.EnemiesTargeting.Remove(m_Target); 
                        if (m_CombatManager)
                        {
                            m_CombatManager.CheckEnemyTargets();
                        }
                    }

                }
               // AkSoundEngine.PostEvent("GolemStopCharge", gameObject);
				Destroy (gameObject);
			}
		}
	}
	
	public override void OnEnter() {
        m_IsTarget.SetTargeteable(false);

        ResetAnimator();
		l_Anim.SetTrigger ("die");
        AkSoundEngine.PostEvent("GolemStopCharge", gameObject);
		l_Agent.enabled = false;
		gameObject.layer = LayerMask.NameToLayer (dieLayer);
        Target l_Target = GetComponent<Target>(); 
        if (destroyColliders == true)
        {
            DestroyColliders();
        }
		if (l_Target != null) {
			Destroy (l_Target);
		}
	}

    void ResetAnimator()
    {
        AnimatorControllerParameter[] parameters = l_Anim.parameters;
        for (int i = 0; i < parameters.Length; i++)
        {
            AnimatorControllerParameter parameter = parameters[i];
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Int:
                    l_Anim.SetInteger(parameter.name, parameter.defaultInt);
                    break;
                case AnimatorControllerParameterType.Float:
                    l_Anim.SetFloat(parameter.name, parameter.defaultFloat);
                    break;
                case AnimatorControllerParameterType.Bool:
                    l_Anim.SetBool(parameter.name, parameter.defaultBool);
                    break;
                case AnimatorControllerParameterType.Trigger:
                    l_Anim.ResetTrigger(parameter.name);
                    break;
            }
        }
    }
	
	public override void OnExit() {
	}
	
	public override void OnStart() {
		l_Anim = GetComponentInChildren<Animator> ();
		l_Agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
        m_Target = GetComponent<TargetSelector>();
		m_Vaporizing = false;
        m_Material = GetComponentInChildren<Renderer>().material;
        m_IsTarget = GetComponent<Target>();
        m_CombatManager = CustomTagManager.GetObjectByTag("CombatManager").GetComponent<CombatManager>();
	}

	void BeginVaporize() {
		Renderer ren = GetComponentInChildren<Renderer> ();
		m_Material.shader = vaporizeShader;
		m_Material.SetColor ("_BurntColorBegin", burntStartColor);
		m_Material.SetColor ("_BurntColorEnd", burntEndColor);
		m_Material.SetFloat ("_BurntThreshold", burntThreshold);
		m_Material.SetTexture ("_Noise", noiseTexture);
		m_Threshold = 1f;
        m_Vaporizing = true;
	}

	void DestroyColliders() {
		Collider[] l_Colliders = GetComponentsInChildren<Collider> ();
		for (int i = 0; i < l_Colliders.Length; ++i) {
			Destroy (l_Colliders [i]);
		}
		CharacterController l_CC = GetComponent<CharacterController> ();
		if (l_CC != null) {
			ApplyGravity l_Gravity = GetComponent<ApplyGravity> ();
			if (l_Gravity != null) {
				Destroy (l_Gravity);
			}
			Destroy (l_CC);
		}
		NavMeshAgent l_Agent = GetComponent<NavMeshAgent> ();
		if (l_Agent != null && l_Agent.enabled == true) {
			DebuffController l_Debuff = GetComponent<DebuffController> ();
			if (l_Debuff != null) {
				Destroy (l_Debuff);
			}
			l_Agent.isStopped = true;
			Destroy (l_Agent);
		}
	}
	
	#region DieReceiver implementation
	public void Die ()
	{
		m_Parent.ChangeState (stateName);
	}
	#endregion
}
