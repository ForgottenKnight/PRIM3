﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MSMS_Buried : MonoStateMachineState {
	[Header("Buried parameters")]
	private Animator l_Anim;
	private UnityEngine.AI.NavMeshAgent m_Agent;
	private TargetSelector m_Target;
	private Health m_Health;
	private Target m_TargetIndicator;
	private MSMS_Active_Chase m_ChaseState;
	//public float distanceToEmerge = 5.0f;
	private bool m_Emerging;
    private BoxCollider m_Collider;
    private CharacterController m_CharController;
	public bool emergeByPlayerProximity = true;
	public bool invulnerableBeforeEmerging = true;
	public string layerBeforeEmerging = "Event";
	public string layerAfterEmerging = "Enemy";
	public float emergeSpeed = 1.0f;
    public bool moveBody = false;
    public float YPos = -0.55f;
	public GameObject m_particleSystemPrefab;
	private GameObject m_particleSystem;
    private Target m_IsTarget;
    private GolemAnimationEvents m_GolAnimEvents;

	public override void StateUpdate() {
		if (!m_Emerging) {
			if (m_Target.target != null && emergeByPlayerProximity == true) {
				StartEmerging (0.0f);
			}
		}
	}

	IEnumerator FinishEmergeAnimation() {
		while (l_Anim.GetCurrentAnimatorStateInfo (0).normalizedTime < 1f && l_Anim.GetCurrentAnimatorStateInfo(0).IsName("Emerger")) {
			yield return null;
		}

		//Particulas de polvo basadas en movimiento

		ParticleSystem.EmissionModule emission = m_particleSystem.GetComponent<ParticleSystem> ().emission;
		emission.rateOverDistance = new ParticleSystem.MinMaxCurve(8.0f);
		emission.rateOverTime = new ParticleSystem.MinMaxCurve(0.0f);

		l_Anim.speed = 1.0f;
		m_Parent.ChangeState ("Active");
		yield return null;
	}
	
	public override void OnEnter() {
		gameObject.layer = LayerMask.NameToLayer(layerBeforeEmerging);
        m_IsTarget.SetTargeteable(false);
        m_GolAnimEvents.burriedSound = true;
	}
	
	public override void OnExit() {
        if (moveBody)
        {
            l_Anim.transform.localPosition = new Vector3(0f, YPos, 0f);
        }
	}
	
	public override void OnStart() {
        m_GolAnimEvents = GetComponentInChildren<GolemAnimationEvents>();
		l_Anim = GetComponentInChildren<Animator> ();
		m_Target = GetComponent<TargetSelector> ();
		m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		m_Health = GetComponent<Health> ();
		m_TargetIndicator = GetComponent<Target> ();
        m_CharController = GetComponent<CharacterController>();
        m_Collider = GetComponent<BoxCollider>();
		m_ChaseState = GetComponent<MSMS_Active_Chase> ();
		//m_TargetIndicator.DeactivateTarget ();
		l_Anim.speed = 0.0f;
		l_Anim.Play ("Emerger");
		m_Emerging = false;
        m_IsTarget = GetComponent<Target>();

		//Particulas de polvo al aparecer
		m_particleSystem = Instantiate (m_particleSystemPrefab, gameObject.transform);
		m_particleSystem.SetActive (false);
		ParticleSystem.EmissionModule emission = m_particleSystem.GetComponent<ParticleSystem> ().emission;
		emission.rateOverTime = new ParticleSystem.MinMaxCurve(15.0f);
		emission.rateOverDistance = new ParticleSystem.MinMaxCurve(0.0f);

		m_Agent.enabled = false;
        m_CharController.enabled = false;
        m_Collider.enabled = false;
        m_GolAnimEvents.burriedSound = true;
		MakeInvulnerable ();
	}

	public void Emerge(float aWaitForSeconds) {
		StartEmerging (aWaitForSeconds);
	}

	private void StartEmerging(float aWaitForSeconds) {
		StartCoroutine (EmergeAfterSeconds (aWaitForSeconds));
	}

	IEnumerator EmergeAfterSeconds(float aWaitForSeconds) {
		yield return new WaitForSecondsRealtime (aWaitForSeconds);
		m_Emerging = true;
		l_Anim.speed = emergeSpeed;
		m_particleSystem.SetActive (true);
		gameObject.layer = LayerMask.NameToLayer(layerAfterEmerging);
		if (m_ChaseState.canMove)
		    m_Agent.enabled = true;
        m_CharController.enabled = true;
        m_Collider.enabled = true;
		MakeVulnerable ();
        m_IsTarget.SetTargeteable(true);
		StartCoroutine(FinishEmergeAnimation());
		yield return null;
	}

	private void MakeInvulnerable() {
		if (invulnerableBeforeEmerging) {
			m_Health.invincible = true;
		}
	}

	private void MakeVulnerable() {
		if (invulnerableBeforeEmerging) {
			m_Health.invincible = false;
		}
	}
}
