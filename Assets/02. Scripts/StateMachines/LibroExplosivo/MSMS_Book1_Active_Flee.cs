﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class MSMS_Book1_Active_Flee : MonoStateMachineState
{
	private UnityEngine.AI.NavMeshAgent m_Agent;

	public float distanceToFlee = 8f;
	public float maxFleeDistance = 10f;

	private Vector3 lastPositionToFlee;
	private float threshold = 4f;
	private Vector3 finalDestination = Vector3.zero;

	public float maxFleeTime = 2f;
	private float timer;

	public override void StateUpdate()
	{
		timer += Time.deltaTime;
		Vector3 pos2d = transform.position;
		pos2d.y = 0f;
		Vector3 final2d = finalDestination;
		final2d.y = 0f;
		float distance = Vector3.Distance (pos2d, final2d);
		if (distance < threshold || timer > maxFleeTime) {
			ChangeState ();
		}
	}

	public override void OnEnter()
	{
		if (!CheckFleeDistance ()) {
			ChangeState ();
		} else {
			timer = 0f;
			FleeData fd = GetFleeDestination ();
			if (fd.success) {
				m_Agent.SetDestination (fd.target);
			}
		}
        AkSoundEngine.PostEvent("BookFlapping", gameObject);
	}

	bool CheckFleeDistance() {
		List<GameObject> players = CustomTagManager.GetObjectsByTag ("Player");
		float minDistance = distanceToFlee + 1f;
		Vector3 pos2d = transform.position;
		pos2d.y = 0f;
		foreach (GameObject player in players) {
			Vector3 playerpos2d = player.transform.position;
			playerpos2d.y = 0f;
			float distance = Vector3.Distance (player.transform.position, transform.position);
			if (distance < minDistance) {
				minDistance = distance;
				lastPositionToFlee = player.transform.position;
			}
		}
		return minDistance < distanceToFlee;
	}

	struct FleeData {
		public Vector3 target;
		public bool success;
	}

	FleeData GetFleeDestination() {
		FleeData fd;
		fd.success = false;
		float fleeDistance = Random.Range (0.5f, 1f) * maxFleeDistance;
		Vector3 pos2d = transform.position;
		pos2d.y = 0f;
		Vector3 target2d = lastPositionToFlee;
		target2d.y = 0f;
		Vector3 direction = (pos2d - target2d).normalized;
		Vector3 finalTarget = pos2d + direction * fleeDistance;
		finalTarget.y = transform.position.y + 2f;
		RaycastHit hit;
		if (Physics.Raycast (finalTarget, Vector3.down, out hit, 3.0f)) {
			fd.success = true;
			fd.target = hit.point;
			finalDestination = hit.point;
		}
		return fd;
	}

	public void ChangeState()
	{
		m_Parent.ChangeState("Active_Chase");
	}

	public override void OnExit()
	{
		
	}

	public override void OnStart()
	{
		m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
	}

}
