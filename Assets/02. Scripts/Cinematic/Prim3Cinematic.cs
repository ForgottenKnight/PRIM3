﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prim3Cinematic : MonoBehaviour {
	public Animator anim;
	public Animator noxAnimator;

	public PrimeCamera primeCamera;
	public Camera cinematicCamera;
	string currentState;

	private bool m_AcceptInput;

	private bool m_FollowStopped;

	private Canvas[] m_canvas;

	public Transform playerContainer;
	public Transform nox;
	public SkinnedMeshRenderer noxMeshRenderer;

	public Transform mockNox;
	public Transform mockPlayers;

	// Use this for initialization
	void Start () {
		foreach (AnimationClip a in anim.runtimeAnimatorController.animationClips) {
			foreach (AnimationEvent ev in a.events) {
				ev.messageOptions = SendMessageOptions.DontRequireReceiver;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (m_AcceptInput == true && Input.GetAxis("Cancel") > 0.0f) {
			//FinishAnimation();
		}
	}

	public void PlayAnimation(string state)	{
		if (anim) {
			Input.ResetInputAxes();
			cinematicCamera.enabled = true;
			primeCamera.GetComponent<Camera> ().enabled = false;
			m_FollowStopped = primeCamera.followStoppedPlayers;
			primeCamera.followStoppedPlayers = true;
			StopEnemies();
			StopPlayers();
			DestroyProjectiles ();
			anim.speed = 0.2f;
			currentState = state;
			CameraFade cf = gameObject.AddComponent<CameraFade>();
			cf.AddCallback(UnFade1);
			cf.SetScreenOverlayColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
			cf.StartFade(Color.black, 2.0f);
			StopCanvas();
		}
	}

	public void UnFade1() {
		playerContainer.gameObject.SetActive (false);
		CameraFade cf = gameObject.AddComponent<CameraFade>();
		cf.AddCallback(Play);
		cf.SetScreenOverlayColor(Color.black);
		cf.StartFade(new Color(0.0f, 0.0f, 0.0f, 0.0f), 2.0f);
		//cinematicCamera.enabled = false;
		anim.Play (currentState);
		anim.speed = 0.0f;
	}

	public void Play() {
		m_AcceptInput = true;
		//anim.Play(currentState);
		anim.speed = 0.1f;
	}

	public void FinishAnimation() {
		if (currentState != "") {
			//mockNox.gameObject.SetActive (false);
			//mockPlayers.gameObject.SetActive (false);
			m_AcceptInput = false;
			CameraFade cf = gameObject.AddComponent<CameraFade> ();
			cf.AddCallback (UnFade2);
			cf.SetScreenOverlayColor (new Color (0.0f, 0.0f, 0.0f, 0.0f));
			cf.StartFade (Color.black, 2.0f);
		}

	}

	public void UnFade2() {
		playerContainer.gameObject.SetActive (true);
		nox.gameObject.SetActive (true);
		nox.GetComponent<MonoStateMachineSystem> ().Unpause ();
		anim.speed = 50.0f;
		currentState = "";
		CameraFade cf = gameObject.AddComponent<CameraFade>();
		cf.AddCallback(ResetCamera);
		cf.SetScreenOverlayColor(Color.black);
		cf.StartFade(new Color(0.0f, 0.0f, 0.0f, 0.0f), 2.0f);

		//Destroy (gameObject);
	}

	public void ResetCamera() {
		StartPlayers ();
		primeCamera.followStoppedPlayers = m_FollowStopped;
		StartEnemies ();
		//gameObject.SetActive (false);
		StartCanvas();
		primeCamera.GetComponent<Camera> ().enabled = true;
		cinematicCamera.enabled = false;
		gameObject.SetActive (false);
	}

	public void StopEnemies() {
		List<GameObject> enemies = CustomTagManager.GetObjectsByTag("Enemy");
		foreach (GameObject enemy in enemies) {
			IPausable pausable = enemy.GetComponent<IPausable> ();
			if (pausable != null) {
				pausable.Pause ();
			}
		}
	}

	public void StopCanvas()
	{
		m_canvas = FindObjectsOfType<Canvas>();
		foreach (Canvas canvas in m_canvas)
		{
			if (canvas && canvas.gameObject.tag != "ContainsPause")
			{
				canvas.enabled = false;
			}
		}
	}

	public void StartCanvas()
	{
		foreach (Canvas canvas in m_canvas)
		{

			if (canvas)
			{
				canvas.enabled = true;
				if (canvas.transform.parent)
				{
					Health l_H = canvas.transform.parent.GetComponent<Health>();

					if (l_H && l_H.health >= l_H.maxHealth)
					{
						canvas.enabled = false;
					}
				}
			}
		}
	}

	public void StartEnemies() {
		List<GameObject> enemies = CustomTagManager.GetObjectsByTag("Enemy");
		foreach (GameObject enemy in enemies) {
			IPausable pausable = enemy.GetComponent<IPausable> ();
			if (pausable != null) {
				pausable.Unpause ();
			}
		}
	}

	public void StopPlayers() {
		List<GameObject> players = CustomTagManager.GetObjectsByTag ("Player");
		foreach (GameObject player in players) {
			if (player.name == "Salt")
			{
				SaltController l_SC = player.GetComponent<SaltController>();
				if(l_SC && l_SC.isActionActive())
				{
					l_SC.DeactivateShield();
				}
			}
			IPausable[] l_Pausables = player.GetComponents<IPausable> ();
			for (int j = 0; j < l_Pausables.Length; ++j) {
				l_Pausables [j].Pause ();
			}
			player.GetComponent<Health>().invincible = true;
		}
	}

	public void StartPlayers() {
		List<GameObject> players = CustomTagManager.GetObjectsByTag ("Player");
		foreach (GameObject player in players) {
			IPausable[] l_Pausables = player.GetComponents<IPausable> ();
			for (int j = 0; j < l_Pausables.Length; ++j) {
				if (PauseGameController.Paused) {
					l_Pausables [j].Unpause ();
					//l_Pausables [j].RemoveStack();
				} else {
					l_Pausables [j].Unpause ();
				}
			}
			player.GetComponent<Health> ().invincible = false;
		}
	}

	public void CallEvent(string ev) {
		if (SimpleEvent.eventsDictionary.ContainsKey (ev)) {
			SimpleEvent.eventsDictionary [ev].ExternalTriggerFunction ();
		}
	}

	public void ActivateEventTimer(string ev) {
		if (SimpleEvent.eventsDictionary.ContainsKey (ev)) {
			SimpleEvent.eventsDictionary [ev].ActivateTimer ();
		}
	}

	public void SetNoxTrigger(string trigger) {
		noxAnimator.SetTrigger (trigger);
	}

	public void SetNoxBoolTrue(string param) {
		noxAnimator.SetBool (param, true);
	}

	public void SetNoxBoolFalse(string param) {
		noxAnimator.SetBool (param, false);
	}

	public void NoxPlayAnimation (string animation) {
		noxAnimator.Play (animation);
	}

	public void HideNox() {
		noxMeshRenderer.enabled = false;
	}

	public void ShowNox() {
		noxMeshRenderer.enabled = true;
	}

	public void DestroyProjectiles() {
		List<GameObject> projectiles = CustomTagManager.GetObjectsByTag ("Projectile");
		foreach (GameObject projectile in projectiles) {
			Destroy (projectile);
		}
	}

	public void ChangeOwlSwitch() {
		AkSoundEngine.SetSwitch ("NoxSteps", "Owl", noxAnimator.transform.parent.gameObject);
	}
}
