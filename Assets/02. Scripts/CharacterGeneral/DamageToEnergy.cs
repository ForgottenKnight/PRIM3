﻿using UnityEngine;
using System.Collections;

public class DamageToEnergy : Damageable {
	[HideInInspector]
	public Energy source;
	[HideInInspector]
	public float energyPerDamageRatio;

    private GeneralPlayerController m_gc;

    void Start()
    {
        m_gc = transform.parent.GetComponent<GeneralPlayerController>();
    }

	override public float Damage(float damage) {
		Energy e = source;

        if (m_gc)
        {
            StaticStatistics.damageAvoided[m_gc.player] += damage;
        }

		if (!e.ConsumeEnergy (energyPerDamageRatio * damage)) {
			e.ConsumeAllEnergy();
		}
		return damage;
	}

	override public float GetHealth() {
		return source.energy;
	}
}
