﻿using UnityEngine;
using System.Collections;

public interface IPausable {
	void Pause();
	void Unpause();
	void AddStack();
	void RemoveStack();
}
