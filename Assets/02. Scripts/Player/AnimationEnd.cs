﻿using UnityEngine;
using System.Collections;
using AK.Wwise;

public class AnimationEnd : MonoBehaviour {
	Animator anim;
    public Jump jumpComponent;

	void Start() {
		anim = GetComponent<Animator> ();
	}

	public void EndAnimation(string name) {
		//Debug.Log ("Ended animation");
		anim.SetBool (name, false);
	}

    public void EndJump()
    {
        AkSoundEngine.PostEvent("EndJump", transform.parent.gameObject);
    }

    public void ChangeJumpSoundBool()
    {
        jumpComponent.canMakeSound = true;
    }
}
