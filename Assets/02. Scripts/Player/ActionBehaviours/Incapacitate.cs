﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Incapacitate : ActionBehaviour {
	public bool incapacitated = false;
	public bool canBeRescued = true;
	//bool Rotating = false;
	//bool Reviving = false;

    public GameObject slideChecker;
	public Animator AnimControler;
	public PlayerManager PM;

	//public float initAngle = 0.0f;
	//public float finalAngle = -90.0f;

	public GameObject RescArea;
    public float reviveTimer = 2.0f;
	public float rescueLength = 3.0f;
	public float rescueWidth = 3.0f;
	public float rescueHeight = 3.0f;

	public float fallspeed = 1.0f;

	[Range(0,1)]
	public float regainedHealth = 0.25f; //valor entre 0 y 1
	
	public Slider rescueBar;

	private bool moving = true;
	private Vector3 lastPos = default(Vector3);
	private bool charEnabled = true;
	private CharacterController charController;

	private IngameDialogManager idm;
	public float dialogProbability = 1f;
	int character;

	public GameObject reviveEffect;
	// Use this for initialization
	void Awake () {
		rescueBar.gameObject.SetActive(false);
		charController = GetComponent<CharacterController>();
		AnimControler = GetComponentInChildren<Animator> ();

		foreach (AnimationClip ac in AnimControler.runtimeAnimatorController.animationClips) {
			if (ac.name == "resurreccion") {
				float animSpeed = ac.length / reviveTimer;
				AnimControler.SetFloat ("ReviveSpeed", animSpeed);
			}
		}
	}


	void Start() {
		idm = GameObject.FindObjectOfType<IngameDialogManager> ();
		character = GetComponent<GeneralPlayerController> ().character;
	}
	
	// Update is called once per frame
	void Update () {
		if(incapacitated && moving)
		{
            gameObject.layer = LayerMask.NameToLayer("Incapacitate");
            charController.Move(-Vector3.up * 9.81f * Time.deltaTime);
			/*if(lastPos != transform.position)
			{
				PM.movement = Vector3.down * fallspeed * Time.deltaTime;
				lastPos = transform.position;
			}else{
				charController.enabled = false;
				charEnabled = false;
			}*/
		}

		if(!AnimControler.GetBool("Incap") && !AnimControler.GetBool("Reviving") && !charEnabled)
		{
			charEnabled = true;
			//charController.enabled = true;
		}
	
	}

	public void Incap()
	{
		//Rotating = true;
		Health h = transform.GetComponent<Health>();
		if (h)
			h.invincible = true;
		incapacitated = true;
        slideChecker.SetActive(false);
		AnimControler.SetBool("Incap",incapacitated);
		if (canBeRescued) {
			Debug.Log ("RescueArea");
			RescueArea ();
		}
		if (!idm.DialogShowing) {
			idm.ShowEvent ("G_P" + character.ToString() +"_5");
		}
	}

	public void BeginRevive() {

		Debug.Log ("Someone reviving");
		AnimControler.SetTrigger ("resurrect");
		AnimControler.SetBool("Incap", false);
		AnimControler.SetBool("Reviving", true);
		if (reviveEffect != null) {
			reviveEffect.SetActive (true);
		}

	}

	public void StopRevive() {
		AnimControler.SetTrigger("stopResurrect");
		if (reviveEffect != null) {
			reviveEffect.SetActive (false);
		}
	}

	public void Revive()
	{
		Health h = transform.GetComponent<Health>();
		if (h && incapacitated)
		{
			h.invincible = false;
            h.FillPercent(regainedHealth * 100f);
			//h.Heal(h.maxHealth * regainedHealth);
		}
		incapacitated = false;
        //slideChecker.SetActive(true);
		//Reviving = true;
		AnimControler.SetBool("Incap",incapacitated);
		AnimControler.SetBool("Reviving", false);
		AnimControler.Play ("Idle");
		rescueBar.gameObject.SetActive(false);
		if (reviveEffect != null) {
			reviveEffect.SetActive (false);
		}

	}

	private void RescueArea()
	{
		RescArea = GameObject.CreatePrimitive(PrimitiveType.Cube);
		RescArea.transform.localScale = new Vector3(rescueWidth, rescueHeight, rescueLength);			
		RescArea.transform.position = transform.position;
		RescArea.GetComponent<BoxCollider>().isTrigger = true;
		RescueTrigger rt = RescArea.AddComponent<RescueTrigger>();
		rt.rescueBar = rescueBar;
		rt.currentPlayer = gameObject.name;
        rt.rescueActivationTime = reviveTimer;

        RescArea.layer = LayerMask.NameToLayer("Ignore Raycast");
		RescArea.transform.SetParent(transform);
		RescArea.GetComponent<MeshRenderer>().enabled = false;
	}

	void endReviving(){	
		//Reviving = false;
		//charController.enabled = true;
        gameObject.layer = LayerMask.NameToLayer("Player");

    }


	override public bool isActionActive() {
		if (gameObject.activeSelf) {
			return (AnimControler.GetBool ("Incap") || AnimControler.GetBool ("Reviving"));
		} else
        {
            return false;
        }
	}
}
