﻿using UnityEngine;
using System.Collections;
using AK.Wwise;

public class AnimationAttack : MonoBehaviour {
	Animator anim;
    public GeneralPlayerController gc;
	public AttackNew attk;
	public Transform enemies;
    public Movement m_Movement;

	public GameObject damageParticles;

	private SuperAbility superAbility;
	private IngameDialogManager idm;
	int character;
	public float damageDialogProbability = 0.1f;
	public float killDialogProbability = 0.1f;

	void Start() {
		anim = GetComponent<Animator> ();
		enemies = GameObject.FindGameObjectWithTag ("EnemiesContainer").transform;
		superAbility = GetComponentInParent<SuperAbility> ();
		idm = GameObject.FindObjectOfType<IngameDialogManager> ();
		character = attk.GetComponent<GeneralPlayerController> ().character;
	}

	public void CalculateAttack() {
		if (attk.tp != null) {
			attk.tp.lookAtTargetRequest ();
		}

        bool l_enemyHit = false;
		
		int childCount = enemies.childCount;
		for (int i = 0; i < childCount; ++i) {
			float dmg = staticAttackCheck.checkAttack (enemies.GetChild (i), attk.transform, attk.attackLength, attk.attackAngle, attk.attackHeight, attk.attackForce + attk.chargedDamage, KillCallback, attk.damageBonus, attk.currentPhase);
			if (dmg > 0.0f) {
                l_enemyHit = true;
				attk.OnAttackHit (enemies.GetChild (i).gameObject, dmg);
                StaticStatistics.damageDone[gc.player] += dmg;
				if (damageParticles != null) {
					Vector3 rotForward = enemies.GetChild(i).position - transform.position;
					rotForward.y = 0f;
					Quaternion q = Quaternion.LookRotation (rotForward) * Quaternion.Euler (0f, 180f, 0f);
					Instantiate (damageParticles, enemies.GetChild (i).position, q);
                }
				idm.ShowIfNotShowing ("G_P" + character.ToString () + "_4", damageDialogProbability);
            }
        }
        attk.chargedDamage = 0.0f;
        if (!l_enemyHit)
        {
            ++StaticStatistics.attackMissed[gc.player];
            AkSoundEngine.PostEvent("MissedAttack", transform.parent.gameObject);
			attk.OnAttackMiss ();
        }
        else
        {
            ++StaticStatistics.attackHit[gc.player];
        }
    }

	public void KillCallback() 
	{
		idm.ShowIfNotShowing ("G_P" + character.ToString() + "_1", killDialogProbability);
	}

    public void SlowDown()
    {
        m_Movement.currentSpeed = 0.0f;
    }

	public void DoSuperAbility() {
		superAbility.SetMark ();
	}


}
