﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using XboxCtrlrInput;
using PRIM3;
using AK.Wwise;
using UnityEngine.SceneManagement;

[AddComponentMenu("PRIM3/CharacterScripts/Core/Player General Controller")] 
public class GeneralPlayerController : GeneralController, IPausable {
	[Header("Prefabs")]
	public GameObject salt;
	public GameObject sulphur;
	public GameObject mercury;
	private GameObject prefab;
    public GameObject[] characters;// 0 -> Salt, 1 -> Sulphur, 2-> Mercury
    public Animator animController;

    public XboxController controller;
	public bool useKeyboard;

    [HideInInspector]
    public List<TargetSelector> EnemiesTargeting;

	public enum CharacterSelection {
		Next,
		Previous,
		Reinit
	}

	[Header("UI References")]
	public CharactersUIManager charactersUIManager;

	[Header("Configuration")]
	public int character = 0;
    //private int m_ChangeCharacter = 0;
	public int player = 0;
	public int UI = 0;
    public Transform targetController;
	protected string characterName = "";
	public Transform playerContainer;
	[HideInInspector]
	public bool canInput = true;

	Incapacitate incap;

    private int m_PauseCount = 0;
    private string m_SceneName;

	// Use this for initialization
	void Start () {

	    //charactersUIManager.setActiveCharacterUI (UI, true);
		GetComponent<Health> ().setUIType (Health.UI_Type.MainUI); // Tipo de UI: MainUI - La UI con los portraits y probetas con vida y energia, GameUI - Barras de vida de los enemigos del escenario
        incap = gameObject.GetComponent<Incapacitate>();
		transform.parent  = GameObject.FindGameObjectWithTag ("PlayerContainer").transform;
        //m_ChangeCharacter = character;
		animController.SetInteger ("Character", character);
        Scene l_Scene = SceneManager.GetActiveScene();
        m_SceneName = l_Scene.name;
        if (m_SceneName == "Level3Full")
        {
            AkSoundEngine.SetSwitch("Level", "Level3", gameObject);
        }
        else
        {
            AkSoundEngine.SetSwitch("Level", "Level1_2", gameObject);
        }
	}

	// Update is called once per frame
	void Update () {
		if (canInput) {
			bool changed = false;
			int auxChar = character;
			Jump jump = GetComponent<Jump> ();
			bool jumping = false;
			bool incapacitated = false;
			if (jump)
				jumping = jump.isJumpingOrFalling ();
			if (incap)
				incapacitated = incap.isActionActive ();
			if (!jumping && !incapacitated) {

				if (useKeyboard) { //Si usa teclado se ignora el controller

					if (Input.GetButtonDown ("ChangePlayerRight0")) {
						ChangeCharacter (CharacterSelection.Next);
					} else if (Input.GetButtonDown ("ChangePlayerLeft0")) {
						ChangeCharacter (CharacterSelection.Previous);
					}
				} else {
					if (XCI.GetButtonDown (XboxButton.RightBumper, controller)) {
						ChangeCharacter (CharacterSelection.Next);
					} else if (XCI.GetButtonDown(XboxButton.LeftBumper, controller)) {
						ChangeCharacter (CharacterSelection.Previous);
					}
				}
			}
		}
			
		StaticStatistics.useTime [player, character] += Time.deltaTime;
	}


	bool CharacterExist(int chara) {
        return characters[chara].activeSelf;
	}

	bool AllPlayers() {
		GameObject [] players = GameObject.FindGameObjectsWithTag ("Player");
		return players.Length >= 3;
	}

	public void ReInitCharacter() {
		ChangeCharacter (CharacterSelection.Reinit);
	}

    void ChangeEnemiesTarget(GameObject newChar, GeneralPlayerController newCharController)
    {
        List<TargetSelector> enemies = newCharController.EnemiesTargeting;
        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].currentTarget.player = newCharController.player;
            enemies[i].currentTarget.inc = newChar.GetComponent<Incapacitate>();
            enemies[i].currentTarget.target = newChar;
        }
    }
	

	private int CharacterMod(int a, int b)
	{
		return (a % b + b) % b;
	}

	void ChangeCharacter(CharacterSelection aSelection)
	{
		bool l_found = false;
		int l_newCharacter = character;

		if (aSelection != CharacterSelection.Reinit) {			
			int l_iterations = 0;


			while (!l_found && l_iterations <= StaticParemeters.numPlayers) {
				if (aSelection == CharacterSelection.Next) {
					l_newCharacter = CharacterMod (l_newCharacter + 1, 3);
				} else {
					l_newCharacter = CharacterMod (l_newCharacter - 1, 3);
				}
				if (!CharacterExist (l_newCharacter)) {
					l_found = true;
				}
				l_iterations++;
			}
		} else {
			l_found = true;
		}

		if (l_found) {
			GameObject newChar = characters [l_newCharacter];
			newChar.transform.position = transform.position;
			newChar.transform.rotation = transform.rotation;

			GeneralPlayerController newCharController = newChar.GetComponent<GeneralPlayerController> ();
			newCharController.player = player;
			for (int i = 0; i < EnemiesTargeting.Count; ++i) {
				if (EnemiesTargeting [i] != null) {
					EnemiesTargeting [i].currentTarget.player = newCharController.player;
					EnemiesTargeting [i].currentTarget.inc = newChar.GetComponent<Incapacitate> ();
					EnemiesTargeting [i].currentTarget.target = newChar;
					newCharController.EnemiesTargeting.Add (EnemiesTargeting [i]);
				}
			}
			//newCharController.character = m_ChangeCharacter;
			//newCharController.m_ChangeCharacter = m_ChangeCharacter;
			newCharController.controller = controller;
			newCharController.useKeyboard = useKeyboard;
			charactersUIManager.changeCharacterUI (UI, newChar);
			newCharController.UI = UI;
			newCharController.charactersUIManager = charactersUIManager;
			newChar.GetComponent<ShowOffCamera> ().iconCanvas = charactersUIManager.GetComponent<Canvas> ();
			CheckpointHandler l_CPH = newChar.GetComponent<CheckpointHandler> ();
			if (l_CPH) {
				l_CPH.useCheckpoint = false;
			}
			newChar.transform.parent = gameObject.transform.parent;
			newChar.SetActive (true);

			Energy e = newChar.GetComponent<Energy> ();
			if (e) {
				Energy oldE = gameObject.GetComponent<Energy> ();
				if (oldE) {
					e.SetEnergy (oldE.GetEnergy ());
				}
			}

			Health h = newChar.GetComponent<Health> ();
			if (h) {
				Health oldH = gameObject.GetComponent<Health> ();
				if (oldH) {
					h.FillPercent (oldH.GetHealthAsPercent ());
					h.invincible = false;       
				}
			}
			TargetPointer l_TP = gameObject.GetComponent<TargetPointer> ();
			if (l_TP) {
				l_TP.active = false;
				l_TP.unlockTarget ();
			}

			ResetAnimator ();
			EnemiesTargeting.Clear ();
			AkSoundEngine.PostEvent ("StopRun", gameObject);
			gameObject.transform.parent = null;
			gameObject.SetActive (false);
		}
    }

    void ResetAnimator()
    {

        animController.Play("Idle");
        AnimatorControllerParameter[] parameters = animController.parameters;
        for (int i = 0; i < parameters.Length; i++)
        {
            AnimatorControllerParameter parameter = parameters[i];
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Int:
                    animController.SetInteger(parameter.name, parameter.defaultInt);
                    break;
                case AnimatorControllerParameterType.Float:
                    animController.SetFloat(parameter.name, parameter.defaultFloat);
                    break;
                case AnimatorControllerParameterType.Bool:
                    animController.SetBool(parameter.name, parameter.defaultBool);
                    break;
                case AnimatorControllerParameterType.Trigger:
                    animController.ResetTrigger(parameter.name);
                    break;
            }
        }
    }

	void ChangeCharacterPrefabs() {
		switch (character) {
			case 0: // Salt
				prefab = salt;
				characterName = "Salt";
				break;
			case 1: // Sulphur
				prefab = sulphur;
				characterName = "Sulphur";
				break;
			case 2: // Mercury
				prefab = mercury;
				characterName = "Mercury";
				break;
		}

		GameObject[] chars = GameObject.FindGameObjectsWithTag ("Player");
		GameObject oldChar = null;
		foreach (GameObject go in chars) {
			GeneralPlayerController mc = go.GetComponent<GeneralPlayerController>();
			if (mc) {
				if (mc.player == player) {
					oldChar = go;
				}
			}
		}
		if (oldChar) {
			List<GameObject> ces = CustomTagManager.GetObjectsByTag("CameraEvent");
			for (int i = 0; i < ces.Count; ++i) {
				CameraEvent cameraEvent = ces[i].GetComponent<CameraEvent>();
				if (cameraEvent) {
					cameraEvent.DeletePlayerFromList(player, true);
				}
			}
			GameObject newChar = (GameObject)Instantiate (prefab, oldChar.transform.position, oldChar.transform.rotation);
			GeneralPlayerController newCharController = newChar.GetComponent<GeneralPlayerController> ();
			newCharController.player = player;
			newCharController.character = character;
            newCharController.controller = controller;
			newChar.name = characterName;
			charactersUIManager.changeCharacterUI( UI, newChar);
			newCharController.UI = UI;
			newCharController.charactersUIManager = charactersUIManager;
			newChar.GetComponent<ShowOffCamera>().iconCanvas = charactersUIManager.GetComponent<Canvas>();
			newChar.SetActive(true);

			Energy e = newChar.GetComponent<Energy>();
			if (e) {
				Energy oldE = oldChar.GetComponent<Energy>();
				if (oldE) {
					e.SetEnergy(oldE.GetEnergy());
				}
			}

			Health h = newChar.GetComponent<Health>();
			if (h) {
				Health oldH = oldChar.GetComponent<Health>();
				if (oldH) {
					h.FillPercent(oldH.GetHealthAsPercent());
				}
			}
				
			incap = newChar.GetComponent<Incapacitate>();

			DestroyImmediate (oldChar.gameObject);
			Destroy (oldChar.gameObject);
		}
	}

	#region IPausable implementation

	public void Pause ()
	{
		canInput = false;
		m_PauseCount++;
        AkSoundEngine.PostEvent("PauseAllEvents", gameObject);
	}

	public void Unpause ()
	{
		m_PauseCount--;
		if (m_PauseCount <= 0) {
			canInput = true;
			m_PauseCount = 0;
            AkSoundEngine.PostEvent("ResumeAllEvents", gameObject);
		}
	}

	public void AddStack ()
	{
		m_PauseCount++;
	}

	public void RemoveStack ()
	{
		m_PauseCount--;
	}

	#endregion


}
