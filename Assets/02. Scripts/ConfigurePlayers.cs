﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class ConfigurePlayers : MonoBehaviour {

	public GeneralPlayerController[] charactersGPC; //0->Salt, 1->Sulphur, 2->Mercury

    public PlayerDeactivator playerDeactiv;

	void Start() {
		if (!StaticParemeters.savedConfiguration) { //DEBUG Purpouses
			Debug.Log("No se ha seleccionado el número de jugadores desde el menu. Se ha inicializado a Salt como Default");
			charactersGPC [0].gameObject.SetActive (true);
			charactersGPC [0].charactersUIManager.setActiveCharacterUI (0, true);
			charactersGPC [0].charactersUIManager.changeCharacterUI (0, charactersGPC [0].gameObject);

			for (int i = 1; i < charactersGPC.Length; i++) {
				charactersGPC [i].transform.parent = null;
				charactersGPC [i].gameObject.SetActive (false);
				Debug.Log ("Character deactivated");
			}
		} else {
			for (int i = 0; i < charactersGPC.Length; i++) {	
				if (StaticParemeters.activeCharacters [i]) {
					charactersGPC [i].gameObject.SetActive (true);
					charactersGPC [i].character = i; //Algo así como el ID. Hardcoded en cada pj;
					charactersGPC [i].player = StaticParemeters.players [i];
					charactersGPC [i].UI = StaticParemeters.players [i];
					charactersGPC [i].controller = StaticParemeters.playerControllers [i];
					charactersGPC [i].charactersUIManager.setActiveCharacterUI (charactersGPC [i].player, true);
					charactersGPC [i].charactersUIManager.changeCharacterUI (charactersGPC [i].player, charactersGPC [i].gameObject);
					Debug.Log ("Character activated");
				} else {
					charactersGPC [i].transform.parent = null;
					charactersGPC [i].gameObject.SetActive (false);
					Debug.Log ("Character deactivated");
				}
			}
		}

		if (StaticParemeters.useKeyboard) {
			charactersGPC [StaticParemeters.playerWithKeyboard].useKeyboard = true;
		}
	}

	/*void Start() {
	}*/
	/*
	// Use this for initialization
	void Start () {

        GeneralPlayerController l_SaltGPC = Salt.GetComponent<GeneralPlayerController>();
        GeneralPlayerController l_SulphurGPC = Sulphur.GetComponent<GeneralPlayerController>();
        GeneralPlayerController l_MercuryGPC = Mercury.GetComponent<GeneralPlayerController>();

        if (!StaticParemeters.savedConfiguration)
        {
            l_SaltGPC.player = 0;
            l_SaltGPC.UI = 0;
            if (StaticParemeters.useKeyboard)
            {
                l_SulphurGPC.controller = XboxController.First;
                l_MercuryGPC.controller = XboxController.Second;
            }
            else
            {
                l_SaltGPC.controller = XboxController.First;
                l_SulphurGPC.controller = XboxController.Second;
                l_MercuryGPC.controller = XboxController.Third;
            }
            l_SulphurGPC.player = 1;
            l_SulphurGPC.UI = 1;
            l_MercuryGPC.player = 2;
            l_MercuryGPC.UI = 2;

         //   Salt.SetActive(true);
          //  Sulphur.SetActive(true);
           // Mercury.SetActive(true);


            switch (StaticParemeters.numPlayers)
            {
                case 1:
                    StaticParemeters.activeCharacters[0] = true;
                    StaticParemeters.characters[0] = 0;
                    l_SaltGPC.charactersUIManager.setActiveCharacterUI(0, true);
                    //Salt.SetActive(true);
                    break;
                case 2:
                    StaticParemeters.activeCharacters[0] = true;
                    StaticParemeters.characters[0] = 0;
                    l_SaltGPC.charactersUIManager.setActiveCharacterUI(0, true);

                    StaticParemeters.activeCharacters[1] = true;
                    StaticParemeters.characters[1] = 1;
                    l_SulphurGPC.charactersUIManager.setActiveCharacterUI(1, true);


                   // Salt.SetActive(true);
                  //  Sulphur.SetActive(true);
                    break;
                case 3:
                    StaticParemeters.activeCharacters[0] = true;   
                    StaticParemeters.characters[0] = 0;
                    l_SaltGPC.charactersUIManager.setActiveCharacterUI(0, true);

                    StaticParemeters.activeCharacters[1] = true;  
                    StaticParemeters.characters[1] = 1;
                    l_SulphurGPC.charactersUIManager.setActiveCharacterUI(1, true);

                    StaticParemeters.activeCharacters[2] = true;
                    StaticParemeters.characters[2] = 2;
                    l_MercuryGPC.charactersUIManager.setActiveCharacterUI(2, true);

                  //  Salt.SetActive(true);
                  //  Sulphur.SetActive(true);
                  //  Mercury.SetActive(true);
                    break;
            }
        }
        else
        {

          //  Salt.SetActive(true);
           // Sulphur.SetActive(true);
            //Mercury.SetActive(true);


            for (int i = 0; i < StaticParemeters.activeCharacters.Length; ++i)
            {
                if(StaticParemeters.activeCharacters[i])
                {
                    switch(i)
                    {
                        case 0:
                            CharactersUIManager l_SaltcharactersUIManager = l_SaltGPC.charactersUIManager;
                            l_SaltGPC.player = StaticParemeters.characters[0];
                            l_SaltGPC.UI = StaticParemeters.characters[0];
                            l_SaltGPC.controller = StaticParemeters.playerControllers[0];
                           // Salt.SetActive(true);
                            l_SaltcharactersUIManager.changeCharacterUI(l_SaltGPC.UI, Salt);

                            break;
                        case 1:
                            CharactersUIManager l_SulphurcharactersUIManager = l_SulphurGPC.charactersUIManager;
                            l_SulphurGPC.player = StaticParemeters.characters[1];
                            l_SulphurGPC.UI = StaticParemeters.characters[1];
                            l_SulphurGPC.controller = StaticParemeters.playerControllers[1];
                          //  Sulphur.SetActive(true);
                            l_SulphurcharactersUIManager.changeCharacterUI(l_SulphurGPC.UI, Sulphur);

                            break;
                        case 2:
                            CharactersUIManager l_MercurycharactersUIManager = l_MercuryGPC.charactersUIManager;
                            l_MercuryGPC.player = StaticParemeters.characters[2];
                            l_MercuryGPC.UI = StaticParemeters.characters[2];
                            l_MercuryGPC.controller = StaticParemeters.playerControllers[2];
                           // Mercury.SetActive(true);
                            l_MercurycharactersUIManager.changeCharacterUI(l_MercuryGPC.UI, Mercury);
                            break;
                    }
                }
            }
				
        }

        playerDeactiv.DeactivatePlayers();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	*/
    public void SaveParameters()
    {
        //StaticParemeters.SaveConfiguration(Salt, Sulphur,Mercury);
    }
}
