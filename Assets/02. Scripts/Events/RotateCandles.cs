﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCandles : MonoBehaviour {

    public bool activateRotation = false;
    public float rotationSpeed = 1f;
    public bool increasingSpeed = false;
    public float maxRotationSpeed = 100f;
    public float speedIncreasedBy = 5f;
    public float timerToIncreaseSpeed = 2.5f;
    private float timer = 0.0f;

	public ParticleSystem[] fires;
	public bool unscaled = false;
	public GameObject lights;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(activateRotation)
        {
            Rotate();
			if (unscaled) {
				timer += Time.unscaledDeltaTime;
				UpdateParticles ();
			} else {
				timer += Time.deltaTime;
			}
            if(increasingSpeed && timer >= timerToIncreaseSpeed)
            {
                timer = 0f;
                rotationSpeed += speedIncreasedBy;
                if(rotationSpeed >= maxRotationSpeed)
                {
                    rotationSpeed = maxRotationSpeed;
                    increasingSpeed = false;
                }
            }
        }
	}

    public void Activate()
    {
        activateRotation = true;
		lights.SetActive (true);
    }

	public void UpdateParticles() {
		foreach (ParticleSystem ps in fires) {
			ps.Simulate (Time.unscaledDeltaTime, true, false);
		}
	}

	public void SetUnscale() {
		unscaled = true;
		foreach (ParticleSystem ps in fires) {
			ps.Play ();
		}
	}

    private void Rotate()
    {
        Vector3 newRot = transform.rotation.eulerAngles;
		if (unscaled) {
			newRot.y += Time.unscaledDeltaTime * rotationSpeed;
		} else {
			newRot.y += Time.deltaTime * rotationSpeed;
		}
        transform.localEulerAngles = newRot;
    }
}
