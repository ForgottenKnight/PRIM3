﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayersPosition : MonoBehaviour {

    public Transform playerContainer;
    public Transform centerPosition;
    public float distanceBetweenPlayers = 5;

	public void SetPositions()
    {
        for (int i = 0; i< playerContainer.childCount; ++i)
        {
            Vector3 pos = centerPosition.transform.position;
            pos.x = pos.x + distanceBetweenPlayers * i;
            centerPosition.transform.position = pos;

            distanceBetweenPlayers *= -1;

            playerContainer.GetChild(i).transform.position = centerPosition.transform.position;
        }
    }
}
