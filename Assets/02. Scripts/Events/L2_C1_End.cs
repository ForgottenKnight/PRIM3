﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L2_C1_End : MonoBehaviour {
	public L2_C1 l2c1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player") || col.gameObject.layer == LayerMask.NameToLayer ("JumpingPlayer")) {
			l2c1.OnEnd (col.gameObject);
		}
	}
}
