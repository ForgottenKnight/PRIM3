﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using AK.Wwise;

public class PortalScript : MonoBehaviour {
	public string scene = "MainMenu";
	public StatisticsController statistics;
    public ConfigurePlayers PlayerConfiguration;
    private bool m_Activated = false;
    private bool m_NextLevel = false;
    AsyncOperation m_async;

	public bool hasSound = true;
    string m_SceneName;

	// Use this for initialization
	void Start () {
        m_Activated = false;
		if (hasSound) {
			AkSoundEngine.PostEvent ("Portal", gameObject);
		}
        
        Scene l_Scene = SceneManager.GetActiveScene();
        m_SceneName = l_Scene.name;
	}
	
	// Update is called once per frame
	void Update () {
        if(m_Activated)
        {
            if(m_async.progress >= 0.9f)
            {
                m_Activated = false;
				statistics.ShowContinue ();
            }
        }
        else
        {
            if(CheckNextLevel() && m_async != null)
            {
                m_async.allowSceneActivation = true;
				StaticStatistics.nextLevel = false;
            }
        }
	
	}

    IEnumerator LoadLevel()
    {
        m_async = SceneManager.LoadSceneAsync(scene);
        m_async.allowSceneActivation = false;
        yield return m_async;
        Debug.Log("Loading complete");
    }

    public bool CheckNextLevel()
    {
		return StaticStatistics.nextLevel;
    }

	void OnTriggerEnter(Collider col) 
	{
		if(col.tag == "Player" && !m_Activated)
		{
			SimpleEvent.ClearDictionary();
            if (PlayerConfiguration)
            {
               PlayerConfiguration.SaveParameters();
            }
			if(scene == "MainMenu")
				FinishGame.ReturnToMenu();

			ReestartCheckpoint.haveCheckpoint = false;
            m_Activated = true;
            AkSoundEngine.PostEvent("StopPortal", gameObject);

            if (m_SceneName == "Level1Full")
            {
                AkSoundEngine.PostEvent("StopLevel1", gameObject);
            }
            else if (m_SceneName == "Level2Full")
            {
                AkSoundEngine.PostEvent("StopLevel2", gameObject);
            }

			if (statistics)
            {
				statistics.ShowStatistics ();
                StaticStatistics.restartLevel = false;
            }
            StartCoroutine("LoadLevel");
		}
	}

	public void ExternalActivation() {
		if (!m_Activated) {
			SimpleEvent.ClearDictionary ();
			if (PlayerConfiguration) {
				PlayerConfiguration.SaveParameters ();
			}
			if (scene == "MainMenu")
				FinishGame.ReturnToMenu ();

			ReestartCheckpoint.haveCheckpoint = false;
			m_Activated = true;
			if (statistics) {
				statistics.ShowStatistics ();
				StaticStatistics.restartLevel = false;
			}
			StartCoroutine ("LoadLevel");
		}
	}
}
