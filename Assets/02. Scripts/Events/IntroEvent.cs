﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroEvent : MonoBehaviour {
    public IntroManager m_IntroManager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateEvent()
    {
        if (m_IntroManager != null)
        {
            m_IntroManager.activateLevelLoad = true;
        }
    }
}
