﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L2_C1_VoidFall : MonoBehaviour {
	public L2_C1 l2c1;

	// Use this for initialization
	void Start () {
		gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player") || col.gameObject.layer == LayerMask.NameToLayer ("JumpingPlayer")) {
			l2c1.OnPlayerFall (col.gameObject);
		}
	}
}
