﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L2_C1 : MonoBehaviour {

	public GameObject fallToVoid;

	public GameObject fallToVoidtL2C1;
	public GameObject start;
	public GameObject end;

	public PlayerStatusManager playerStatusM;
	public PlatformAnimatorGroup C1Group;

	public bool started = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (started) {
			if (playerStatusM.GetNumOfDeadPlayers () == StaticParemeters.numPlayers) { //Si mueren todos se les revive
				List<GameObject> deadPlayers = playerStatusM.GetDeadPlayers();
				for (int i = 0; i < deadPlayers.Count; i++) {
					Incapacitate l_incapacitate = deadPlayers [i].GetComponent<Incapacitate> ();
					if (l_incapacitate != null) {
						//Debug.Log ("Revivido " + l_incapacitate.gameObject.name);
						l_incapacitate.Revive ();
					}
				}
				C1Group.InitializeGroup ();
			}
		}
	}

	public void OnPlayerFall(GameObject player) {  //Si cae alguien muere y se le transporta al inicio
		//Debug.Log (player.name + " ha caido");
		Health l_health = player.GetComponent<Health> ();
		if (l_health != null) {
			l_health.RemoveHealth ();
		}
		player.transform.position = start.transform.position;
	}

	public void OnStart(GameObject player) { //Alguien activa el inicio del "Puzzle". El sistema de FallToVoid se cambia por uno nuevo.
		if (!started) {
			List<GameObject> l_players = playerStatusM.GetPlayers ();
			for (int i = 0; i < l_players.Count; i++) {
				l_players [i].GetComponent<Incapacitate> ().canBeRescued = false;
			}

			fallToVoid.SetActive (false);
			fallToVoidtL2C1.SetActive (true);
			playerStatusM.endGameOnAllDead = false;
			started = true;
		}
	}

	public void OnEnd(GameObject player) { //Alguien alcanza el final del "Puzzle". Se revive a todos los muertos y se les transporta al final
		List<GameObject> deadPlayers = playerStatusM.GetDeadPlayers();

		for (int i = 0; i < deadPlayers.Count; i++) {
			Incapacitate l_incapacitate = deadPlayers [i].GetComponent<Incapacitate> ();
			if (l_incapacitate != null) {
				l_incapacitate.Revive ();
			}
		}

		List<GameObject> l_players = playerStatusM.GetPlayers ();
		for (int i = 0; i < l_players.Count; i++) {
			if (l_players [i] != player) {
				l_players [i].transform.position = end.transform.position;
			}
			l_players [i].GetComponent<Incapacitate> ().canBeRescued = true;
		}

		Completed ();
		started = false;
	}

	public void Completed() { //Se desactivan todos los GameObjects relacionados con el puzzle y se activa el sistema FallToVoid
		playerStatusM.endGameOnAllDead = true;
		fallToVoid.SetActive (true);
		Destroy (fallToVoidtL2C1);
		Destroy (start);
		Destroy (end);
		C1Group.DisableGroupAnimators ();
		Destroy (this);
	}
}
