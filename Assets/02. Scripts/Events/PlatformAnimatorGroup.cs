﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformAnimatorGroup : MonoBehaviour {
	//[HideInInspector]
	public List<PlatformAnimator> group = new List<PlatformAnimator>();

	// Use this for initialization
	void Start () {
		for (int i = 0; i < transform.childCount; i++) {
			PlatformAnimator l_platAnim = transform.GetChild (i).GetComponent<PlatformAnimator> ();
			if (l_platAnim != null) {
				group.Add (l_platAnim);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void InitializeGroup() {
		//Debug.Log ("Initializing group");

		for (int i = group.Count - 1; i > -1; i--) {
			if (group [i] != null) { 
				group [i].Init ();
			} else {
				group.RemoveAt (i);
				Debug.LogWarning ("El script PlatformAnimator no se puede reinicializar porque ha sido destruido. Asegurate de desmarcar la opcion DestroyGameobjectOnEnd de dicho Script");
		
			}
		}
	}

	public void DisableGroupAnimators() {
		for (int i = group.Count - 1; i > -1; i--) {
			if (group [i] != null) { 
				group [i].Disable ();
			} else {
				group.RemoveAt (i);
				Debug.LogWarning ("El script PlatformAnimator no se puede reinicializar porque ha sido destruido. Asegurate de desmarcar la opcion DestroyGameobjectOnEnd de dicho Script");

			}
		}
	}
}
