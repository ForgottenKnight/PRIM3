﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveElevator : MonoBehaviour {

    public GameObject objective;
    public float speed = 1f;
    public bool move = false;
    public float distanceToStop = 5f;
    public SimpleEvent simpleEventComp;

    public float timerToStartAfterActivation = 2f;
    private float timer = 0f;


    Transform playerContainer;
    CharacterController cc;

	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();
        playerContainer = GameObject.FindGameObjectWithTag("PlayerContainer").transform;
	}
	
	// Update is called once per frame
	void Update () {
        if(move)
        {
            if (timer >= timerToStartAfterActivation)
            {
                if(simpleEventComp != null)
                {
                    simpleEventComp.enabled = true;
                    simpleEventComp = null;
                }

                Vector3 mov = (objective.transform.position - transform.position).normalized;

                mov = mov * Time.deltaTime * speed;
                cc.Move(mov);
                MovePlayers(mov);
                CheckDestination();
            }
            else
            {
                timer += Time.deltaTime;
            }
        }
	}

    public void ActivateElevator()
    {
        move = true;
    }

    private void CheckDestination()
    {
        if (Vector3.Distance(transform.position, objective.transform.position) <= distanceToStop)
        {
            move = false;
        }
    }

    private void MovePlayers(Vector3 direction)
    {
        CharacterController playerCC;
        for (int i = 0; i< playerContainer.childCount; ++i)
        {
            GameObject player = playerContainer.GetChild(i).gameObject;
            player.transform.position += direction;
            playerCC = player.GetComponent<CharacterController>();
            /*if(playerCC)
            {
                playerCC.Move(direction);
            }*/
        }
    }
}
