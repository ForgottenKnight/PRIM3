﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCamera : MonoBehaviour {
	public Transform lookAtTransform;
	public float radius;
	public Vector2 radiusLimit;
	public float yaw;
	public Vector2 yawLimit;
	public float pitch;
	public Vector2 pitchLimit;
	public Vector2 timeLimit;

	public float speed = 0.5f;

	enum CurrentMovement {
		YAW = 0,
		PITCH,
		RADIUS
	}

	CurrentMovement currentMovement = CurrentMovement.RADIUS;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		DoPositionChange ();
	}

	void DoPositionChange() {
		transform.position = lookAtTransform.position;
		transform.eulerAngles = new Vector3 (pitch, yaw, 0f);
		transform.position = transform.position - transform.forward * radius;
	}

	IEnumerator GetNewValues() {
		while (true) {
			float time = Random.Range (timeLimit.x, timeLimit.y);
			StartCoroutine (Move (time));
			yield return new WaitForSecondsRealtime (time);
			Vector3 lastVector = transform.position - lookAtTransform.position;
			Vector3 currentVector = Vector3.zero;
			float angle = 0f;
			while (angle < 30f) { // Regla de los 30º
				yaw = Random.Range (yawLimit.x, yawLimit.y);
				pitch = Random.Range (pitchLimit.x, pitchLimit.y);
				radius = Random.Range (radiusLimit.x, radiusLimit.y);
				DoPositionChange();
				currentVector = transform.position - lookAtTransform.position;
				angle = Vector3.Angle (lastVector, currentVector);
			}
		}
	}

	IEnumerator Move(float time) {
		currentMovement = (CurrentMovement)Random.Range (0, 2);
		float sign = Mathf.Sign (Random.Range (-1f, 1f));
		float finalSpeed = sign * speed;
		while (time > 0f) {
			float delta = finalSpeed * Time.unscaledDeltaTime;
			switch(currentMovement) {
			case CurrentMovement.PITCH:
				if (pitch + delta < pitchLimit.y && pitch + delta > pitchLimit.x) {
					pitch += delta;
				}
				break;
			case CurrentMovement.RADIUS:
				if (radius + delta < radiusLimit.y && radius + delta > radiusLimit.x) {
					radius += delta;
				}
				break;
			case CurrentMovement.YAW:
				if (yaw + delta < yawLimit.y && yaw + delta > yawLimit.x) {
					yaw += delta;
				}
				break;
			}
			time -= Time.unscaledDeltaTime;
			yield return null;
		}
	}

	void OnDisable() {
		StopAllCoroutines ();
	}

	void OnEnable() {
		StartCoroutine (GetNewValues ());
	}
}
