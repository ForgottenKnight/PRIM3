﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCameraManager : MonoBehaviour {
	public PrimeCamera primeCamera;
	public GameObject finalEvent;
	public DialogsManager dialogManager;
	public int dialogToCall;
	public Camera finalCamera;
	public Animator noxAnimator;
	public RotateCandles rotateCandles;
	public PortalScript portal;

	private Canvas[] m_canvas;

	void Start() {
		if (primeCamera == null) {
			primeCamera = Camera.main.GetComponent<PrimeCamera>();
		}
	}

	public void BeginEndEvent() {
		finalCamera.gameObject.SetActive (true);
		noxAnimator.SetBool ("depie", true);
		rotateCandles.Activate ();
		rotateCandles.SetUnscale ();
		CameraFade cf = finalCamera.gameObject.AddComponent<CameraFade>();
		cf.AddCallback(UnFade1);
		cf.SetScreenOverlayColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
		cf.StartFade(Color.black, 2.0f);
	}

	public void UnFade1() {
		finalEvent.SetActive(true);
		DeactivatePlayers ();
		StopCanvas ();
		primeCamera.GetComponent<Camera>().enabled = false;
		CameraFade cf = finalCamera.gameObject.AddComponent<CameraFade>();
		cf.AddCallback(Play);
		cf.SetScreenOverlayColor(Color.black);
		cf.StartFade(new Color(0.0f, 0.0f, 0.0f, 0.0f), 2.0f);
	}

	public void Play() {
		StartCoroutine (PlayCoroutine ());
	}

	IEnumerator PlayCoroutine() {
		yield return new WaitForSeconds (1f);
		dialogManager.ShowDialog (dialogToCall);
		StartCoroutine (FinishDialog ());
	}

	IEnumerator FinishDialog() {
		yield return new WaitForSeconds (1f);
		portal.ExternalActivation ();
		CameraFade cf = finalCamera.gameObject.AddComponent<CameraFade>();
		cf.AddCallback(UnFade2);
		cf.SetScreenOverlayColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
		cf.StartFade(Color.black, 2.0f);
	}

	public void UnFade2() {
		StartCanvas ();
		/*CameraFade cf = finalCamera.gameObject.AddComponent<CameraFade>();
		cf.AddCallback(Stop);
		cf.SetScreenOverlayColor(Color.black);
		cf.StartFade(new Color(0.0f, 0.0f, 0.0f, 0.0f), 2.0f);*/
	}

	public void StopCanvas()
	{
		m_canvas = FindObjectsOfType<Canvas>();
		foreach (Canvas canvas in m_canvas)
		{
			if (canvas && canvas.gameObject.tag != "ContainsPause")
			{
				canvas.enabled = false;
			}
		}
	}

	public void StartCanvas()
	{
		foreach (Canvas canvas in m_canvas)
		{

			if (canvas)
			{
				canvas.enabled = true;
				if (canvas.transform.parent)
				{
					Health l_H = canvas.transform.parent.GetComponent<Health>();

                    if (l_H && (l_H.health >= l_H.maxHealth || l_H.health <= 0))
					{
						canvas.enabled = false;
					}
				}
			}
		}
	}

	public void DeactivatePlayers() {
		List<GameObject> players = CustomTagManager.GetObjectsByTag ("Player");
		foreach (GameObject player in players) {
			player.SetActive (false);
		}
	}
}
