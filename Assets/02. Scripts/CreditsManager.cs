﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour {
	public float speed = 5f;
	public float posToFinish = 2100f;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * speed * Time.deltaTime);
		if (transform.localPosition.y > posToFinish) {
			SceneManager.LoadScene ("MainMenuFull");
		}
	}
}
