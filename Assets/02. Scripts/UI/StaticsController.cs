﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticsController : MonoBehaviour
{

    public GameObject statics;
    public int numNewLines = 5;
    public GameObject stats;
    public GameObject timer;
    public GameObject[] players;// 0->Player1,1->Player2,2->Player3
    public Transform playerContainer;
    public GameObject charactersUI;

    // Use this for initialization
    void Start()
    {
        playerContainer = GameObject.FindGameObjectWithTag("PlayerContainer").transform;
        StaticStatistics.timer = 0f;

        Text l_text = stats.GetComponent<Text>();

        l_text.text = "Damage dealt: ";
        for (int i = 0; i <= numNewLines; ++i)
        {
            l_text.text += "\n";
        }

        l_text.text += "Damage recieved: ";
        for (int i = 0; i <= numNewLines; ++i)
        {
            l_text.text += "\n";
        }

        l_text.text += "Damage Avoided: ";
        for (int i = 0; i <= numNewLines; ++i)
        {
            l_text.text += "\n";
        }

        l_text.text += "SuperAbility used: ";
        for (int i = 0; i <= numNewLines; ++i)
        {
            l_text.text += "\n";
        }

        l_text.text += "Energy spent: ";
        for (int i = 0; i <= numNewLines; ++i)
        {
            l_text.text += "\n";
        }

        l_text.text += "Basic attack accuracy: ";

    }

    public void OnButtonClick()
    {
        StaticStatistics.nextLevel = true;
    }

    // Update is called once per frame
    void Update()
    {
        StaticStatistics.timer += Time.unscaledDeltaTime;

     /*   statics.SetActive(true);
        int minutes = (int)Time.timeSinceLevelLoad / 60;
        int seconds = (int)Time.timeSinceLevelLoad % 60;

        Text timer_txt = timer.GetComponent<Text>();
        timer_txt.text = "Completed in: " + minutes + ":";

        if (seconds < 10)
        {
            timer_txt.text += "0" + seconds;
        }
        else
        {
            timer_txt.text += seconds;
        }


        if (StaticParemeters.numPlayers < 3)
        {
            players[2].SetActive(false);
            if (StaticParemeters.numPlayers < 2)
            {
                players[1].SetActive(false);
            }
        }

        for (int i = 0; i < StaticParemeters.numPlayers; ++i)
        {
            Text l_txt = players[i].transform.GetChild(0).GetComponent<Text>();

            l_txt.text = StaticStatistics.damageDone[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.damageRecieved[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.damageAvoided[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.usedSuperAbility.ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.energySpent[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            float percentage = StaticStatistics.attackHit[i] / (StaticStatistics.attackHit[i] + StaticStatistics.attackMissed[i]) * 100f;

            l_txt.text += percentage.ToString("F2") + "%";

        }*/
    }

    public void ShowStatistics()
    {

        if (playerContainer)
        {
            for (int i = 0; i< playerContainer.childCount; ++i)
            {
                playerContainer.GetChild(i).gameObject.SetActive(false);
            }
        }

        if(charactersUI)
        {
            charactersUI.SetActive(false);
        }

        statics.SetActive(true);
        int minutes = (int)StaticStatistics.timer / 60;
        int seconds = (int)StaticStatistics.timer % 60;

        Text timer_txt = timer.GetComponent<Text>();
        timer_txt.text = "Completed in: " + minutes + ":";

        if (seconds < 10)
        {
            timer_txt.text += "0" + seconds;
        }
        else
        {
            timer_txt.text += seconds;
        }

        if (StaticParemeters.numPlayers < 3)
        {
            players[2].SetActive(false);
            if (StaticParemeters.numPlayers < 2)
            {
                players[1].SetActive(false);
            }
        }

        for (int i = 0; i < StaticParemeters.numPlayers; ++i)
        {
            Text l_txt = players[i].transform.GetChild(0).GetComponent<Text>();

            l_txt.text = StaticStatistics.damageDone[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.damageRecieved[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.damageAvoided[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.usedSuperAbility.ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            l_txt.text += StaticStatistics.energySpent[i].ToString();
            for (int j = 0; j <= numNewLines; ++j)
            {
                l_txt.text += "\n";
            }

            float percentage;
            if ((StaticStatistics.attackHit[i] + StaticStatistics.attackMissed[i]) > 0)
            {
                percentage = StaticStatistics.attackHit[i] / (StaticStatistics.attackHit[i] + StaticStatistics.attackMissed[i]) * 100f;
            }else{
                percentage = 0.0f;
             }

            l_txt.text += percentage.ToString("F2") + "%";
        }
    }
}