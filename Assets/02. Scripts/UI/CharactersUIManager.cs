﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class CharactersUIManager : MonoBehaviour {
	public bool showNumericValues = true;

	[Header("Referencias")]
	public UICharacterRelationship[] CharacterUI;

	[Header("Estados del personaje")]
	public State[] States;

	[Header("Estados de animo personaje")]
	public CharacterMoodList[] CharacterMoods;
	private CharacterMood defaultMood = CharacterMood.Default;
	public Coroutine[] moodCoroutines = new Coroutine[3];

	private Dictionary<CharacterMood, Sprite>[] moodImageDictionary  = new Dictionary<CharacterMood, Sprite>[3];


	[System.Serializable]
	public enum CharacterMood {
		Default,
		Happy,
		Hurt,
		Serious,
		Surprised,
		Cheeky
	}

	[System.Serializable]
	public struct CharacterMoodList
	{
		public MoodImagePair[] characterMood;
	}
		
	[System.Serializable]
	public struct MoodImagePair
	{
		public CharacterMood mood;
		public Sprite sprite;
	}



	[System.Serializable]
	public struct UICharacterRelationship
	{
		[Header("Personaje")]
		public GameObject character;
		[HideInInspector]
		public GeneralPlayerController gpcScript;
		[HideInInspector]
		public Health healthScript;
		[HideInInspector]
		public Energy energyScript;


		public GameObject UI;
		[HideInInspector]
		public Image portraitUI;
		[HideInInspector]
		public Image healthUI;
		//[HideInInspector]
		public Text healthTextUI;
		[HideInInspector]
		public Image energyUI;
		//[HideInInspector]
		public Text energyTextUI;
		[HideInInspector]
		public Image reservedEnergyUI;
		[HideInInspector]
		public Text nameUI;



		[Header("Estados")]
		public List<UIState> statesUI;
	}

    private bool assigned = false;


	[System.Serializable]
	public struct State
	{
		public string name;
		public GameObject prefab;
	}

	// Use this for initialization
	void Awake () {

	}

	// Use this for initialization
	void Start () {
		AssignUIElements ();
		CreateStates ();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateCharacterUI ();

	}

	private void PrepareMoodDictionaries() {
		for (int i = 0; i < CharacterUI.Length; i++) {
			Dictionary<CharacterMood, Sprite> l_dic = new Dictionary<CharacterMood, Sprite>();
			moodImageDictionary [i] = l_dic;
			for (int n = 0; n < CharacterMoods[i].characterMood.Length; n++) {
				moodImageDictionary[i] [CharacterMoods [i].characterMood[n].mood] = CharacterMoods [i].characterMood[n].sprite;
			}
		}
	}
	private void AssignUIElements() {

        if (!assigned)
        {
			PrepareMoodDictionaries ();
            assigned = true;
            for (int i = 0; i < CharacterUI.Length; ++i)
            {
                CharacterUI[i].healthUI = CharacterUI[i].UI.transform.Find("Panel HealthBar/HealthBar/Health").gameObject.GetComponent<Image>();
				CharacterUI[i].healthTextUI = CharacterUI[i].UI.transform.Find("Panel HealthBar/HealthBar/HealthText").gameObject.GetComponent<Text>();
                CharacterUI[i].energyUI = CharacterUI[i].UI.transform.Find("Panel EnergyBar/EnergyBar/Energy").gameObject.GetComponent<Image>();
				CharacterUI[i].energyTextUI = CharacterUI[i].UI.transform.Find("Panel EnergyBar/EnergyBar/EnergyText").gameObject.GetComponent<Text>();
                CharacterUI[i].reservedEnergyUI = CharacterUI[i].UI.transform.Find("Panel EnergyBar/EnergyBar/ReservedEnergy").gameObject.GetComponent<Image>();
                CharacterUI[i].portraitUI = CharacterUI[i].UI.transform.Find("Panel Portrait/Borders/Portrait").gameObject.GetComponent<Image>();

                CharacterUI[i].healthScript = CharacterUI[i].character.GetComponent<Health>();
                CharacterUI[i].energyScript = CharacterUI[i].character.GetComponent<Energy>();
				CharacterUI[i].gpcScript = CharacterUI[i].character.GetComponent<GeneralPlayerController>();

				CharacterUI [i].portraitUI.sprite = moodImageDictionary[i] [CharacterMood.Default];

				if (!showNumericValues) {
					CharacterUI [i].healthTextUI.gameObject.SetActive (false);
					CharacterUI [i].energyTextUI.gameObject.SetActive (false);
				}
            }
        }
	}

	private void CreateStates() {
		for (int i = 0; i < CharacterUI.Length; ++i) {
			CharacterUI[i].statesUI = new List<UIState>();
			for (int n = 0; n < States.Length; ++n) {
				GameObject state = Instantiate(States[n].prefab, CharacterUI[i].portraitUI.transform);	
				state.GetComponent<UIState>().SetCharacter(CharacterUI[i].character);
				CharacterUI[i].statesUI.Add(state.GetComponent<UIState>());
			}
		}
	}



	private void UpdateCharacterUI() {
		for (int i = 0; i < CharacterUI.Length; ++i) {
			if(CharacterUI[i].UI.activeInHierarchy) {
				CharacterUI[i].healthUI.fillAmount = CharacterUI[i].healthScript.GetHealthAsUnit();
				CharacterUI[i].energyUI.fillAmount = CharacterUI[i].energyScript.GetEnergyAsUnit() - CharacterUI[i].energyScript.GetReservedEnergyAsUnit();
				CharacterUI[i].reservedEnergyUI.fillAmount = CharacterUI[i].energyScript.GetEnergyAsUnit();

				if (showNumericValues) {
					CharacterUI[i].healthTextUI.text = CharacterUI[i].healthScript.GetHealth().ToString ("F0") + "/" + CharacterUI[i].healthScript.maxHealth;
					CharacterUI [i].energyTextUI.text = CharacterUI [i].energyScript.GetEnergy ().ToString ("F0") + "/" + CharacterUI[i].energyScript.maxEnergy;
				}
			}
		}
	}

	public void changeCharacterUI(int indexUI, GameObject character) {
        if (!assigned)
        {
            AssignUIElements();
        }
		if (indexUI >= 0 && indexUI < CharacterUI.Length) {
				GeneralPlayerController l_gpc = character.GetComponent<GeneralPlayerController> ();

				if(CharacterUI[indexUI].character.name != character.name)
				{
					CharacterUI[indexUI].portraitUI.sprite = moodImageDictionary[l_gpc.character][CharacterMood.Default];
				}
				CharacterUI[indexUI].character = character;
				CharacterUI[indexUI].healthScript = CharacterUI[indexUI].character.GetComponent<Health>();
				CharacterUI[indexUI].energyScript = CharacterUI[indexUI].character.GetComponent<Energy>();
				CharacterUI[indexUI].gpcScript = CharacterUI[indexUI].character.GetComponent<GeneralPlayerController>();

				for(int i = 0; i < CharacterUI[indexUI].statesUI.Count; ++i) {
					CharacterUI[indexUI].statesUI[i].SetCharacter(character);
				}
			
		}
	}
	
	public void setActiveCharacterUI(int indexUI, bool active) {
        if (indexUI >= 0 && indexUI < CharacterUI.Length)
        {
			CharacterUI[indexUI].UI.SetActive(active);
		}
	}


	private void setCharacterMood(int indexUI, CharacterMood mood) {
		int l_characterIndex = CharacterUI [indexUI].gpcScript.character;
		CharacterUI [indexUI].portraitUI.sprite = moodImageDictionary[l_characterIndex] [mood];
	}

	public void setTimedCharacterMood(int indexUI, CharacterMood mood, float aDuration) {
		if (moodCoroutines [indexUI] != null) {
			StopCoroutine (moodCoroutines[indexUI]);
		}
		moodCoroutines[indexUI] = StartCoroutine (setTimedCharacterMoodCoroutine (indexUI, mood, aDuration));
	}

	private IEnumerator setTimedCharacterMoodCoroutine (int indexUI, CharacterMood mood, float aDuration) {
		setCharacterMood (indexUI, mood);
		yield return new WaitForSeconds (aDuration);
		setCharacterMood (indexUI, defaultMood);
	}
}
