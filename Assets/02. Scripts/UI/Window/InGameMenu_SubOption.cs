﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenu_SubOption : MonoBehaviour {
	public Text UIName;
	public Image PadLock;

	private string name;
	public string Name {
		get {
			return name;
		}

		set {
			name = value;
			UIName.text = value;
		}
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetAccesible(bool aAccesible) {
		//Debug.Log ("SetAccesible");
		PadLock.enabled = !aAccesible;
	}
}
