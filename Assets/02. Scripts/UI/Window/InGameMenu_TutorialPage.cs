﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenu_TutorialPage : MonoBehaviour {
	public Text UIDescription;
	public Text UICurrentPage;
	public Animator anim;

	private RectTransform rectTrans;
	float lastWidth = 0;
	public RectTransform descriptionToFit;


	private string description;
	public string Description {
		get {
			return description;
		}

		set {
			description = value;
			if (UIDescription != null) {
				UIDescription.text = value;
			}
		}
	}

	private string currentPage;
	public string CurrentPage {
		get {
			return currentPage;
		}

		set {
			currentPage = value;
			if (UICurrentPage != null) {
				UICurrentPage.text = value;
			}
		}
	}
		

	// Use this for initialization
	void Start () {
		rectTrans = GetComponent<RectTransform> ();
	}

	// Update is called once per frame
	void Update () {
		FitParent ();
	}

	public void HideFromLeft() {
		anim.Play ("HideLeft");
	}

	public void HideFromRight() {
		anim.Play ("HideRight");
	}

	public void ShowFromRight() {
		gameObject.SetActive (true);
		anim.Play ("ShowRight");
	}

	public void ShowFromLeft() {
		gameObject.SetActive (true);
		anim.Play ("ShowLeft");
	}

	public void SetInactive() {
		gameObject.SetActive (false);
	}

	public void SetActive() {
		gameObject.SetActive (true);
	}

	public void FitParent() {
		float l_newWidth = rectTrans.rect.width;

		//if (lastWidth != l_newWidth) {
			lastWidth = l_newWidth;
			descriptionToFit.sizeDelta = new Vector2( l_newWidth, descriptionToFit.sizeDelta.y );
		//}
	}
}
