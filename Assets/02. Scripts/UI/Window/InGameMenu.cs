﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using XboxCtrlrInput;
using AK.Wwise;

public class InGameMenu : MonoBehaviour {
	[Header("Dialogs in this level")]
	public Vector2 accesibleDialogs;

	[Header("Selection")]
	public Options currentOption;
	public MenuZone currentMenuZone;


	[Header("Animator")]
	public Animator tutorialsOption;
	public Animator dialogsOption;
	public Animator optionsOption;
	public Animator buttonPreviousSubOption;
	public Animator buttonNextSubOption;
	public Animator buttonPreviousComment;
	public Animator buttonNextComment;
	public Animator buttonPreviousTutorialPage;
	public Animator buttonNextTutorialPage;
	public Animator tutorialVideo;
	private Animator menu;
	public Animator panel;
	public Animator panelAlert;

	[Header("References")]
	public GameObject tutorialLeftPanel;
	public GameObject tutorialRightPanel;
	public GameObject dialogsLeftPanel;
	public GameObject dialogsRightPanel;
	public GameObject optionsLeftPanel;

	public GameObject commentsRightPanel;
	public GameObject visibleCommentsSpace;
	public GameObject tutorialPagesRightPanel;

	public Scrollbar scrollBarComments;
	public Image controlsImage;
	public Sprite defaultControlsSprite;

	public VideoPlayer tutorialsVideoPlayer;
	public AudioSource tutorialsAudioSource;
	public RawImage tutorialsRawImage;

	public Text panelText;
	public Text panelAlertText;

	[Header("Prefabs")]
	public GameObject prefabSubOption;
	public GameObject prefabCommentLeft;
	public GameObject prefabCommentRight;
	public GameObject prefabCommentPicture;
	public GameObject prefabCommentPictureLeft;
	public GameObject prefabCommentPictureRight;
	public GameObject prefabTutorialPage;

	private List<MenuDialog> dialogs = new List<MenuDialog>();
	private List<MenuTutorial>  tutorials = new List<MenuTutorial>();
	private List<MenuOption>  options = new List<MenuOption>();

	[Header("Configuration")]
	public TutorialsData tutorialsData;
	public DialogsData dialogsData;
	public XboxController controller;
	public List<MonoBehaviour> toDisable = new  List<MonoBehaviour> ();
	public List<GameObject> toSetInactive = new  List<GameObject> ();

	public float timeBeetweenAppearSubOption = 0.2f;
	public float timeBeetweenAppearComment = 0.2f;
	public float timeToLoadVideo = 3.0f;
	public float timePanelDuration = 5.0f;
	public float scrollCommentsSpeed = 100.0f;

	private int currentSelectedSubOption = -1;
	private int disponibleSpacesSubOptions = 0;
	private int ocuppiedSpacesSubOptions = 0;
	private int startOffset = 0;
	private int endOffset = 0;
	private bool scrollActive = false;

	//Panel
	private float currentTimePanelDuration;
	private float currentTimePanelAlertDuration;
	private int subOptionToOpen = 0;
	private Options optionToOpen = Options.Dialogs;

	public bool accesible = true;
	private bool open = false;
	private bool panelOpen = false;
	private bool panelAlertOpen = false;
	private bool openAnimation = false;

	[Header("Input")]
	public ControllerInputAxisTimers controllerInputAxisTimers;

	[System.Serializable]
	public struct ControllerInputAxisTimers
	{
		public float refreshTime;
		public float treshHold;

		public float timeButtonDown;

		public bool leftStickXPositive;
		public bool pressedLeftStickXPositive;
		public bool leftStickXNegative;
		public bool pressedLeftStickXNegative;
		public bool leftStickYPositive;
		public bool pressedLeftStickYPositive;
		public bool leftStickYNegative;
		public bool pressedLeftStickYNegative;
		public bool rightTrigger;
		public bool pressedRightTrigger;
		public bool leftTrigger;
		public bool pressedLeftTrigger;
	}
		

	[Header("Corutinas")]
	public Coroutine fillSubOptionsCoroutine;
	public Coroutine hideSubOptionsCoroutine;
	private bool UIUpdating = false;

	public enum Options {
		Tutorials,
		Dialogs,
		Options
	}

	public enum MenuZone {
		Options,
		SubOptions,
		RightWindow
	}

	[System.Serializable]
	public class MenuDialog {
		private InGameMenu parent;

		public List<GameObject> comments = new List<GameObject> ();
		public GameObject subOption;
		public RectTransform commentsRightPanelRectTrans;
		public RectTransform visibleCommentsSpaceRectTrans;
		public Animator subOptionAnimator;

		public bool showing = false;
		public bool selected = false;
		public bool accesible = false;

		public int numComments = 0;
		public float timeBeetweenAppearComment = 0.0f;
		public float scrollCommentsSpeed = 0.0f;
		public bool updatingCommentsSpace = false;
		public bool zeroScrollTop = true;
		public bool zeroScrollDown = false;
		public float scrollTop = 0;
		public float scrollDown = 0;

		public Coroutine showCommentsCoroutine;

		public enum CommentType
		{
			NormalLeft,
			NormalRight,
			Picture,
			PictureLeft,
			PictureRight
		}

		public MenuDialog(string aName, InGameMenu aInGameMenu) {
			parent = aInGameMenu;

			subOption = Instantiate (parent.prefabSubOption, parent.dialogsLeftPanel.transform);
			subOptionAnimator = subOption.GetComponent<Animator>();
			commentsRightPanelRectTrans = parent.commentsRightPanel.GetComponent<RectTransform>();
			visibleCommentsSpaceRectTrans = parent.visibleCommentsSpace.GetComponent<RectTransform>();
			InGameMenu_SubOption l_subOptionScript = subOption.GetComponent<InGameMenu_SubOption> ();
			l_subOptionScript.Name = aName;
			SetSelected(false);
			SetAccesible(true);
			SetShowing(false);
		}

		public void AddComment(string aCharacter, string aComment, Sprite aCharacterSprite, CommentType type) {
			numComments++;

			GameObject l_Comment;
			switch (type) {
			case CommentType.NormalLeft:
				l_Comment = Instantiate (parent.prefabCommentLeft, parent.commentsRightPanel.transform);
				break;
			case CommentType.NormalRight:
				l_Comment = Instantiate (parent.prefabCommentRight, parent.commentsRightPanel.transform);
				break;
			default:
				l_Comment = Instantiate (parent.prefabCommentLeft, parent.commentsRightPanel.transform);
				break;
			}

			InGameMenu_Comment l_CommentScript = l_Comment.GetComponent<InGameMenu_Comment> ();
			l_CommentScript.Character = aCharacter;
			l_CommentScript.Comment = aComment;
			l_CommentScript.SetSprite (aCharacterSprite);
			l_Comment.SetActive (false);
			comments.Add (l_Comment);
		}

		public void AddComment(string aComment, Sprite aSprite,  CommentType type) {
			numComments++;

			GameObject l_Comment;
			switch (type) {
			case CommentType.Picture:
				l_Comment = Instantiate (parent.prefabCommentPicture, parent.commentsRightPanel.transform);
				break;
			case CommentType.PictureLeft:
				l_Comment = Instantiate (parent.prefabCommentPictureLeft, parent.commentsRightPanel.transform);
				break;
			case CommentType.PictureRight:
				l_Comment = Instantiate (parent.prefabCommentPictureRight, parent.commentsRightPanel.transform);
				break;
			default:
				l_Comment = Instantiate (parent.prefabCommentPicture, parent.commentsRightPanel.transform);
				break;
			}
				
			InGameMenu_Comment l_CommentScript = l_Comment.GetComponent<InGameMenu_Comment> ();
			l_CommentScript.Comment = aComment;
			l_CommentScript.SetSprite (aSprite);
			l_Comment.SetActive (false);
			comments.Add (l_Comment);
		}

		public void UpdateScrollLimitDown() {
			float l_visibleSpaceHeight = visibleCommentsSpaceRectTrans.rect.height;
			float offsetHeigth = commentsRightPanelRectTrans.offsetMax.y;
			float l_commentsSpaceHeight = commentsRightPanelRectTrans.rect.height;

			scrollDown = (l_commentsSpaceHeight - offsetHeigth) - l_visibleSpaceHeight;

			if ((l_commentsSpaceHeight - offsetHeigth) >= l_visibleSpaceHeight) {
				zeroScrollDown = false;
			} else {
				zeroScrollDown = true;
			}
		}

		public void UpdateScrollLimitTop() {
			scrollTop = commentsRightPanelRectTrans.anchoredPosition.y + 5;

			if (commentsRightPanelRectTrans.anchoredPosition.y <= -5) {
				zeroScrollTop = true;
			} else {
				zeroScrollTop = false;
			}
		}

		public void UpdateScrollLimits() {
			UpdateScrollLimitDown ();
			UpdateScrollLimitTop ();
			UpdateScrollBar ();
		}

		public void UpdateScrollBar() {
			if (scrollDown <= 0 && scrollTop <= 0) {
				if (parent.scrollActive) {
					parent.scrollActive = false;
					parent.scrollBarComments.gameObject.SetActive (false);
				}
			} else {
				if (!parent.scrollActive) {
					parent.scrollActive = true;
					parent.scrollBarComments.gameObject.SetActive (true);
				}
				if (commentsRightPanelRectTrans.rect.height != 0) {
					//parent.scrollBarComments.size = visibleCommentsSpaceRectTrans.rect.height / commentsRightPanelRectTrans.rect.height;
				}
				float totalNotVisibleSpace = scrollTop + scrollDown;
				parent.scrollBarComments.value = Mathf.Clamp (scrollTop / totalNotVisibleSpace, 0, 1);
			}
		}

		public bool ScrollForward() {
			bool l_hitLimit = false;

			Vector2 l_newPosition = new Vector2 (commentsRightPanelRectTrans.anchoredPosition.x, commentsRightPanelRectTrans.anchoredPosition.y + (parent.scrollCommentsSpeed  * Time.unscaledDeltaTime));

			float l_visibleSpaceHeight = visibleCommentsSpaceRectTrans.rect.height;
			float offsetHeigth = commentsRightPanelRectTrans.offsetMax.y;
			float l_commentsSpaceHeight = commentsRightPanelRectTrans.rect.height;


			if ((l_commentsSpaceHeight - offsetHeigth) >= l_visibleSpaceHeight) {
				commentsRightPanelRectTrans.anchoredPosition = l_newPosition;
			} else { //Choque
				l_hitLimit = true;
			}

			UpdateScrollLimits ();

			return !l_hitLimit;
		}

		public bool ScrollBackward() {
			bool l_hitLimit = false;

			Vector2 l_newPosition = new Vector2 (commentsRightPanelRectTrans.anchoredPosition.x, commentsRightPanelRectTrans.anchoredPosition.y - (parent.scrollCommentsSpeed  * Time.unscaledDeltaTime));

			if (l_newPosition.y < -5) { //Choque
				l_newPosition.y = -5;
				l_hitLimit = true;
			}
			commentsRightPanelRectTrans.anchoredPosition = l_newPosition;
			UpdateScrollLimits ();

			return !l_hitLimit;
		}
			
		public void SetSelected(bool aSelected) {
			if (showCommentsCoroutine != null) {
				parent.StopChildCoroutine (showCommentsCoroutine);
			}
			if (showing) {
				subOptionAnimator.SetBool ("Selected", aSelected);
				//AkSoundEngine.PostEvent ("UI_Select_2", parent.transform.gameObject);
			}
			selected = aSelected;

			if (!aSelected) { //Los esconde instaneamente
				for (int i = 0; i < comments.Count; i++) {
					comments [i].SetActive (false);
				}
			} else { //Añade un retardo para mostrarlos
				
				showCommentsCoroutine = parent.StartChildCoroutine (ShowComments()); //TODO:Cancelarla al llamarla de nuevo
			}
		}

		public IEnumerator ShowComments () {
			commentsRightPanelRectTrans.sizeDelta = new Vector2 (commentsRightPanelRectTrans.sizeDelta.x, -5);
			commentsRightPanelRectTrans.anchoredPosition = new Vector2 (commentsRightPanelRectTrans.anchoredPosition.x, -5);

			float spacing = parent.commentsRightPanel.GetComponent<VerticalLayoutGroup> ().spacing;

			for (int i = 0; i < comments.Count; i++) {
				yield return new WaitForSecondsRealtime (parent.timeBeetweenAppearComment);
				comments [i].SetActive (true);
				commentsRightPanelRectTrans.sizeDelta = new Vector2 (commentsRightPanelRectTrans.sizeDelta.x, commentsRightPanelRectTrans.sizeDelta.y + comments [i].GetComponent<ILayoutElement> ().preferredHeight + spacing);
				UpdateScrollLimits ();
			}

			showCommentsCoroutine = null;
		}
			
		public void SetShowing(bool aShowing) {
			subOption.SetActive (aShowing);
			showing = aShowing;
			if (showing && parent.open) {
				subOptionAnimator.Play ("Appear");
			}
		}

		public void SetAccesible(bool aAccesible) {
			subOption.GetComponent<InGameMenu_SubOption> ().SetAccesible (aAccesible);
			accesible = aAccesible;
		}
	}

	[System.Serializable]
	public class MenuOption {
		private InGameMenu parent;
		public GameObject subOption;
		public Action action;
		public Animator subOptionAnimator;
		public bool showing = false;
		public bool accesible = false;
		public bool selected = false;

		public Coroutine showOptionsCoroutine;

		public MenuOption(string aName, Action aAction, InGameMenu  aInGameMenu) {
			parent = aInGameMenu;
			action = aAction;
			subOption = Instantiate (parent.prefabSubOption, parent.optionsLeftPanel.transform);
			subOptionAnimator = subOption.GetComponent<Animator>();
			InGameMenu_SubOption l_subOptionScript = subOption.GetComponent<InGameMenu_SubOption> ();
			l_subOptionScript.Name = aName;
		}

		public void SetShowing(bool aShowing) {
			subOption.SetActive (aShowing);
			showing = aShowing;
			if (showing && parent.open) {
				subOptionAnimator.Play ("Appear");
			}
		}

		public void SetAccesible(bool aAccesible) {
			subOption.GetComponent<InGameMenu_SubOption> ().SetAccesible (aAccesible);
			accesible = aAccesible;
		}

		public void SetSelected(bool aSelected) {
			if (showOptionsCoroutine != null) {
				parent.StopChildCoroutine (showOptionsCoroutine);
			}
			if (showing) {
				subOptionAnimator.SetBool ("Selected", aSelected);
			}
		}

		public void Click() {
			if (action != null) {
				action ();
			}
		}
	}


	[System.Serializable]
	public class MenuTutorial {
		private InGameMenu parent;

		public List<GameObject> tutorialPages = new List<GameObject> ();
		public GameObject subOption;
		public Animator subOptionAnimator;

		public bool showing = false;
		public bool selected = false;
		public bool accesible = true;

		public int currentPage = 0;
		public int numPages = 0;

		public Sprite controlsSprite;
		public VideoClip tutorialVideo;

		private bool preparedVideo = false;
		public Coroutine playTutorialVideoCoroutine;

		public MenuTutorial(string aName, Sprite aControlsSprite, VideoClip aTutorialVideo, InGameMenu  aInGameMenu) {
			parent = aInGameMenu;

			subOption = Instantiate (parent.prefabSubOption, parent.tutorialLeftPanel.transform);
			subOptionAnimator = subOption.GetComponent<Animator>();
			InGameMenu_SubOption l_subOptionScript = subOption.GetComponent<InGameMenu_SubOption> ();
			l_subOptionScript.Name = aName;
			controlsSprite = aControlsSprite;
			tutorialVideo = aTutorialVideo;

			SetSelected(false);
			SetAccesible(false);
			SetShowing(false);
		}

		public void AddTutorialPage (string aDescription) {
			numPages++;
	
			GameObject l_tutorialPage = Instantiate (parent.prefabTutorialPage, parent.tutorialPagesRightPanel.transform);
			tutorialPages.Add (l_tutorialPage);

			InGameMenu_TutorialPage l_tutorialPageScript = l_tutorialPage.GetComponent<InGameMenu_TutorialPage> ();
			l_tutorialPageScript.Description = aDescription;
			l_tutorialPageScript.CurrentPage = (tutorialPages.Count) + "/" + numPages;
			l_tutorialPage.SetActive (false);


			for (int i = 0; i < tutorialPages.Count - 1; i++) {
				InGameMenu_TutorialPage l_tutorialPageScript2 = tutorialPages[i].GetComponent<InGameMenu_TutorialPage> ();
				l_tutorialPageScript2.CurrentPage = (i  + 1) + "/" + numPages;
			}

		}


		public void SelectTutorialPage (int aIndex) {
			InGameMenu_TutorialPage l_tutorialPageScript = tutorialPages [currentPage].GetComponent<InGameMenu_TutorialPage> ();
			InGameMenu_TutorialPage l_tutorialPageScript2 = tutorialPages [aIndex].GetComponent<InGameMenu_TutorialPage> ();

			if (aIndex > currentPage) {
				l_tutorialPageScript.HideFromLeft ();
				l_tutorialPageScript2.ShowFromLeft ();
			} else {
				l_tutorialPageScript.HideFromRight ();
				l_tutorialPageScript2.ShowFromRight ();
			}
			currentPage = aIndex;
		}

		public bool NextTutorialPage () {
			int l_newCurrentPage = Mathf.Clamp (currentPage + 1, 0, tutorialPages.Count - 1);

			if (l_newCurrentPage != currentPage) {
				SelectTutorialPage (l_newCurrentPage);
				return true;
			}
			return false;
		}

		public bool PreviousTutorialPage () {
			int l_newCurrentPage  = Mathf.Clamp (currentPage - 1, 0, tutorialPages.Count - 1);
			if (l_newCurrentPage != currentPage) {
				SelectTutorialPage (l_newCurrentPage);
				return true;
			}
			return false;
		}
			
		public void SetSelected(bool aSelected) {
			if (showing) {
				subOptionAnimator.SetBool ("Selected", aSelected);
				//AkSoundEngine.PostEvent ("UI_Select_2", parent.transform.gameObject);
			}
			selected = aSelected;
			if (numPages > 0) {
				if (!aSelected) {
					//tutorialPages [currentPage].GetComponent<InGameMenu_TutorialPage> ().Hide ();
					tutorialPages [currentPage].GetComponent<InGameMenu_TutorialPage> ().SetInactive();
				} else {
					tutorialPages [currentPage].GetComponent<InGameMenu_TutorialPage> ().ShowFromRight ();
				}
			}
			if (aSelected) {
				parent.tutorialVideo.Play ("Load");
				playTutorialVideoCoroutine = parent.StartChildCoroutine (PlayVideo ());
			} else {
				if (playTutorialVideoCoroutine != null) {
					parent.StopChildCoroutine (playTutorialVideoCoroutine);
				}
				StopVideo ();
			}
			currentPage = 0;
			SetControlImage (aSelected);
		}

		public void SetShowing(bool aShowing) {
			subOption.SetActive (aShowing);
			showing = aShowing;
			if (showing && parent.open) {
				subOptionAnimator.Play ("Appear");
			}
		}

		public void SetAccesible(bool aAccesible) {
			subOption.GetComponent<InGameMenu_SubOption> ().SetAccesible (aAccesible);
			accesible = aAccesible;
		}

		public void SetControlImage(bool aSet) {
			if (aSet) {
				parent.controlsImage.sprite = controlsSprite;
			} else {
				parent.controlsImage.sprite = parent.defaultControlsSprite;
			}
		}

		public void StopVideo() {
			parent.tutorialsVideoPlayer.Stop ();
			parent.tutorialsRawImage.color = Color.black;
		}
	 	IEnumerator PlayVideo () {
			yield return new WaitForSecondsRealtime (parent.timeToLoadVideo);

			parent.tutorialsVideoPlayer.playOnAwake = false;
			parent.tutorialsAudioSource.playOnAwake = false;
			parent.tutorialsAudioSource.Pause ();

			parent.tutorialsVideoPlayer.source = VideoSource.VideoClip;
			parent.tutorialsVideoPlayer.isLooping = true;


			parent.tutorialsVideoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
			parent.tutorialsVideoPlayer.EnableAudioTrack (0, true);
			parent.tutorialsVideoPlayer.SetTargetAudioSource (0, parent.tutorialsAudioSource);

			parent.tutorialsVideoPlayer.clip = tutorialVideo;

			parent.tutorialsVideoPlayer.prepareCompleted += PreparedVideo;
			parent.tutorialsVideoPlayer.errorReceived += VideoError;
			parent.tutorialsVideoPlayer.Prepare ();
			while (!parent.tutorialsVideoPlayer.isPrepared) {
				yield return null;
			}
			parent.tutorialVideo.Play ("Show");
			parent.tutorialsRawImage.color = Color.white;
			parent.tutorialsRawImage.texture = parent.tutorialsVideoPlayer.texture;
			parent.tutorialsVideoPlayer.Play ();
			parent.tutorialsAudioSource.Play ();
			//m_videoPlayer.enabled = false;
			//m_videoPlayer.enabled = true;
			playTutorialVideoCoroutine = null;
		}

		void PreparedVideo(VideoPlayer vp) {
			preparedVideo = true;
		}

		void VideoError(VideoPlayer vp, string errorString) {
			Debug.LogError ("El siguiente error se ha dado con el video: " + errorString);
		}

	}

	public MenuTutorial AddTutorial (string aName, Sprite aSprite, VideoClip aTutorialVideo) {
		MenuTutorial l_tutorial = new MenuTutorial (aName, aSprite, aTutorialVideo, this);
		tutorials.Add (l_tutorial);
		if (ocuppiedSpacesSubOptions + 1 <= disponibleSpacesSubOptions) { // Si hay espacio lo muestra
			ocuppiedSpacesSubOptions++;
			l_tutorial.SetShowing (true);
		}
		return l_tutorial;
	}

	public MenuDialog AddDialog (string aName) {
		MenuDialog l_dialog = new MenuDialog (aName, this);
		dialogs.Add (l_dialog);
		return l_dialog;
	}

	public MenuOption AddOption (string aName, Action aAction) {
		MenuOption l_option = new MenuOption (aName, aAction, this);
		options.Add (l_option);
		l_option.SetAccesible (true);
		return l_option;
	}
		

	private void LoadTutorials () {
		for (int i = 0; i < tutorialsData.tutorials.Count; i++) {
			TutorialsData.TutorialData l_tutorialData = tutorialsData.tutorials [i];
			MenuTutorial l_tutorial = AddTutorial (l_tutorialData.name, l_tutorialData.controlsSprite, l_tutorialData.tutorialVideo);

			for (int n = 0; n < l_tutorialData.tutorialPagesData.Count; n++) {
				l_tutorial.AddTutorialPage (l_tutorialData.tutorialPagesData[n]);
			}
		}
	}

	private void LoadDialogs() {
		for (int i = 0; i < dialogsData.dialogs.Count; i++) {
			DialogsData.DialogData l_dialogData = dialogsData.dialogs [i];
			MenuDialog l_dialog = AddDialog (l_dialogData.name);

			if (i == 0) {
				l_dialog.SetAccesible (true);
			} else {
				l_dialog.SetAccesible (false);
			}

			for (int n = 0; n < l_dialogData.commentsData.Count; n++) {
				MenuDialog.CommentType l_type = l_dialogData.commentsData [n].picturePosition;
				if (l_type == MenuDialog.CommentType.NormalLeft || l_type == MenuDialog.CommentType.NormalRight) {
					l_dialog.AddComment (l_dialogData.commentsData [n].character,l_dialogData.commentsData [n].comment,  l_dialogData.commentsData [n].picture, l_type);
				} else {
					l_dialog.AddComment (l_dialogData.commentsData [n].comment, l_dialogData.commentsData [n].picture, l_type);
				}
			}
		}
	}

	private void LoadOptions () {
		MenuOption l_option =  AddOption("Reanudar partida", CloseMenu);
		l_option = AddOption("Volver al menu principal", LoadMainMenu);
		l_option = AddOption("Salir del juego", ExitGame);
	}

	public void OpenMenu() {
		if (!open) {
			if (hideSubOptionsCoroutine != null) {
				StopCoroutine (hideSubOptionsCoroutine);
			}
			menu.Play ("Show");
			PauseGameController.Pause ();
			open = true;

			for (int i = 0; i < toDisable.Count; i++) {
				toDisable [i].enabled = false;
			}
			for (int i = 0; i < toSetInactive.Count; i++) {
				toSetInactive [i].SetActive(false);
			}
			if (currentOption != Options.Tutorials) {
				SelectOption (Options.Tutorials);
			}
		}
	}

	public void CloseMenu() {
		if (open) {
			menu.Play ("Hide");
			PauseGameController.Resume ();
			open = false;
			openAnimation = false;
			SelectSubOption (-1);
			hideSubOptionsCoroutine = StartCoroutine (HideAllSubOptions());

			for (int i = 0; i < toDisable.Count; i++) {
				toDisable [i].enabled = true;
			}

			for (int i = 0; i < toSetInactive.Count; i++) {
				toSetInactive [i].SetActive(true);
			}
		}
	}

	IEnumerator HideAllSubOptions() {
		yield return new WaitForSecondsRealtime (1.0f);
		if (currentOption == Options.Dialogs) {
			for (int i = 0; i < dialogs.Count; i++) {
				dialogs [i].SetShowing (false);
			}
		} else if (currentOption == Options.Tutorials) {
			for (int i = 0; i < tutorials.Count; i++) {
				tutorials [i].SetShowing (false);
			}
		}
		hideSubOptionsCoroutine = null;
	}

	private void OnAnimationMenuShowEnds() {
		openAnimation = true;
	}

	IEnumerator ShowTutorialCoroutine (int aIndex) { 
		if (!open) {
			OpenMenu ();
		}

		SelectOption (Options.Tutorials); //Corutina
		SelectMenuZone (MenuZone.SubOptions);

		while (!openAnimation || UIUpdating ) {
			yield return null; 
		}

		SelectSubOption (aIndex);
		SelectMenuZone (MenuZone.RightWindow);
	}

	IEnumerator ShowDialogCoroutine (int aIndex) { 
		if (!open) { //Si no esta la ventana de menu abierta la abre
			OpenMenu ();
		}

		SelectOption (Options.Dialogs); //Corutina 
		SelectMenuZone (MenuZone.SubOptions);

		while (!openAnimation && !UIUpdating ) {  //Espera a que acabe de abrirse la ventana y que todas las subopciones se muestren
			yield return null; 
		}
		SelectSubOption (aIndex);
		SelectMenuZone (MenuZone.RightWindow);
	}

	public void ShowTutorial(int aIndex) {
		if (aIndex >= 0 && aIndex < tutorials.Count - 1) {
			tutorials [aIndex].SetAccesible (true); 
			StartCoroutine (ShowTutorialCoroutine(aIndex));
		}
	}


	public void ShowDialog(int aIndex) {
		if (aIndex >= 0 && aIndex < dialogs.Count - 1) {
			dialogs [aIndex].SetAccesible (true); 
			StartCoroutine (ShowDialogCoroutine (aIndex));
		}
	}

	public void Alert (string aAlert) {
		panelAlertOpen = true;
		panelAlert.gameObject.SetActive (true);
		panelAlert.Play ("Show");
		//optionToOpen = Options.None;
		panelAlertText.text = aAlert;
	}

	public void TutorialAlert (int aIndex) {
		if (aIndex >= 0 && aIndex < tutorials.Count - 1) {
			tutorials [aIndex].SetAccesible (true); 
			panelOpen = true;
			panel.gameObject.SetActive (true);
			panel.Play ("Show");
			subOptionToOpen = aIndex;
			optionToOpen = Options.Tutorials;
			string l_name = tutorialsData.tutorials [aIndex].name;
			panelText.text = "¡Nueva entrada de tutorial disponible!\n\"" + l_name + "\"";
		}
	}

	public void DialogAlert (int aIndex) {
		if (aIndex >= 0 && aIndex < tutorials.Count - 1) {
			dialogs [aIndex].SetAccesible (true); 
			panelOpen = true;
			panel.gameObject.SetActive (true);
			panel.Play ("Show");
			subOptionToOpen = aIndex;
			optionToOpen = Options.Dialogs;
			string l_name = dialogsData.dialogs [aIndex].name;
			panelText.text ="¡Nueva entrada de diálogo disponible!\n\"" + l_name + "\"";
		}
	}

	public void SetTutorialAccesible (int aIndex) {
		if (aIndex >= 0 && aIndex < tutorials.Count - 1) {
			tutorials [aIndex].SetAccesible (true);
		}
	}

	public void SetDialogAccesible (int aIndex) {
		if (aIndex >= 0 && aIndex < dialogs.Count - 1) {
			dialogs [aIndex].SetAccesible (true);
		}
	}


	// Use this for initialization
	void Start () {
		currentTimePanelDuration = timePanelDuration;
		currentTimePanelAlertDuration = timePanelDuration;
			
		menu = GetComponent<Animator> ();

		currentOption = Options.Tutorials;
		currentMenuZone = MenuZone.Options;

		LoadTutorials ();
		LoadDialogs ();
		LoadOptions ();

		for (int i = 0; i < dialogs.Count; i++) {
			if (i >= accesibleDialogs.x && i <= accesibleDialogs.y) {
				dialogs [i].SetAccesible (true);
			}
		}
	}

	private int Module(int a, int b)
	{
		return (a % b + b) % b;
	}


	void SelectNextOption() {
		Options l_option;
		l_option = (Options)Module ((int)currentOption + 1, 3);
		SelectOption (l_option);
	}

	void SelectPreviousOption() {
		Options l_option;
		l_option = (Options)Module ((int)currentOption - 1, 3);
		SelectOption (l_option);
	}

	void SelectOption(Options option) {
		if (option != currentOption) {
			if (option == Options.Dialogs) {
				dialogsOption.SetBool ("Selected", true);
				tutorialsOption.SetBool ("Selected", false);
				optionsOption.SetBool ("Selected", false);
				tutorialLeftPanel.SetActive (false);
				tutorialRightPanel.SetActive (false);
				optionsLeftPanel.SetActive (false);
				dialogsLeftPanel.SetActive (true);
				dialogsRightPanel.SetActive (true);


			} else if (option == Options.Tutorials) {
				dialogsOption.SetBool ("Selected", false);
				optionsOption.SetBool ("Selected", false);
				tutorialsOption.SetBool ("Selected", true);
				dialogsLeftPanel.SetActive (false);
				dialogsRightPanel.SetActive (false);
				optionsLeftPanel.SetActive (false);
				tutorialLeftPanel.SetActive (true);
				tutorialRightPanel.SetActive (true);
			} else if (option == Options.Options) {
				dialogsOption.SetBool ("Selected", false);
				optionsOption.SetBool ("Selected", true);
				tutorialsOption.SetBool ("Selected", false);
				dialogsLeftPanel.SetActive (false);
				optionsLeftPanel.SetActive (true);
				tutorialLeftPanel.SetActive (false);
			}
			currentOption = option;
			FillSubOptionsDelayed (timeBeetweenAppearSubOption); //Reset de subOptions
		} 
	}

	private void ScrollCommentsForward() {	
		buttonNextComment.SetBool ("Selected", true);
		dialogs [currentSelectedSubOption].ScrollForward ();
	}


	private void ScrollCommentsBackward() {
		buttonPreviousComment.SetBool ("Selected", true);
		dialogs [currentSelectedSubOption].ScrollBackward ();
	}


	private void SelectNextTutorialPage() {
		if (tutorials [currentSelectedSubOption].NextTutorialPage ()) {
			if (tutorials [currentSelectedSubOption].currentPage == 0) {
				buttonPreviousTutorialPage.SetBool ("Selected", true);
				buttonNextTutorialPage.SetBool ("Selected", false);
			} else if (	tutorials [currentSelectedSubOption].currentPage == tutorials [currentSelectedSubOption].numPages - 1) {
				buttonNextTutorialPage.SetBool ("Selected", true);
				buttonPreviousTutorialPage.SetBool ("Selected", false);
			} else {
				buttonNextTutorialPage.Play ("Click");
				buttonNextTutorialPage.SetBool ("Selected", false);
				buttonPreviousTutorialPage.SetBool ("Selected", false);
			}
			//AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
		}
	}

	private void SelectPreviousTutorialPage() {
		if (tutorials [currentSelectedSubOption].PreviousTutorialPage ()) {
			if (tutorials [currentSelectedSubOption].currentPage == 0) {
				buttonPreviousTutorialPage.SetBool ("Selected", true);
				buttonNextTutorialPage.SetBool ("Selected", false);
			} else if (	tutorials [currentSelectedSubOption].currentPage == tutorials [currentSelectedSubOption].numPages - 1) {
				buttonNextTutorialPage.SetBool ("Selected", true);
				buttonPreviousTutorialPage.SetBool ("Selected", false);
			} else {
				buttonPreviousTutorialPage.Play ("Click");
				buttonNextTutorialPage.SetBool ("Selected", false);
				buttonPreviousTutorialPage.SetBool ("Selected", false);
			}
			//AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
		}
	}

	private void SelectSubOption(int index) {
		if (currentOption == Options.Dialogs && index != currentSelectedSubOption) {

			if (index >= 0) {
				int l_endOffsetSpace = endOffset;
				int l_startOffsetSpace = startOffset;
				int l_displacement;

				if (index > endOffset) {
					l_displacement = index - endOffset;
					if (l_displacement > 1) {
						startOffset = startOffset + l_displacement;
						endOffset = endOffset + l_displacement;
						for (int i = 0; i < dialogs.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								dialogs [i].SetShowing (true);
							} else {
								dialogs [i].SetShowing (false);
							}
						}
					} else {
						startOffset++;
						endOffset++;
						dialogs [startOffset - 1].SetShowing (false);
						dialogs [endOffset].SetShowing (true);
					}
				} else if (index < startOffset) {
					l_displacement = startOffset - index;
					if (l_displacement > 1) {
						startOffset = startOffset - l_displacement;
						endOffset = endOffset - l_displacement;
						for (int i = 0; i < dialogs.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								dialogs [i].SetShowing (true);
							} else {
								dialogs [i].SetShowing (false);
							}
						}
					} else {
						startOffset--;
						endOffset--;
						dialogs [startOffset].SetShowing (true);
						dialogs [endOffset + 1].SetShowing (false);

					}
				}
			}

			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < dialogs.Count)) {
				dialogs [currentSelectedSubOption].SetSelected (false);
			}
			if ((index >= 0) && (index < dialogs.Count)) {
				dialogs [index].SetSelected (true);
			}
			currentSelectedSubOption = index;
		} else if (currentOption == Options.Tutorials && index != currentSelectedSubOption) {
	
			if (index >= 0) {
				int l_endOffsetSpace = endOffset;
				int l_startOffsetSpace = startOffset;
				int l_displacement;

				if (index > endOffset) {
					l_displacement = index - endOffset;
					if (l_displacement > 1) {
						startOffset = startOffset + l_displacement;
						endOffset = endOffset + l_displacement;
						for (int i = 0; i < tutorials.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								tutorials [i].SetShowing (true);
							} else {
								tutorials [i].SetShowing (false);
							}
						}
					} else {
						startOffset++;
						endOffset++;
						tutorials [startOffset - 1].SetShowing (false);
						tutorials [endOffset].SetShowing (true);
					}
				} else if (index < startOffset) {
					l_displacement = startOffset - index;
					if (l_displacement > 1) {
						startOffset = startOffset - l_displacement;
						endOffset = endOffset - l_displacement;
						for (int i = 0; i < tutorials.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								tutorials [i].SetShowing (true);
							} else {
								tutorials [i].SetShowing (false);
							}
						}
					} else {
						startOffset--;
						endOffset--;
						tutorials [startOffset].SetShowing (true);
						tutorials [endOffset + 1].SetShowing (false);
					
					}
				}
			}

			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < tutorials.Count)) {
				tutorials [currentSelectedSubOption].SetSelected (false);
			}
			if ((index >= 0) && (index < tutorials.Count)) {
				tutorials [index].SetSelected (true);
			}
			currentSelectedSubOption = index;
		} else if (currentOption == Options.Options && index != currentSelectedSubOption) {

			if (index >= 0) {
				int l_endOffsetSpace = endOffset;
				int l_startOffsetSpace = startOffset;
				int l_displacement;

				if (index > endOffset) {
					l_displacement = index - endOffset;
					if (l_displacement > 1) {
						startOffset = startOffset + l_displacement;
						endOffset = endOffset + l_displacement;
						for (int i = 0; i < options.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								options [i].SetShowing (true);
							} else {
								options [i].SetShowing (false);
							}
						}
					} else {
						startOffset++;
						endOffset++;
						options [startOffset - 1].SetShowing (false);
						options [endOffset].SetShowing (true);
					}
				} else if (index < startOffset) {
					l_displacement = startOffset - index;
					if (l_displacement > 1) {
						startOffset = startOffset - l_displacement;
						endOffset = endOffset - l_displacement;
						for (int i = 0; i < options.Count; i++) {
							if (i >= startOffset && i <= endOffset) {
								options [i].SetShowing (true);
							} else {
								options [i].SetShowing (false);
							}
						}
					} else {
						startOffset--;
						endOffset--;
						options [startOffset].SetShowing (true);
						options [endOffset + 1].SetShowing (false);

					}
				}
			}

			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < options.Count)) {
				options [currentSelectedSubOption].SetSelected (false);
			}
			if ((index >= 0) && (index < options.Count)) {
				options [index].SetSelected (true);
			}
			currentSelectedSubOption = index;
		}
	}

	private void ResetCurrentSubOption() {
		if (currentOption == Options.Dialogs) {
			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < dialogs.Count)) {
				dialogs [currentSelectedSubOption].SetSelected (false);
			}
		} else if (currentOption == Options.Tutorials) {
			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < tutorials.Count)) {
				tutorials[currentSelectedSubOption].SetSelected (false);
			}
		} else if (currentOption == Options.Options) {
			if ((currentSelectedSubOption >= 0) && (currentSelectedSubOption < options.Count)) {
				options[currentSelectedSubOption].SetSelected (false);
			}
		}
		currentSelectedSubOption = -1;
	}

	private void SelectNextSubOptionAccesible() {
		buttonNextSubOption.Play ("Click");

		int offset = currentSelectedSubOption + 1;

		if (currentOption == Options.Dialogs) {

			if (offset > dialogs.Count - 1) {
				offset = 0;
			}

			for (int i = offset; i < dialogs.Count; i++) {
				if (dialogs [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = 0; i < offset; i++) {
				if (dialogs [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
	
		} else if (currentOption == Options.Tutorials) {

			if (offset > tutorials.Count - 1) {
				offset = 0;
			}

			for (int i = offset; i < tutorials.Count; i++) {
				if (tutorials [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = 0; i < offset; i++) {
				if (tutorials [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
		} else if (currentOption == Options.Options) {

			if (offset > options.Count - 1) {
				offset = 0;
			}

			for (int i = offset; i < options.Count; i++) {
				if (options [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = 0; i < offset; i++) {
				if (options [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
		}


	}

	private void SelectPreviousSubOptionAccesible() {
		buttonPreviousSubOption.Play ("Click");
		int offset = currentSelectedSubOption - 1;

		if (currentOption == Options.Dialogs) {

			if (offset < 0) {
				offset = dialogs.Count - 1;
			}
			for (int i = offset; i >= 0; i--) {
				if (dialogs [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = dialogs.Count - 1; i < offset; i--) {
				if (dialogs [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
		} else if (currentOption == Options.Tutorials) {
			if (offset < 0) {
				offset = tutorials.Count - 1;
			}

			for (int i = offset; i >= 0; i--) {
				if (tutorials [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = tutorials.Count - 1; i < offset; i--) {
				if (tutorials [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
		} else if (currentOption == Options.Options) {
			if (offset < 0) {
				offset = options.Count - 1;
			}

			for (int i = offset; i >= 0; i--) {
				if (options [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
			for (int i = options.Count - 1; i < offset; i--) {
				if (options [i].accesible) {
					SelectSubOption (i);
					return;
				}
			}
		}
	}



	private void SelectMenuZone(MenuZone aMenuZone) { //Buttons Animations
		if (aMenuZone != currentMenuZone) {

			//Activando botones
			if (aMenuZone == MenuZone.SubOptions) {
				buttonPreviousSubOption.SetBool ("Selected", false);
				buttonNextSubOption.SetBool ("Selected", false);
			} else if (aMenuZone == MenuZone.Options) {

			} else if (aMenuZone == MenuZone.RightWindow) {
				if (currentOption == Options.Tutorials) {
					if (tutorials [currentSelectedSubOption].numPages > 0) {
						buttonPreviousTutorialPage.SetBool ("Selected", true);
						buttonNextTutorialPage.SetBool ("Selected", false);
					} else {
						buttonPreviousTutorialPage.SetBool ("Selected", true);
						buttonNextTutorialPage.SetBool ("Selected", true);
					}
				}
			}

			//Desactivando botones
	
			if (aMenuZone != MenuZone.RightWindow) {
				if (currentOption == Options.Dialogs) {
					buttonPreviousComment.SetBool ("Selected", true);
					buttonNextComment.SetBool ("Selected", true);
				} else if (currentOption == Options.Tutorials) {
					buttonPreviousTutorialPage.SetBool ("Selected", true);
					buttonNextTutorialPage.SetBool ("Selected", true);
				}
			}

			if (aMenuZone != MenuZone.SubOptions) {
				buttonPreviousSubOption.SetBool ("Selected", true);
				buttonNextSubOption.SetBool ("Selected", true);
			}

			currentMenuZone = aMenuZone;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (accesible) {
			if (panelOpen) { //Panel de aviso de tutorial
				if (currentTimePanelDuration > 0) {
					currentTimePanelDuration -= Time.deltaTime;

					ManagePanelOpening (); //Controla si se abre el menu mientras el panel esta abierto
				} else {
					panel.Play ("Hide");
					currentTimePanelDuration = timePanelDuration;
					panelOpen = false;
				}
			} else {
				ManageOpening ();
			}

			if (panelAlertOpen) {
				if (currentTimePanelAlertDuration > 0) {
					currentTimePanelAlertDuration -= Time.deltaTime;
				} else {
					panelAlert.Play ("Hide");
					currentTimePanelAlertDuration = timePanelDuration;
					panelAlertOpen = false;
				}
			}
		}



			if (open) { //Ventana de menu
				ManageControllerInputAxis ();
				ManageSize ();
				ManageControls ();
			}
	}

	private void ManagePanelOpening() {
		if (Input.GetKeyDown (KeyCode.Return) || XCI.GetButtonDown (XboxButton.Start, controller)) {
			panel.Play ("Hide");
			currentTimePanelDuration = timePanelDuration;
			panelOpen = false;
			if (optionToOpen == Options.Tutorials) {
				ShowTutorial (subOptionToOpen);
			} else if (optionToOpen == Options.Dialogs){
				ShowDialog (subOptionToOpen);
			}
		}
	}

	private void ManageOpening() {
		if (open) {
			if ( (currentMenuZone == MenuZone.Options && Input.GetKeyDown (KeyCode.Escape)) || (currentMenuZone == MenuZone.Options && XCI.GetButtonDown(XboxButton.B, controller)) || XCI.GetButtonDown (XboxButton.Start, controller)) {
				CloseMenu ();
				AkSoundEngine.PostEvent ("UI_Back_1", transform.gameObject);
			}
		} else {
			if (Input.GetKeyDown (KeyCode.Escape) || XCI.GetButtonDown (XboxButton.Start, controller)) {
				OpenMenu ();
				AkSoundEngine.PostEvent ("UI_Click", transform.gameObject);
			}
		}
	}


	private void ManageControllerInputAxis () {
		if (controllerInputAxisTimers.timeButtonDown > 0.0f) {
			controllerInputAxisTimers.timeButtonDown -= Time.unscaledDeltaTime;
		} else {
			controllerInputAxisTimers.timeButtonDown = controllerInputAxisTimers.refreshTime;
		}

		controllerInputAxisTimers.pressedLeftStickXPositive = false;
		controllerInputAxisTimers.pressedLeftStickXNegative = false;
		controllerInputAxisTimers.pressedLeftStickYPositive = false;
		controllerInputAxisTimers.pressedLeftStickYNegative = false;
		controllerInputAxisTimers.pressedLeftTrigger = false;
		controllerInputAxisTimers.pressedRightTrigger = false;


		float value = XCI.GetAxis (XboxAxis.LeftStickX, controller);
		if (value >= controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.leftStickXPositive) {
				controllerInputAxisTimers.pressedLeftStickXPositive = true;
			}
			controllerInputAxisTimers.leftStickXPositive = true;
		} else {
			controllerInputAxisTimers.leftStickXPositive = false;
		}

		if (value <= -controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.leftStickXNegative) {
				controllerInputAxisTimers.pressedLeftStickXNegative = true;
			}
			controllerInputAxisTimers.leftStickXNegative = true;
		} else {
			controllerInputAxisTimers.leftStickXNegative = false;
		}

		value = XCI.GetAxis (XboxAxis.LeftStickY, controller);
		if (value >= controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.leftStickYPositive) {
				controllerInputAxisTimers.pressedLeftStickYPositive = true;
			}
			controllerInputAxisTimers.leftStickYPositive = true;
		} else {
			controllerInputAxisTimers.leftStickYPositive = false;
		}

		if (value <= -controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.leftStickYNegative) {
				controllerInputAxisTimers.pressedLeftStickYNegative = true;
			}
			controllerInputAxisTimers.leftStickYNegative = true;
		} else {
			controllerInputAxisTimers.leftStickYNegative = false;
		}

		value = XCI.GetAxis (XboxAxis.RightTrigger, controller);
		if (value >= controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.rightTrigger) {
				controllerInputAxisTimers.pressedRightTrigger = true;
			}
			controllerInputAxisTimers.rightTrigger = true;
		} else {
			controllerInputAxisTimers.rightTrigger = false;
		}

		value = XCI.GetAxis (XboxAxis.LeftTrigger, controller);
		if (value >= controllerInputAxisTimers.treshHold) {
			if (!controllerInputAxisTimers.leftTrigger) {
				controllerInputAxisTimers.pressedLeftTrigger = true;
			}
			controllerInputAxisTimers.leftTrigger = true;
		} else {
			controllerInputAxisTimers.leftTrigger = false;
		}
	}
			
	private bool GetControllerAxis (XboxAxis aAxis, bool aPositive) {
		float value = XCI.GetAxis (aAxis, controller);

		if (aPositive) {
			if (value >= controllerInputAxisTimers.treshHold) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value <= -controllerInputAxisTimers.treshHold) {
				return true;
			} else {
				return false;
			}
		}
	}
		

	private bool GetControllerAxisDown (XboxAxis aAxis, bool aPositive) {
		switch (aAxis) {
		case XboxAxis.LeftStickX:
			if (aPositive) {
				return controllerInputAxisTimers.pressedLeftStickXPositive;
			} else {
				return controllerInputAxisTimers.pressedLeftStickXNegative;
			}
		case XboxAxis.LeftStickY:
			if (aPositive) {
				return controllerInputAxisTimers.pressedLeftStickYPositive;
			} else {
				return controllerInputAxisTimers.pressedLeftStickYNegative;
				}
		case XboxAxis.LeftTrigger:
			return controllerInputAxisTimers.pressedLeftTrigger;
		case XboxAxis.RightTrigger:
			return controllerInputAxisTimers.pressedRightTrigger;
		}

		return false;
	}



	private void ManageControls() {
		if (currentMenuZone == MenuZone.Options) {
			if (Input.GetKeyDown (KeyCode.RightArrow) || XCI.GetButtonDown(XboxButton.DPadRight, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, true) || GetControllerAxisDown (XboxAxis.RightTrigger, true)) {
				SelectNextOption ();
				AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
			} else if (Input.GetKeyDown (KeyCode.LeftArrow) || XCI.GetButtonDown(XboxButton.DPadLeft, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, false) || GetControllerAxisDown (XboxAxis.LeftTrigger, true)) {
				SelectPreviousOption ();
				AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
			} else if (Input.GetKeyDown (KeyCode.Space) || XCI.GetButtonDown(XboxButton.A, controller) || Input.GetKeyDown (KeyCode.DownArrow) ||  XCI.GetButtonDown(XboxButton.DPadDown, controller) || GetControllerAxisDown (XboxAxis.LeftStickY, false)) {
				SelectMenuZone (MenuZone.SubOptions);
				SelectNextSubOptionAccesible ();
				AkSoundEngine.PostEvent ("UI_Click", transform.gameObject);
			}
		} else if (currentMenuZone == MenuZone.SubOptions) {
			if (currentOption == Options.Dialogs) { //SubOpciones de Dialogos
				if (Input.GetKeyDown (KeyCode.UpArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, true)  ||  XCI.GetButtonDown(XboxButton.DPadUp, controller) ) {
					SelectPreviousSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.DownArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, false) ||  XCI.GetButtonDown(XboxButton.DPadDown, controller) ) {
					SelectNextSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.Space) || XCI.GetButtonDown(XboxButton.A, controller) || Input.GetKeyDown (KeyCode.RightArrow) || XCI.GetButtonDown(XboxButton.DPadRight, controller) ) {
					dialogs [currentSelectedSubOption].subOptionAnimator.Play ("Click");
					SelectMenuZone (MenuZone.RightWindow);
					AkSoundEngine.PostEvent ("UI_Click", transform.gameObject);
				}
			} else if (currentOption == Options.Tutorials) {  //SubOpciones de Tutoriales
				if (Input.GetKeyDown (KeyCode.UpArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, true)  ||  XCI.GetButtonDown(XboxButton.DPadUp, controller)) {
					SelectPreviousSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.DownArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, false) ||  XCI.GetButtonDown(XboxButton.DPadDown, controller)) {
					SelectNextSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.Space) || XCI.GetButtonDown(XboxButton.A, controller) || Input.GetKeyDown (KeyCode.RightArrow) ||  XCI.GetButtonDown(XboxButton.DPadRight, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, true)) {
					tutorials [currentSelectedSubOption].subOptionAnimator.Play ("Click");
					SelectMenuZone (MenuZone.RightWindow);
					AkSoundEngine.PostEvent ("UI_Click", transform.gameObject);
				}
			}  else if (currentOption == Options.Options) {  //SubOpciones de Tutoriales
				if (Input.GetKeyDown (KeyCode.UpArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, true)  ||  XCI.GetButtonDown(XboxButton.DPadUp, controller)) {
					SelectPreviousSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.DownArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, false) ||  XCI.GetButtonDown(XboxButton.DPadDown, controller)) {
					SelectNextSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.Space) || XCI.GetButtonDown(XboxButton.A, controller) || Input.GetKeyDown (KeyCode.RightArrow) ||  XCI.GetButtonDown(XboxButton.DPadRight, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, true)) {
					options [currentSelectedSubOption].subOptionAnimator.Play ("Click");
					options [currentSelectedSubOption].Click ();
					AkSoundEngine.PostEvent ("UI_Click", transform.gameObject);
				}
			} 
			if (Input.GetKeyDown (KeyCode.Escape) || XCI.GetButtonDown(XboxButton.B, controller)) {
				SelectSubOption (-1);
				SelectMenuZone (MenuZone.Options);
				AkSoundEngine.PostEvent ("UI_Back_1", transform.gameObject);
			}
		} else if (currentMenuZone == MenuZone.RightWindow) {
			if (currentOption == Options.Dialogs) {
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					SelectMenuZone (MenuZone.SubOptions);
					AkSoundEngine.PostEvent ("UI_Back_1", transform.gameObject);
				} else if (Input.GetKey (KeyCode.UpArrow) ||  XCI.GetButton(XboxButton.DPadUp, controller) || GetControllerAxis(XboxAxis.LeftStickY, true)) {
					ScrollCommentsBackward ();
				} else if (Input.GetKey (KeyCode.DownArrow) ||  XCI.GetButton(XboxButton.DPadDown, controller)  || GetControllerAxis(XboxAxis.LeftStickY, false)) {
					ScrollCommentsForward ();
				} else {
					if (dialogs [currentSelectedSubOption].zeroScrollTop) {
						buttonPreviousComment.SetBool ("Selected", true);
					} else {
						buttonPreviousComment.SetBool ("Selected", false);
					}
					if (dialogs [currentSelectedSubOption].zeroScrollDown) {
						buttonNextComment.SetBool ("Selected", true);
					} else {
						buttonNextComment.SetBool ("Selected", false);
					}
				}

			} else if (currentOption == Options.Tutorials) {
				if (Input.GetKeyDown (KeyCode.UpArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, true) ||  XCI.GetButtonDown(XboxButton.DPadUp, controller)) {
					SelectMenuZone (MenuZone.SubOptions);
					SelectPreviousSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.DownArrow) || GetControllerAxisDown (XboxAxis.LeftStickY, false)||  XCI.GetButtonDown(XboxButton.DPadDown, controller)) {
					SelectMenuZone (MenuZone.SubOptions);
					SelectNextSubOptionAccesible ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.RightArrow) || XCI.GetButtonDown(XboxButton.DPadRight, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, true) || GetControllerAxisDown (XboxAxis.RightTrigger, true)) {
					SelectNextTutorialPage ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				} else if (Input.GetKeyDown (KeyCode.LeftArrow) ||  XCI.GetButtonDown(XboxButton.DPadLeft, controller) || GetControllerAxisDown (XboxAxis.LeftStickX, false) || GetControllerAxisDown (XboxAxis.LeftTrigger, true)) {
					SelectPreviousTutorialPage ();
					AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape) || XCI.GetButtonDown(XboxButton.B, controller)) {
				SelectMenuZone (MenuZone.SubOptions);
				AkSoundEngine.PostEvent ("UI_Back_1", transform.gameObject);
			}
		}
	}

	//Funciones de control de espacio
	private void FillSubOptions() {
		if (currentOption == Options.Dialogs) {
			for (int i = 0; i < tutorials.Count; i++) {
				tutorials [i].SetShowing (false);
			}

			for (int i = 0; i < dialogs.Count; i++) {
				if (i >= startOffset && i <= endOffset) {
					if (!dialogs [i].showing) {
						dialogs [i].SetShowing (true);
					}
				} else {
					dialogs [i].SetShowing (false);
				}
			}
		} else if (currentOption == Options.Tutorials) {
			for (int i = 0; i < dialogs.Count; i++) {
				dialogs [i].SetShowing (false);
			}

			for (int i = 0; i < tutorials.Count; i++) {
				if (i >= startOffset && i <= endOffset) {
					if (!tutorials [i].showing) {
						tutorials [i].SetShowing (true);
					}
				} else {
					tutorials [i].SetShowing (false);
				}
			}
		}
	}

		
	public void FillSubOptionsDelayed(float aSeconds) {
		UIUpdating = true;
		fillSubOptionsCoroutine = StartCoroutine (FillSubOptionsCoroutine (aSeconds));
	}

	IEnumerator FillSubOptionsCoroutine(float aSeconds) {
		if (currentOption == Options.Dialogs) {
			for (int i = 0; i < dialogs.Count; i++) {
				dialogs [i].SetShowing (false);
			}

			for (int i = 0; i < dialogs.Count; i++) {
				if (i >= startOffset && i <= endOffset) {
					if (!dialogs [i].showing) {
						dialogs [i].SetShowing (true);
					}
				} else {
					dialogs [i].SetShowing (false);
				}

				yield return new WaitForSecondsRealtime(aSeconds);
			}
		} else if (currentOption == Options.Tutorials) {
			for (int i = 0; i < tutorials.Count; i++) {
				tutorials [i].SetShowing (false);
			}

			for (int i = 0; i < tutorials.Count; i++) {
				if (i >= startOffset && i <= endOffset) {
					if (!tutorials [i].showing) {
						tutorials [i].SetShowing (true);
					}
				} else {
					tutorials [i].SetShowing (false);
				}

				yield return new WaitForSecondsRealtime(aSeconds);
			}
		} else if (currentOption == Options.Options) {
			for (int i = 0; i < options.Count; i++) {
				options [i].SetShowing (false);
			}

			for (int i = 0; i < options.Count; i++) {
				if (i >= startOffset && i <= endOffset) {
					if (!options [i].showing) {
						options [i].SetShowing (true);
					}
				} else {
					options [i].SetShowing (false);
				}

				yield return new WaitForSecondsRealtime(aSeconds);
			}

		}

		UIUpdating = false;
		fillSubOptionsCoroutine = null;
	}


	public void RecalculateSubOptions () {
		FillSubOptionsDelayed (timeBeetweenAppearSubOption);
	}
		

	public void ManageSize() { //Controla el espacio
		ManageLeftPanelSize();
	}

	public void ManageLeftPanelSize () {
		float l_disponibleHeight = 0; 
		float l_subOptionHeight = 0;
		float l_spacing = 0;
		if (currentOption == Options.Dialogs && dialogs.Count > 0) {
			l_spacing = dialogsLeftPanel.GetComponent<VerticalLayoutGroup> ().spacing;
			l_disponibleHeight = dialogsLeftPanel.GetComponent<RectTransform> ().rect.height + l_spacing;
			l_subOptionHeight = dialogs[0].subOption.GetComponent<RectTransform> ().rect.height + l_spacing;

		} else if (currentOption == Options.Tutorials && tutorials.Count > 0)  {
			l_spacing = tutorialLeftPanel.GetComponent<VerticalLayoutGroup> ().spacing;
			l_disponibleHeight = tutorialLeftPanel.GetComponent<RectTransform> ().rect.height + l_spacing ;
			l_subOptionHeight = tutorials [0].subOption.GetComponent<RectTransform> ().rect.height + l_spacing;
		}
		int l_newDisponibleSpacesSubOptions = Mathf.FloorToInt (l_disponibleHeight / l_subOptionHeight); // Se contrarresta el espacio de mas

		if (disponibleSpacesSubOptions != l_newDisponibleSpacesSubOptions) {
			disponibleSpacesSubOptions = l_newDisponibleSpacesSubOptions;

			endOffset = startOffset + disponibleSpacesSubOptions - 1;
			RecalculateSubOptions ();
		}
	}
		
	public Coroutine StartChildCoroutine (IEnumerator aIenumerator) {
		return StartCoroutine (aIenumerator);
	}

	public void StopChildCoroutine (Coroutine aCoroutine) {
		StopCoroutine (aCoroutine);
	}


	//Options
	public void ExitGame() {
		Application.Quit ();
	}

	public void LoadMainMenu () {
		SceneManager.LoadScene ("MainMenuFull");
		PauseGameController.Resume ();
	}

}
