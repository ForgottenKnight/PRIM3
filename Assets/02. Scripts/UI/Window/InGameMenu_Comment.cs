﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AK.Wwise;


public class InGameMenu_Comment : MonoBehaviour {
	public Text UIComment;
	public Text UICharacter;
	public Image UIImage;

	private string character;
	public string Character {
		get {
			return character;
		}

		set {
			character = value;
			if (UICharacter != null) {
				UICharacter.text = value;
			}
		}
	}

	private string comment;
	public string Comment {
		get {
			return comment;
		}

		set {
			comment = value;
			if (UIComment != null) {
				UIComment.text = value;
			}
		}
	}

    private string soundEvent;
    public string SoundEvent
    {
        get
        {
            return soundEvent;
        }

        set
        {
            soundEvent = value;
        }
    }

	public void SetSprite (Sprite aSprite) {
		UIImage.sprite = aSprite;
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void Hide() {
		gameObject.SetActive (false);
	}

    public void PlaySound()
    {
        if (soundEvent != "")
        {
            AkSoundEngine.PostEvent(soundEvent, gameObject);
        }
    }

    public void StopSound()
    {
        if (soundEvent != "")
        {
            AkSoundEngine.PostEvent("Stop" + soundEvent, gameObject);
        }
    }
}
