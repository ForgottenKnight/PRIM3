﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using System.Linq;
using UnityEngine.SceneManagement;
using AK.Wwise;


public class MainMenuNew : MonoBehaviour {
	[Header("Global variables")]
	public MenuModule currentModule = MenuModule.Main;
	public bool loadingScene;
	public int numPlayers = 0;
	public int availableGamepads = 0;
	public bool[] usedGamepads = new bool[3]; //0 -> Mando 1 | 1 -> Mando 2 | 2 -> Mando 3
	public bool usedKeyboard = false;

	private Camera camera;

	public enum Levels {
		IntroFull,
		Level2Full,
		Level3Full
	}

	public enum InputMode {
		Keyboard,
		Gamepad,
		All
	}
		
	public enum MenuModule {
		Main,
		Character,
		Level
	}

	[System.Serializable]
	public class Piece
	{
		public bool active;
		[HideInInspector]
		public MainMenuNew parent;
		public ParticleSystem particles;
		public bool selectedPiece;
		public float characterAnglePosition;
		public float characterAngleSize;
		public int pieceID; // 1- >Salt | 2 -> Sulphur | 3 -> Mercury

		public GameObject circle;
		protected bool moving = false;
		public bool firstStartFrame = true;
		public float speed = 10.0f;

		protected XboxController controller;
		protected InputMode input = InputMode.Keyboard;

		public virtual void Update () {
		}
			

		public virtual int SelectPiece (float aAngle, Selector aSelector) {
			return -1;
		}

		public virtual void ConfirmPiece(bool aConfirmed) {
			if (particles != null) {
				if (aConfirmed) {
					StaticParemeters.activeCharacters [pieceID] = true;
					particles.Play ();
				} else {
					StaticParemeters.activeCharacters [pieceID] = false;
					particles.Stop ();
				}
			}
		}


		public virtual void DeselectPiece () {
			selectedPiece = false;
		}

		public void setActive (bool aActive) {
			active = aActive;
		}
	}


	[Header("BackAndForward")]
	public BackAndForward backwardAndForward;

	[System.Serializable]
	public class BackAndForward {
		public BackAndForward () {
			
		}
			
		public float duration;
		private float currentDuration;
		public MainMenuNew parent;
		public Transform[] cameraPositions;
		private int currentPosition = 0;
		private Transform start;
		private Transform end;

		[HideInInspector]
		public bool moving = false;

		public void Start() {
			start = cameraPositions[0];
			end = cameraPositions[0];
		}

		public void Update() {
			ManageInput ();
		}

		public void ManageInput() {
			if (!moving) {


			} else {
				currentDuration += Time.deltaTime;
				parent.camera.transform.position = Vector3.Lerp (start.position, end.position, currentDuration/duration);
				parent.camera.transform.rotation = Quaternion.Lerp (start.rotation, end.rotation, currentDuration/duration);

				if (parent.camera.transform.position == end.position && parent.camera.transform.rotation == end.rotation) {
					moving = false;
				}
			}
		}

		public void Forward () {
			MenuModule l_newModule = (MenuModule)Mathf.Clamp (currentPosition + 1, 0, 2);
			SetModule (l_newModule);

			if (l_newModule == MenuModule.Character) {
				parent.pergaminoAnimator.Play ("MainMenu_HideMenu");
			}
		}

		public void Backward () {
			MenuModule l_newModule = (MenuModule)Mathf.Clamp (currentPosition - 1, 0, 2);
			SetModule (l_newModule);

			if (l_newModule == MenuModule.Main) {
				parent.pergaminoAnimator.Play ("MainMenu_ShowMenu");
				parent.playAnimator.Play ("MainMenu_OptionSelected");
			}
		}

		public void SetModule (MenuModule aModule) {
			if (parent.currentModule != aModule) {
				parent.ResetSelectors ();
				Debug.Log ("Activated module " + aModule);
				start = cameraPositions [currentPosition];
				currentPosition = (int)aModule;
				end = cameraPositions [currentPosition];
				moving = true;
				currentDuration = 0;
			

				parent.mainModule.active = false;

				switch (aModule) {
				case MenuModule.Main:
						parent.mainModule.active = true;
						break;
				case MenuModule.Character:
						break;
					case MenuModule.Level:
	
						break;
				}

				parent.currentModule = aModule;
			}
		}
	}	

	public void MenuForward() {
		backwardAndForward.Forward ();
	}

	public void MenuBackward() {
		backwardAndForward.Backward ();
	}

	[Header("MainModule")]
	public MainModule mainModule;
	public Animator playAnimator;
	public Animator exitAnimator;
	public Animator pergaminoAnimator;

	[System.Serializable]
	public class MainModule {
		public bool active = false;
		public MainMenuNew parent;
		public bool playOption = true;
		public bool canChangeOption = true;
	

		public void Start () {
			parent.playAnimator.Play ("MainMenu_OptionSelected");
		}

		public void Update () {
			if (active) { 
				bool l_changeOption = false;
				if (XCI.GetButtonDown (XboxButton.A) || Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.Return)) {
					if (playOption) {
						parent.MenuForward ();
						AkSoundEngine.PostEvent ("UI_Click", Camera.main.transform.gameObject);
						Debug.Log ("Forward");
					} else {
						parent.ExitGame ();
					}
				}

				float l_mean = Mathf.Abs (XCI.GetAxis (XboxAxis.LeftStickY));

				if (canChangeOption && l_mean > 0.5f) {
					canChangeOption = false;
					l_changeOption = true;
				} else if (XCI.GetButtonDown (XboxButton.DPadDown) || XCI.GetButtonDown (XboxButton.DPadUp) || Input.GetKeyDown (KeyCode.DownArrow) || Input.GetKeyDown (KeyCode.UpArrow)) {
					l_changeOption = true;

				}

				if (l_changeOption) {
					if (playOption) {
						parent.playAnimator.Play ("MainMenu_OptionDeselected");
						parent.exitAnimator.Play ("MainMenu_OptionSelected");
					} else {
						parent.exitAnimator.Play ("MainMenu_OptionDeselected");
						parent.playAnimator.Play ("MainMenu_OptionSelected");
					}
					AkSoundEngine.PostEvent ("UI_Select_1", Camera.main.transform.gameObject);
					playOption = !playOption;
				}

				if (l_mean < 0.1f) {
					canChangeOption = true;
				}
			}
		}
	}

	[Header("CharacterModule")]
	public Selector[] selectors;
	public CharacterPiece[] characterPiece;
	public Gradient lightColor;

	[System.Serializable]
	public class CharacterPiece : Piece{
		private Quaternion endAngle;
		private bool canRotate = true;

		public override void Update () {
			if (moving) {
				float step = speed * Time.deltaTime;
				if (circle.transform.localRotation != endAngle) {
					circle.transform.localRotation = Quaternion.RotateTowards (circle.transform.localRotation, endAngle, step);
				} else {
					AkSoundEngine.PostEvent("StopMovingPlatform", circle.transform.gameObject);
					moving = false;
				}
			} else {
				if (selectedPiece) {
					if (firstStartFrame) {
						firstStartFrame = false;
					} else {
						if (active) {
							if (input == InputMode.Keyboard) {
								if (Input.GetKeyDown (KeyCode.D) || Input.GetKeyDown (KeyCode.RightArrow) || Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown (KeyCode.LeftArrow)) {
									ChangeInput ();
								}
							} else {
								float l_axisX = XCI.GetAxis (XboxAxis.LeftStickX, controller);
								float l_axisY = XCI.GetAxis (XboxAxis.LeftStickY, controller);

								if (Mathf.Abs (l_axisX) >= 0.5f || Mathf.Abs (l_axisY) >= 0.5f) {
									if (canRotate) {
										canRotate = false;
										ChangeInput ();
									}
								} else if (Mathf.Abs (l_axisX) <= 0.1f || Mathf.Abs (l_axisY) <= 0.1f) {
									canRotate = true;
								}
							}
						}
					}
				}
			}
		}
			

		public override int SelectPiece (float aAngle, Selector aSelector) {
				if ((Mathf.Abs(Mathf.DeltaAngle (characterAnglePosition, aAngle)) < characterAngleSize)) {
					if (!selectedPiece) {
						input = aSelector.input;
						controller = aSelector.controller;
						selectedPiece = true;
						StaticParemeters.playerControllers[pieceID] = controller;
						StaticParemeters.players [pieceID] = aSelector.selectorID;
						firstStartFrame = true;
						aSelector.piece = this;
					return pieceID;
					}
				return -1;
				}
			return -1;
		}

		public override void DeselectPiece () {
			selectedPiece = false;
			particles.Stop ();
		}

		public void ChangeInput () {
			moving = true;
		
			endAngle = circle.transform.localRotation * Quaternion.Euler (0.0f , 0.0f, 180.0f);
			StaticParemeters.useKeyboard = !StaticParemeters.useKeyboard;
			StaticParemeters.playerWithKeyboard = pieceID;
			Debug.Log ("Usando teclado: " + StaticParemeters.useKeyboard);
			AkSoundEngine.PostEvent("MovingPlatform", circle.transform.gameObject);
		}
	}

	[Header("LevelModule")]
	public Selector selector;
	public LevelPiece[] levelPiece;


	[System.Serializable]
	public class LevelPiece : Piece {


		public override int SelectPiece (float aAngle, Selector aSelector) {
			if ((Mathf.Abs(Mathf.DeltaAngle (characterAnglePosition, aAngle)) < characterAngleSize)) {
				if (!selectedPiece) {
					input = aSelector.input;
					controller = aSelector.controller;
					selectedPiece = true;
					firstStartFrame = true;
					aSelector.piece = this;
					return pieceID;
				}
				return -1;
			}
			return -1;
		}
	}
		
	[System.Serializable]
	public class Selector {
		public Selector () {
		}
		//[HideInInspector]
		public bool active = false;
		public int selectorID; // 1- > Player1 | 2 -> Player2 | 3 -> Player3

		public MainMenuNew parent;
		public GameObject selector;
		public Light light;
		public Renderer materialRenderer;
		public XboxController controller;
		public InputMode input = InputMode.Keyboard;
		public List<Piece> pieces; 

		[HideInInspector]
		public Piece piece;


		//[HideInInspector]
		public bool blockInput = false;
		private bool movingSelector = false;
		public float duration = 1.0f;
		public float speed = 10.0f;

		public float[] radiusLevels ;
		private int currentRadiusLevel;
		private float currentTime;
		private Vector3 startPosition;
		private Quaternion endAngle;
		[HideInInspector]
		public int selectedPiece = -1;
		[HideInInspector]
		public bool ready = false;

		public float lightIntensity;
		public float lightTime;
		private float currentLightTime;

		[HideInInspector]
		public bool pieceSelected = false;

		public void Update () {
			ManageInput ();
			ManageColor ();
		}

		public void Activate (XboxController aController, InputMode aInput, int aSelectorID) {
			if (!active) {
				Debug.Log ("Activado selector " + aSelectorID);
				selectorID = aSelectorID;
				active = true;
				controller = aController;
				input = aInput;
				SetRadiusLevel (1);
			}
		}

		public void DeActivate () {
			if (active) {
				Debug.Log ("Desactivado selector " + selectorID);
				SetRadiusLevel (0);
				active = false;
				ready = false;
				currentLightTime = 0.0f;
				light.intensity = 0.0f;
				materialRenderer.materials [0].color = Color.white;
				//AkSoundEngine.PostEvent("MovingPlatform", selector.transform.gameObject);

				if (piece != null) {
					piece.DeselectPiece ();
				}
				selectedPiece = 0;
				pieceSelected = false;
				parent.numPlayers = Mathf.Clamp(parent.numPlayers - 1, 0, 3);
				if (input == InputMode.Keyboard) {
					parent.usedKeyboard = false;
				} else if (input == InputMode.Gamepad) {
					parent.usedGamepads [((int)controller) - 1] = false;
				}
			}
		}

		public void ToInitialPosition() {
			SetRadiusLevel (1);
			if (piece != null) {
				piece.DeselectPiece ();
			}
			selectedPiece = 0;
			pieceSelected = false;
		}



		public void ManageInput () {

			if (!blockInput) {
				if (active) {
					//Cambio estado
					if (currentRadiusLevel == 0) {

					} else if (currentRadiusLevel == 1) {
						bool l_moveLastFrame = movingSelector;

						if (input == InputMode.Gamepad || input == InputMode.All) {

							float l_axisX;
							float l_axisY;

							//Rotar
							if (controller == XboxController.All) {
								l_axisX = XCI.GetAxis (XboxAxis.LeftStickX);
								l_axisY = XCI.GetAxis (XboxAxis.LeftStickY);
							} else {
								l_axisX = XCI.GetAxis (XboxAxis.LeftStickX, controller);
								l_axisY = XCI.GetAxis (XboxAxis.LeftStickY, controller);
							}


							if (l_axisX != 0 || l_axisY != 0) {
								endAngle = Quaternion.Euler (0.0f, 0.0f, Mathf.Atan2 (l_axisX, l_axisY) * Mathf.Rad2Deg);

								float step = speed * Time.deltaTime;

								selector.transform.localRotation = Quaternion.RotateTowards (selector.transform.localRotation, endAngle, step);
								movingSelector = true;
							} else {
								movingSelector = false;
							}
						}

						if (input == InputMode.Keyboard || input == InputMode.All) {
							if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
								selector.transform.localRotation = selector.transform.localRotation * Quaternion.Euler (0, 0, speed * Time.deltaTime);
								movingSelector = true;
							} else if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
								selector.transform.localRotation = selector.transform.localRotation * Quaternion.Euler (0, 0, -speed * Time.deltaTime);
								movingSelector = true;
							}
						} 

						if (l_moveLastFrame != movingSelector) {
							if (movingSelector) {
								AkSoundEngine.PostEvent("MovingPlatform", selector.transform.gameObject);
							} else {
								AkSoundEngine.PostEvent("StopMovingPlatform", selector.transform.gameObject);
							}
						}

						//Aceptar
						if (((input == InputMode.Gamepad || input == InputMode.All) && XCI.GetButtonDown (XboxButton.A, controller)) || ((input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Return) || (input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Space))) {
							int l_index;
							for (l_index = 0; l_index < pieces.Count; l_index++) {
								selectedPiece = pieces [l_index].SelectPiece (selector.transform.localEulerAngles.z, this);
								if (selectedPiece >= 0) {
									AkSoundEngine.PostEvent("UI_Click", Camera.main.transform.gameObject);
									piece.setActive (true);
									selectedPiece = l_index;
									pieceSelected = true;
									SetRadiusLevel (2);
									break;
								}
							}
						}

						//Atras
						if (((input == InputMode.Gamepad || input == InputMode.All) && XCI.GetButtonDown (XboxButton.B, controller)) || ((input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Escape))) {
							DeActivate ();
							AkSoundEngine.PostEvent("UI_Back_1", Camera.main.transform.gameObject);
						}
					} else if (currentRadiusLevel == 2) {
						//Atras
						if (((input == InputMode.Gamepad || input == InputMode.All) && XCI.GetButtonDown (XboxButton.B, controller)) || ((input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Escape))) {
							if (ready) {
								ready = false;
								piece.ConfirmPiece (false);
								Debug.Log ("Confirm Piece false");
							} else {
								SetRadiusLevel (1);
								piece.DeselectPiece ();
								Debug.Log ("Deselect Piece");
							}
							AkSoundEngine.PostEvent("UI_Back_1", Camera.main.transform.gameObject);
						}

						//Acceptar
						if (((input == InputMode.Gamepad || input == InputMode.All) && XCI.GetButtonDown (XboxButton.A, controller)) || ((input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Space))) {
							if (!ready) {
								ready = true;
								piece.ConfirmPiece (true);
								piece.setActive (false);
								Debug.Log ("Confirm Piece true");
								AkSoundEngine.PostEvent("UI_Click", Camera.main.transform.gameObject);
							}
						}
					}
				} else {
					//Atras
					if (currentRadiusLevel == 0 && parent.numPlayers == 0) {
						if (((input == InputMode.Gamepad || input == InputMode.All) && XCI.GetButtonDown (XboxButton.B, controller)) || ((input == InputMode.Keyboard || input == InputMode.All) && Input.GetKeyDown (KeyCode.Escape))) {
							Debug.Log ("Clicked back");
							parent.backwardAndForward.Backward ();
							AkSoundEngine.PostEvent("UI_Back_1", Camera.main.transform.gameObject);
						}
					}
				}
			} else {
				
				currentTime += Time.deltaTime;
				float l_lerp = currentTime / duration;
				selector.transform.GetChild(0).localPosition = Vector3.Lerp (startPosition, new Vector3 (startPosition.x, radiusLevels [currentRadiusLevel], startPosition.z), l_lerp);

				if (l_lerp >= 1.0f) {
					blockInput = false;
					AkSoundEngine.PostEvent("StopMovingPlatform", selector.transform.gameObject);
				}
			}
		}

		public void ChangeInput () {
			//inputKeyboard = !inputKeyboard;
		}

		public void ManageColor () {
			if (active) {
				float step = selector.transform.localRotation.eulerAngles.z / 360;
				Color l_color = parent.lightColor.Evaluate (step);
				light.color = l_color;
				materialRenderer.materials[0].color = l_color;

				if (currentLightTime < lightTime) {
					currentLightTime += Time.deltaTime;
					light.intensity = Mathf.Lerp (0, lightIntensity, currentLightTime / lightTime);
				}
			}

		}

		private void SetRadiusLevel(int aLevel) {
			if (currentRadiusLevel != aLevel) {
				currentTime = 0.0f;
				startPosition = selector.transform.GetChild (0).transform.localPosition;
				currentRadiusLevel = aLevel;
				blockInput = true;
				AkSoundEngine.PostEvent("MovingPlatform", selector.transform.gameObject);
			}
		}
	}

	// Use this for initialization
	void Start () {
		StaticStatistics.Initialize (); //Reinicia todas las estadisticas

		camera = Camera.main;

		for (int i = 0; i < levelPiece.Length; i++) {
			selector.pieces.Add (levelPiece[i]);
		}

		for(int i = 0; i < selectors.Length; i++) {
			for (int n = 0; n < selectors.Length; n++) {
				selectors[i].pieces.Add (characterPiece[n]);
			}
		}

		mainModule.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		ManageInput ();
	}
		
	private void ManageInput () {
		availableGamepads = 0;

		//Gestion de desactivado de un mando
		for (int i = 1; i < 4; i++) {
			if (XCI.IsPluggedIn (i)) {
				availableGamepads++;
			} else {
				usedGamepads [i - 1] = false;
				for (int n = 0; n < selectors.Length; n++) {
					if (selectors [n].controller == (XboxController)i) {
						selectors [n].DeActivate ();
					}
				}
			}
		}

		//Updates
	
		for (int i = 0; i < characterPiece.Length; i++) {
			characterPiece [i].Update ();
		}
		for (int i = 0; i < selectors.Length; i++) {
			selectors [i].Update ();
		}
		mainModule.Update ();
		selector.Update ();
		backwardAndForward.Update ();	

		if (!backwardAndForward.moving) {
			switch (currentModule) {
			case MenuModule.Main:
				break;
			case MenuModule.Character:

				//Gestiona a quien se le asigna un mando cuando se entra en selección de PJ
				for (int i = 1; i < 4; i++) {
					if (!usedGamepads [i - 1]) {
						if (XCI.GetButtonDown (XboxButton.A, (XboxController)i)) {
							for (int n = 0; n < selectors.Length; n++) {
								if (!selectors [n].active) {
									selectors [n].Activate ((XboxController)i, InputMode.Gamepad, numPlayers);
									usedGamepads [i - 1] = true;
									numPlayers = Mathf.Clamp(numPlayers + 1, 0, 3);
									AkSoundEngine.PostEvent("UI_Click", Camera.main.transform.gameObject);
									Debug.Log ("Mando " + (XboxController)i + " asignado");
									break;
								}
							}
						}
					}
				}

				//Gestiona a quien se le asigna el teclado cuando se entra en selección de PJ
				if (!usedKeyboard) {
					if (Input.GetKeyDown (KeyCode.Space)) {
						for (int i = 0; i < selectors.Length; i++) {
							if (!selectors [i].active) {
								selectors [i].Activate (XboxController.First, InputMode.Keyboard, numPlayers);
								numPlayers = Mathf.Clamp(numPlayers + 1, 0, 3);
								usedKeyboard = true;
								AkSoundEngine.PostEvent("UI_Click", Camera.main.transform.gameObject);
								break;
							}
						}
					}
				}
				int ready_characters = 0;

				//Update de los selectores
				for (int i = 0; i < selectors.Length; i++) {
					if (selectors [i].pieceSelected && !selectors [i].blockInput && selectors [i].ready ) {
						ready_characters += 1;
					}
				}

				if (ready_characters == numPlayers && numPlayers >= 1) {
					StaticParemeters.numPlayers = numPlayers;
					backwardAndForward.Forward();

					//selector.Activate (XboxController.All, InputMode.All); <-- Ideal pero no va
					selector.Activate (XboxController.All, InputMode.All, 0);
				}
					
				break;
			case MenuModule.Level:
				if (!loadingScene) {
					if (selector.pieceSelected) {	
						AkSoundEngine.PostEvent("StopMovingPlatform", transform.gameObject);
						loadingScene = true;
						Levels l_levelToLoad = (Levels)selector.selectedPiece;
						StaticParemeters.savedConfiguration = true;
						StartCoroutine (LoadGameScene (l_levelToLoad.ToString ()));
					}
				}
				break;

			}
		}
	}


	private void ResetSelectors() {
		Debug.Log ("Reseting selectors");
		for (int i = 0; i < selectors.Length; i++) {
			selectors [i].DeActivate ();
		}
	}

	private void ToInitSelectors() {
		Debug.Log ("To init selectors");
		for (int i = 0; i < selectors.Length; i++) {
			selectors[i].ToInitialPosition();
		}
	}

	private void SetInactiveSelectors(bool aActive) {
		Debug.Log ("SetActive selectors");
		for (int i = 0; i < selectors.Length; i++) {
			selectors [i].DeActivate ();
		}
	}

	IEnumerator LoadGameScene (string aScene) {
		yield return null;

		AsyncOperation l_sceneLoaded = SceneManager.LoadSceneAsync (aScene);
		l_sceneLoaded.allowSceneActivation = false;

		while (!l_sceneLoaded.isDone) {
			if (l_sceneLoaded.progress == 0.9f) {

				l_sceneLoaded.allowSceneActivation = true;
			}
		
			yield return null;
		}
	}

	public void ExitGame() {
		Application.Quit ();
	}
}
