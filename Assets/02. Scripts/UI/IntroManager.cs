﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using UnityEngine.UI;
using AK.Wwise;
using UnityEngine.SceneManagement;

public class IntroManager : MonoBehaviour {

    public int steps = 4;
    public Text storyText;
    public DialogsData dialogsData;
    public List<string> dialogComments;
    public List<string> dialogAudios;
    public string loadLevel;
    public bool activateLevelLoad = false;
    public KeyCode keyboardSkip = KeyCode.Space;
    AsyncOperation m_async;

    Animator m_Anim;
    int m_textIndex;
    int m_AudiosIndex;

    void Start()
    {
        m_Anim = GetComponent<Animator>();
        dialogComments = new List<string>();
        LoadIntro();
        m_textIndex = 0;
        storyText.text = dialogComments[m_textIndex];
        StartCoroutine("LoadLevel");
    }

	// Update is called once per frame
	void Update () {
        if (activateLevelLoad && m_async != null)
        {
            m_async.allowSceneActivation = true;
            activateLevelLoad = false;
        }

        if (XCI.GetButtonDown(XboxButton.A) || Input.GetKeyDown(keyboardSkip))
        {
            m_Anim.SetTrigger("Intro");
            ++m_textIndex;
            if (m_textIndex < dialogComments.Count)
            {
                storyText.text = dialogComments[m_textIndex];
                if (m_textIndex > 0)
                {
                    if (m_textIndex > 1)
                    {
                        AkSoundEngine.PostEvent("Stop" + dialogAudios[m_textIndex - 1], gameObject);
                    }
                    AkSoundEngine.PostEvent(dialogAudios[m_textIndex], gameObject);
                }
            }
            else
            {
                if (m_textIndex - 1 < dialogAudios.Count)
                {
                    AkSoundEngine.PostEvent("Stop" + dialogAudios[m_textIndex - 1], gameObject);
                }
                storyText.text = "";
            }
        }
	}

    void LoadIntro()
    {
        DialogsData.DialogData l_dialogData = dialogsData.dialogs [0];
        
        for (int i = 0; i < l_dialogData.commentsData.Count; ++i) {
            dialogComments.Add(l_dialogData.commentsData [i].comment);
            dialogAudios.Add(l_dialogData.commentsData[i].soundEvent);
        }
    }

    IEnumerator LoadLevel()
    {
        m_async = SceneManager.LoadSceneAsync(loadLevel);
        m_async.allowSceneActivation = false;
        yield return m_async;
        Debug.Log("Loading complete");
    }
}
