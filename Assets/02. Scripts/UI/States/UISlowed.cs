﻿using UnityEngine;
using System.Collections;

public class UISlowed : UIState
{
	private Movement movement;

	protected  override void Start () {
		ReferenceDependencies ();
	}

	protected override void ReferenceDependencies() {
		movement = character.GetComponent<Movement> ();
	}

	protected override void UpdateUI() {
		if (movement != null) {
			if (movement.slowDown) {
				SetUIActive (true);
			} else {
				SetUIActive (false);
			}
		}
	}
}
