﻿using UnityEngine;
using System.Collections;

public class UIInvulnerable : UIState {
	private Health health;

	// Use this for initialization
	protected  override void Start () {
		ReferenceDependencies ();
	}

	protected override void ReferenceDependencies() {
		health = character.GetComponent<Health> ();
	}


	protected override void UpdateUI() {
		if (health != null) {
			if (health.invincible && health.GetHealth () > 0) {
				SetUIActive (true);
			} else {
				SetUIActive (false);
			}
		}
	}
}
