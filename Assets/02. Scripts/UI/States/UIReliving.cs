﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIReliving : UIState {
	public Image relivingImage;
	private Incapacitate incapacitate;

	// Use this for initialization
	protected  override void Start () {
		ReferenceDependencies ();
	}

	protected override void ReferenceDependencies() {
		incapacitate = character.GetComponent<Incapacitate> ();
	}

	protected override void UpdateUI() {
		if (incapacitate != null) {
			GameObject RescueArea = incapacitate.RescArea;
			if (RescueArea != null) {
				RescueTrigger rt = RescueArea.GetComponent<RescueTrigger> ();
				if (rt) {
					relivingImage.fillAmount = rt.getRescueAsUnit ();
					SetUIActive (true);
				} else {
					SetUIActive (false);
				}
			}
		}
	}
}
