﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.Assertions;
using AK.Wwise;

public class IngameDialogManager : MonoBehaviour {
	//public TextAsset fileXML;
    public static IngameDialogManager instance;
	struct IngameDialogEvent {
		public string name;
		public string soundEvent;
		public int character;
		public string text;
	}
	private Dictionary<string, IngamedialogsData.IngamedialogData> dialogByKey;

	private List<IngameDialog> playerDialogs;

	private InGameMenu ingameMenu;

	public IngamedialogsData dialogsData;

	bool isShowingDialog;
    bool isNoxDialogShowing;

	// Use this for initialization
	void Start () {
        IngameDialogManager.instance = this;
		ingameMenu = GameObject.FindObjectOfType<InGameMenu> ();
		dialogByKey = new Dictionary<string, IngamedialogsData.IngamedialogData> ();
		Initialize();
		playerDialogs = new List<IngameDialog> ();
		GetIngameDialogs ();
	}

	void Initialize() {
		for (int i = 0; i < dialogsData.dialogs.Count; ++i) {
			IngamedialogsData.IngamedialogData dialog = dialogsData.dialogs [i];
			for (int j = 0; j < dialog.dialogs.Count; ++j) {
				if (dialog.dialogs [j].dialog == "") {
					Debug.LogError ("Dialogo sin texto en el evento " + dialog.name + ", diálogo número " + j+1);
				}
			}
			dialogByKey.Add (dialog.name, dialog);
		}
	}
	
	public void ShowEvent(string eventName) {
		if (dialogByKey.ContainsKey (eventName)) {
			IngamedialogsData.IngamedialogData data = dialogByKey [eventName];
			StartCoroutine(DialogWait(data));
		}
	}

	public void ShowIfNotShowing(string eventName, float probability = 1f) {
		if (!DialogShowing && Random.Range(0f, 1f) < probability) {
			ShowEvent (eventName);
		}
	}

	public void ShowRandomCombatEvent(int eventNumber) {
		// Ej: G_P0_2 donde 0 es el player y 2 el numero de evento
		int player = Random.Range(0, 2);
		string result = "G_P" + player.ToString () + "_" + eventNumber.ToString();
		ShowEvent (result);
	}

	IEnumerator DialogWait(IngamedialogsData.IngamedialogData data) {
		while (isShowingDialog) {
			yield return null;
		}
		isShowingDialog = true;
		StartCoroutine (DialogSequencePlayer (data));
	}

	IEnumerator DialogSequencePlayer(IngamedialogsData.IngamedialogData data) {
		for (int i = 0; i < data.dialogs.Count; ++i) {
			IngamedialogsData.IngamedialogData.Dialog dialog = data.dialogs [i];
			playerDialogs [dialog.character].ShowDialog (dialog.dialog, dialog.duration, dialog.dialogSprite, dialog.soundEvent);
            if(dialog.character == 3)
            {
                isNoxDialogShowing = true;
            }

			if (dialog.unlockDialog != -1) {
				ingameMenu.SetDialogAccesible(dialog.unlockDialog);
			}
			if (dialog.dontWaitForNext == false) {
				yield return new WaitForSeconds (dialog.duration);
			}
		}

        isNoxDialogShowing = false;
		isShowingDialog = false;
	}

	private IngameDialog GetIngameDialog(int character) {
		List<GameObject> players =  CustomTagManager.GetObjectsByTag ("Player");
		foreach (GameObject player in players) {
			if (player.GetComponent<GeneralPlayerController> ().character == character) {
				return player.GetComponent<IngameDialog> ();
			}
		}
		return null;
	}

	private void GetIngameDialogs() {
		List<GameObject> players =  CustomTagManager.GetObjectsByTag ("Player");
		for (int i = 0; i < 4; ++i) {
			playerDialogs.Add (players[0].GetComponent<IngameDialog>()); // Para tener 3 slots en la lista
		}
		foreach (GameObject player in players) {
			int character = player.GetComponent<GeneralPlayerController> ().character;
			playerDialogs [character] = player.GetComponent<IngameDialog> ();
		}
		MSMS_Nox_Active nox = FindObjectOfType<MSMS_Nox_Active> ();
		if (nox != null) {
			playerDialogs[3] = nox.gameObject.GetComponent<IngameDialog> ();
		}
	}

    void OnDestroy()
    {
        IngameDialogManager.instance = null;
    }

	public bool DialogShowing {
		get {
			return isShowingDialog;
		}
	}

    public bool NoxDialogShowing
    {
        get
        {
            return isNoxDialogShowing;
        }
    }
}
