﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DroppedItem : MonoBehaviour {

	public float HealAmmount = 20;

	private Transform m_PlayerContainer;

    [HideInInspector]
    public float m_timer = 0.0f;

	public GameObject healEffect;

	// Use this for initialization
	void Start () {
		m_PlayerContainer = GameObject.FindGameObjectWithTag("PlayerContainer").transform;
	}
	
	// Update is called once per frame
	void Update () {
        m_timer += Time.deltaTime;	
	}

	void HealEffect() {
		if (healEffect != null) {
			List<GameObject> players = CustomTagManager.GetObjectsByTag ("Player");
			foreach (GameObject player in players) {
				if (player.activeSelf)
					Instantiate (healEffect, player.transform.position, transform.rotation);
			}
		}
	}

	void OnTriggerEnter(Collider col) 
	{
        if (m_timer >= 1.0f)
        {
            if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                for (int i = 0; i < m_PlayerContainer.childCount; ++i)
                {
                    Health l_PlayerHealth = m_PlayerContainer.GetChild(i).GetComponent<Health>();

                    if (l_PlayerHealth && l_PlayerHealth.health > 0)
                    {
                        l_PlayerHealth.Heal(HealAmmount);
                    }
                }
				HealEffect ();
                Destroy(transform.parent.gameObject);
            }
        }
	}


    void OnTriggerStay(Collider col)
    {
        if (m_timer >= 1.0f)
        {
            if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                for (int i = 0; i < m_PlayerContainer.childCount; ++i)
                {
                    Health l_PlayerHealth = m_PlayerContainer.GetChild(i).GetComponent<Health>();

                    if (l_PlayerHealth && l_PlayerHealth.health > 0)
                    {
                        l_PlayerHealth.Heal(HealAmmount);
                    }
                }
				HealEffect ();
                Destroy(transform.parent.gameObject);
            }
        }
    }
}
