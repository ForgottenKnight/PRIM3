﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeCollider : MonoBehaviour {


    [HideInInspector]
    public float damage;
    [HideInInspector]
    public float pushTime;
    [HideInInspector]
    public float pushSpeed;

	[HideInInspector]
	public GameObject hitEffect;
	[HideInInspector]
	public Vector3 hitSize = Vector3.one;
	[HideInInspector]
	public Vector3 hitOffset = Vector3.zero;


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player") || collision.gameObject.layer == LayerMask.NameToLayer("JumpingPlayer") || collision.gameObject.layer == LayerMask.NameToLayer("SaltShield"))
        {
            Damageable h = collision.gameObject.GetComponent<Damageable>();
			if (h) {
				if (h.Damage(damage) > 0.0f) {
                    PushBack(collision.gameObject);
					if (hitEffect != null) {
						GameObject hitEf = (GameObject) Instantiate (hitEffect, h.transform.position + hitOffset, Quaternion.identity);
						Vector3 effectScale = hitEf.transform.localScale;
						effectScale.x *= hitSize.x;
						effectScale.y *= hitSize.y;
						effectScale.z *= hitSize.z;
						hitEf.transform.localScale = effectScale;
					}
				}
			}
		}

	}

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("SaltShield"))
        {
            Damageable h = collision.gameObject.GetComponent<Damageable>();
            if (h)
            {
                if (h.Damage(damage) > 0.0f)
                {
                    PushBack(collision.gameObject);
                }
            }
        }
    }

	void PushBack(GameObject go) {
		IPushable p = go.GetComponent<IPushable> ();
		if (p != null) {
			p.Push(pushSpeed, pushTime, transform.position);
		}
	}
}
