﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class Feathers : MonoBehaviour
{
    [Header("Lightning parameters")]
    public float effectTime = 2.5f;
    public float activationTime = 3f;
    public float damageArea = 3f;
    public float destoryAfer = 1.5f;
    public int damage = 15;
    public ParticleSystem[] activationSystems;
    public ParticleSystem[] afterEffects;


    // Use this for initialization
    void Start()
    {
        StartCoroutine(DoFeathers());
    }

    void Delete()
    {
        Destroy(gameObject);
    }

    IEnumerator DoFeathers()
    {
        yield return new WaitForSeconds(effectTime);
        for (int i = 0; i < activationSystems.Length; ++i)
        {
            activationSystems[i].Play();
        }
        yield return new WaitForSeconds(activationTime - effectTime);


        for (int i = 0; i < activationSystems.Length; ++i)
        {
            afterEffects[i].Play();
            AkSoundEngine.PostEvent("Feathers", gameObject);
        }
        List<GameObject> l_Players = CustomTagManager.GetObjectsByTag("Player");
        for (int i = 0; i < l_Players.Count; ++i)
        {
            staticAttackCheck.checkAttack(l_Players[i].transform, transform, damageArea, 360f, damageArea, damage);           
        }
        GameObject l_SaltShield = CustomTagManager.GetObjectByTag("SaltShield");
        if (l_SaltShield != null)
        {
            staticAttackCheck.checkAttack(l_SaltShield.transform, transform, damageArea, 360f, damageArea, damage);
        }
          Invoke("Delete", destoryAfer);
     //   Destroy(gameObject);
        yield return null;
    }
}
