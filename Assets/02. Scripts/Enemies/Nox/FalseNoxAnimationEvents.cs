﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class FalseNoxAnimationEvents : MonoBehaviour {
    public SkinnedMeshRenderer mesh;
    public ParticleSystem airEffect;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound(string EventName)
    {
        AkSoundEngine.PostEvent(EventName, transform.parent.gameObject);
    }

    public void PlayAirEffect()
    {
        if(airEffect)
        {
            airEffect.Play();
        }
    }

    public void FeartherLand()
    {
    }


    public void FeatherAttack()
    {
    }


    public void PicotazoDamage()
    {
    }

    public void WingDamage()
    {
    }

    public void HealEnd()
    {
    }


    public void Fly()
    {
    }

    public void HideMesh()
    {
       /* if(mesh)
        {
            mesh.enabled = false;
        }*/
    }

    public void ShowMesh()
    {
       /* if (mesh)
        {
            mesh.enabled = true;
        }*/
    }

    public void Land()
    {
    }
}
