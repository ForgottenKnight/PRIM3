﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class NoxAnimationEvents : MonoBehaviour {

    public MSMS_Nox_Active_Picotazo picotazoScript;
    public MSMS_Nox_Active_Heal HealScript;
    public MSMS_Nox_Active_Vuelo FlyScript;
    public MSMS_Nox_Active_WingAttack wingScript;
    public MSMS_Nox_Active_Feathers featherScript;
    public SkinnedMeshRenderer mesh;
    public ParticleSystem airEffect;
    private IngameDialogManager idm;

	// Use this for initialization
	void Start () {
        idm = GameObject.FindObjectOfType<IngameDialogManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound(string EventName)
    {
        bool playSound = true;

        if (EventName == "NoxWingAttack" || EventName == "NoxAttacks")
        {
            playSound = !idm.NoxDialogShowing;
        }

        if (playSound)
        {
            AkSoundEngine.PostEvent(EventName, transform.parent.gameObject);
        }
    }

    public void PlayAirEffect()
    {
        if(airEffect)
        {
            airEffect.Play();
        }
    }

    public void FeartherLand()
    {
        if(featherScript)
        {
            featherScript.Land();
        }
    }


    public void FeatherAttack()
    {
        if (featherScript)
        {
            featherScript.DoFeathers();
        }
    }


    public void PicotazoDamage()
    {
        if(picotazoScript)
        {
            picotazoScript.DoAttack();
        }
    }

    public void WingDamage()
    {
        if (wingScript)
        {
            wingScript.DoAttack();
        }
    }

    public void HealEnd()
    {
        if (HealScript)
        {
            HealScript.DoneHealing();
        }
    }


    public void Fly()
    {
        if(FlyScript)
        {
            FlyScript.Flying();
        }
    }

    public void HideMesh()
    {
        if(mesh)
        {
            mesh.enabled = false;
        }
    }

    public void ShowMesh()
    {
        if (mesh)
        {
            mesh.enabled = true;
        }
    }

    public void Land()
    {
        if(FlyScript)
        {
            FlyScript.DoneFlying();
        }
    }
}
