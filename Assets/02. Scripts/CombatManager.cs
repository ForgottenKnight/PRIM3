﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;
using UnityEngine.SceneManagement;

public class CombatManager : MonoBehaviour {


    public bool inCombat = false;
    public bool canHealPlayers = true;
    public float healTimer = 5f;
    public float healAmmount = 1f;

    private float m_currentTimer;    
    private Transform m_Players;
    private Transform m_Enemies;
    public Health[] PlayerHealths;

	private IngameDialogManager idm;
	public float dialogProbability;
    public bool bossStarted = false;

    public string combatMusic = "CombatMusic";
    public string bossMusic = "NoxMusic";
    private string m_SceneName;

	// Use this for initialization
	void Start () {
        m_Players = GameObject.FindGameObjectWithTag("PlayerContainer").transform;
        m_Enemies = GameObject.FindGameObjectWithTag("EnemiesContainer").transform;
		idm = GameObject.FindObjectOfType<IngameDialogManager> ();
        m_currentTimer = 0f;
        Scene l_Scene = SceneManager.GetActiveScene();
        m_SceneName = l_Scene.name;
	}
	
	// Update is called once per frame
	void Update () {
        m_currentTimer += Time.deltaTime;
        if(!inCombat && canHealPlayers && m_currentTimer >= healTimer)
        {
            m_currentTimer = 0f;
            HealPlayers();
        }

	}

    void HealPlayers()
    {
        for (int i = 0; i < PlayerHealths.Length; ++i)
        {
            if (PlayerHealths[i].gameObject.activeSelf && PlayerHealths[i].health > 0f)
            {
                PlayerHealths[i].Heal(healAmmount);
            }
        }
    }

    void CheckCombat()
    {

    }

    public void SetBossActive(bool value)
    {
        bossStarted = value;
		if (value == true) {
			AkSoundEngine.PostEvent(bossMusic, gameObject);
		}
    }

    public void ActivateCombat()
    {
        if(!inCombat)
        {
            inCombat = true;
            if(m_SceneName == "Level3Full")
            {
                if (bossStarted)
                {
                    
                }
            }
            else
            {
                AkSoundEngine.PostEvent("TransitionToCombat", gameObject);
                AkSoundEngine.PostEvent(combatMusic, gameObject);
            }
        }
    }

	public void DeactivateCombat() {
		if (inCombat) {
			inCombat = false;
            AkSoundEngine.PostEvent("TransitionToMusic", gameObject);
			Invoke ("DoEndCombatDialog", 2f);
		}
	}

	void DoEndCombatDialog() {
        if (!bossStarted && Random.Range(0f, 1f) < dialogProbability && !idm.DialogShowing)
        {
			idm.ShowRandomCombatEvent (2); // 2 son los eventos de fin de combate
		}
	}

   /* public void EnemyTargetsPlayer()
    {
        if(!inCombat)
        {
            inCombat = true;

        }
    }
    
    public void PlayerTargetsEnemy()
    {
        if (!inCombat)
        {
            inCombat = true;

        }
    }*/

    public bool CheckPlayerTargets(bool checkEnemies = true)
    {
        bool activeTargets = false;
        for(int i = 0; i < m_Players.childCount; ++i)
        {
            GameObject player = m_Players.GetChild(i).gameObject;
            TargetPointer targetPointer = player.GetComponent<TargetPointer>();
            activeTargets = activeTargets || targetPointer.active;
        }

        if (checkEnemies)
        {
            activeTargets = activeTargets || CheckEnemyTargets(false);
        }
        
		if (activeTargets) {
			ActivateCombat ();
		} else {
			DeactivateCombat ();
		}


        return inCombat;
    }


    public bool CheckEnemyTargets(bool checkPlayers = true)
    {
        bool activeTargets = false;
        for (int i = 0; i < m_Players.childCount; ++i)
        {
            GameObject player = m_Players.GetChild(i).gameObject;
            /*GameObject enemy = m_Enemies.GetChild(i).gameObject;
            TargetSelector targetSelector = enemy.GetComponent<TargetSelector>();*/
            GeneralPlayerController gpc = player.GetComponent<GeneralPlayerController>();
            if(gpc.EnemiesTargeting.Count > 0)
            {
                gpc.EnemiesTargeting.RemoveAll(TargetSelector => TargetSelector == null);
            }
            activeTargets = activeTargets || (gpc.EnemiesTargeting.Count > 0);
        }


        if (checkPlayers)
        {
            activeTargets = activeTargets || CheckPlayerTargets(false);
        }

		if (activeTargets) {
			ActivateCombat ();
		} else {
			DeactivateCombat ();
		}

        return activeTargets;
    }

}
