﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class StaticStatistics {

    static public float[] damageDone = new float[3];//0->Player1, 1->Player2, 2->Player3
    static public float[] damageRecieved = new float[3];//0->Player1, 1->Player2, 2->Player3
    static public float[] energySpent = new float[3];//0->Player1, 1->Player2, 2->Player3
    static public float[] damageAvoided = new float[3];//0->Player1, 1->Player2, 2->Player3
    static public float[] attackHit = new float[3];
    static public float[] attackMissed = new float[3];
	static public float[,] useTime = new float[3,3]; //Firt array ->Player, Second array -> Character

    static public float timer;
    static public int usedSuperAbility;
    static public bool nextLevel = false;
    static public bool restartLevel = false;


	// Use this for initialization
	static void Start () 
    {
       if (!restartLevel)
       {
           Initialize();
           restartLevel = true;
       }
	}

    static public void Initialize()
    {
        usedSuperAbility = 0;
        timer = 0f;
        nextLevel = false;
        for (int i = 0; i < 3; ++i)
        {
            energySpent[i] = 0f;
            damageRecieved[i] = 0f;
            damageDone[i] = 0f;
            damageAvoided[i] = 0f;
            attackHit[i] = 0f;
            attackMissed[i] = 0f;
        }
    }
	
	// Update is called once per frame
	void Update () {

	}

    static public void ShowStatistics()
    {
    }
}
