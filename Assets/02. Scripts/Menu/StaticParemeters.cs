﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class StaticParemeters{

	static public int numPlayers = 1;
	static public bool useKeyboard = false;
	static public int playerWithKeyboard = 0;
    static public bool Init = false;


    static public bool savedConfiguration = false;
	static public bool[] activeCharacters = new bool[3]; //Indica que personaje esta activo. //0->Salt, 1->Sulphur, 2->Mercury

    static public int[] players = new int[3]; //Indica que jugador controla cada personaje. 0->Salt, 1->Sulphur, 2->Mercury
	static public XboxController[] playerControllers = new XboxController[3]; //Indica que gamepad controla cada personaje. //0->Salt, 1->Sulphur, 2->Mercury

	/*
    static public void SaveConfiguration(GameObject Salt, GameObject Sulphur, GameObject Mercury)
    {
        savedConfiguration = true;
        characters[0] = Salt.GetComponent<GeneralPlayerController>().player;
        activeCharacters[0] = Salt.activeSelf;
        playerControllers[0] = Salt.GetComponent<GeneralPlayerController>().controller;

        characters[1] = Sulphur.GetComponent<GeneralPlayerController>().player;
        activeCharacters[1] = Sulphur.activeSelf;
        playerControllers[1] = Sulphur.GetComponent<GeneralPlayerController>().controller;

        characters[2] = Mercury.GetComponent<GeneralPlayerController>().player;
        activeCharacters[2] = Mercury.activeSelf;
        playerControllers[2] = Mercury.GetComponent<GeneralPlayerController>().controller;

    }*/
}
