﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatusManager : MonoBehaviour {
	public List<GameObject> playerList = new List<GameObject>();
	private List<Health> healthList = new List<Health>();
	private List<Incapacitate> incapacitateList = new List<Incapacitate>();

	private List<GameObject> aliveList = new List<GameObject>();
	private List<GameObject> deadList = new List<GameObject>();
	private List<GameObject> incapacitatedList = new List<GameObject>();
	private List<GameObject> notIncapacitatedList = new List<GameObject>();

	public bool endGameOnAllDead = false;
	private bool restarting = false;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < playerList.Count; i++) {
			Health l_health = playerList [i].GetComponent<Health> ();
			healthList.Add (l_health);

			Incapacitate l_incapacitated = playerList [i].GetComponent<Incapacitate> ();
			incapacitateList.Add (l_incapacitated);

			aliveList.Add (playerList [i]);
			notIncapacitatedList.Add (playerList [i]);
		}
	}
		
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < healthList.Count; i++) {
			if (healthList [i].health <= 0.0f) {
				if (aliveList.Contains (healthList [i].gameObject)) {
					aliveList.Remove (healthList [i].gameObject);
					deadList.Add (healthList [i].gameObject);
				}

			} else {
				if (!aliveList.Contains (healthList [i].gameObject)) {
					deadList.Remove (healthList [i].gameObject);
					aliveList.Add (healthList [i].gameObject);
				}
			}
		}

		for (int i = 0; i < incapacitateList.Count; i++) {
			if (incapacitateList [i].incapacitated) {
				if (notIncapacitatedList.Contains (incapacitateList [i].gameObject)) {
					notIncapacitatedList.Remove (incapacitateList [i].gameObject);
					incapacitatedList.Add (incapacitateList [i].gameObject);
				}
			} else {
				if (!notIncapacitatedList.Contains (incapacitateList [i].gameObject)) {
					incapacitatedList.Remove (incapacitateList [i].gameObject);
					notIncapacitatedList.Add (incapacitateList [i].gameObject);
				}
			}
		}

		if (endGameOnAllDead) {
			if (GetNumOfDeadPlayers () == StaticParemeters.numPlayers && !restarting) {
				Debug.Log ("All players dead, restarting game");
				CameraFade cf = gameObject.AddComponent<CameraFade> ();
				cf.SetScreenOverlayColor (new Color (0.0f, 0.0f, 0.0f, 0.0f));
				cf.AddCallback (ReturnToMainMenu);
				cf.StartFade (Color.black, 3.0f);
				restarting = true;
			}
		}
	}

	private void ReturnToMainMenu() {
		FinishGame.ReestartLevel();
	}

	public List<GameObject> GetIncapacitatedPlayers() {
		return incapacitatedList;
	}

	public List<GameObject> GetNonIncapacitatedPlayers() {
		return notIncapacitatedList;
	}

	public List<GameObject> GetDeadPlayers() {
		return deadList;
	}

	public List<GameObject> GetAlivePlayers() {
		return aliveList;
	}

	public List<GameObject> GetPlayers() {
		return playerList;
	}


	public int GetNumOfIncapacitatedPlayers() {
		return incapacitatedList.Count;
	}

	public int GetNumOfNonIncapacitatedPlayers() {
		return notIncapacitatedList.Count;
	}

	public int GetNumOfDeadPlayers() {
		return deadList.Count;
	}

	public int GetNumOfAlivePlayers() {
		return aliveList.Count;
	}
}
