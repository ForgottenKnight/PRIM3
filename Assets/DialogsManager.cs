﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using UnityEngine.UI;
using AK.Wwise;

public class DialogsManager : MonoBehaviour {

	public DialogsData dialogsData;

	[Header("Prefabs")]
	public GameObject commentRight;
	public GameObject commentLeft;
	public GameObject commentsGroup;

	[Header("Config")]
	public float timeForInput = 1.0f;
	public float timeBetweenComments = 0.5f;
	private float currentTimeBetweenComments = 0.0f;
	public float maxBackgroundOpacity = 0.2f;
	public float timeBackgroundOpacity = 1;
	private float currentTimeBackgroundOpacity = 0;

	private bool showingDialogs = false;
	private int numOfDialogs = 0;
	private int currentShowingDialog = -1;
	private int currentShowingComment = 0;
	private int startOffset = 0;
	private int endOffset = 0;
	private bool hasOffsets = false;


	[Header("Referencias")]
	public InGameMenu menu;
	private Image background;

	private GameObject currentDialog;

	private List<GameObject> dialogs = new List<GameObject>();

	// Use this for initialization
	void Start () {
		background = GetComponent<Image> ();
		LoadDialogs ();
	}
	
	// Update is called once per frame
	void Update () {
		if (showingDialogs) {
			ManageControls ();
		}
	}
	public void ManageControls() {
		currentTimeBetweenComments -= Time.unscaledDeltaTime;

		if ( (XCI.GetButtonDown (XboxButton.A) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return)) && currentTimeBetweenComments <= 0.0f) {
			currentTimeBetweenComments = timeBetweenComments;
			currentDialog.transform.GetChild (currentShowingComment).gameObject.GetComponent<Animator>().Play("Hide");

			int l_newCurrent = currentShowingComment + 1;

			if (hasOffsets) {
				if (l_newCurrent <= endOffset) {
					currentShowingComment = l_newCurrent;
					currentDialog.transform.GetChild (currentShowingComment).gameObject.SetActive (true);
				} else {
					HideCurrentDialog ();
					showingDialogs = false;
					hasOffsets = false;
					StartCoroutine (AllowInput(timeForInput));
					menu.accesible = true;
				}
			} else {
				if (l_newCurrent < currentDialog.transform.childCount) {
					currentShowingComment = l_newCurrent;
					currentDialog.transform.GetChild (currentShowingComment).gameObject.SetActive (true);
				} else {
					HideCurrentDialog ();
					showingDialogs = false;
					hasOffsets = false;
					StartCoroutine (AllowInput(timeForInput));
					menu.accesible = true;
				}
			}
			AkSoundEngine.PostEvent ("UI_Select_2", transform.gameObject);
		}
	}

	public IEnumerator AllowInput (float delay) {
		yield return new WaitForSecondsRealtime (delay);
		PauseGameController.Resume();
	}
		

	public void SetDialogsStartOffset (int aStartOffset) {
		startOffset = aStartOffset;
		hasOffsets = true;
	}

	public void SetDialogsEndOffset (int aEndOFfset) {
		endOffset = aEndOFfset;
		hasOffsets = true;
	}
		
	public IEnumerator OpenBackground () {
		currentTimeBackgroundOpacity = 0.0f;
		background.enabled = true;
		background.color = new Color (background.color.r, background.color.g, background.color.b, 0);

		while (background.color.a < maxBackgroundOpacity) {
			currentTimeBackgroundOpacity += Time.unscaledDeltaTime;
			Color l_color = background.color;
			l_color.a = Mathf.Lerp (0, maxBackgroundOpacity, currentTimeBackgroundOpacity / timeBackgroundOpacity);
			background.color = l_color;
			yield return null;
		}
	}


	public IEnumerator CloseBackground () {
		currentTimeBackgroundOpacity = timeBackgroundOpacity;
		background.enabled = true;
		background.color = new Color (background.color.r, background.color.g, background.color.b, maxBackgroundOpacity);

		while (background.color.a > 0) {
			currentTimeBackgroundOpacity -= Time.unscaledDeltaTime;
			Color l_color = background.color;
			l_color.a = Mathf.Lerp (0, maxBackgroundOpacity, currentTimeBackgroundOpacity / timeBackgroundOpacity);
			background.color = l_color;
			yield return null;
		}
		background.enabled = false;
	}

	public void ShowDialog (int aIndex) {
		menu.CloseMenu ();
		menu.accesible = false;
		//HideCurrentDialog ();
		StartCoroutine(OpenBackground());

		int l_numComments = dialogs [aIndex].transform.childCount;
		if (aIndex >= 0 && aIndex < numOfDialogs && l_numComments > 0) {
			PauseGameController.Pause ();
			currentDialog = dialogs [aIndex];
			currentDialog.SetActive (true);
			currentDialog.GetComponent<Animator>().Play("Show");

			if (!hasOffsets) {
				startOffset = 0;
				endOffset = l_numComments - 1;

				currentDialog.transform.GetChild (0).gameObject.SetActive (true);
				currentShowingComment = 0;
			} else {
				startOffset = Mathf.Clamp (startOffset, 0, l_numComments);
				endOffset = Mathf.Clamp (endOffset, startOffset, l_numComments);
				currentDialog.transform.GetChild (startOffset + 1).gameObject.SetActive (true);
				currentShowingComment = startOffset;
			}

			currentShowingDialog = aIndex;
			showingDialogs = true;
		} else {
			Debug.LogWarning ("El diálogo " + aIndex + " no existe o no tiene comentarios");
		}
	}

	public void HideCurrentDialog () {
		if (currentShowingDialog >= 0) {
			currentDialog.GetComponent<Animator>().Play("Hide");
			StartCoroutine(CloseBackground());
		}
	}
		

	public void LoadDialogs() {
		for (int i = 0; i < dialogsData.dialogs.Count; i++) {

			DialogsData.DialogData l_dialogData = dialogsData.dialogs [i];
			GameObject l_dialog = Instantiate (commentsGroup, gameObject.transform);


			l_dialog.SetActive (false);

			for (int n = 0; n < l_dialogData.commentsData.Count; n++) {
				InGameMenu.MenuDialog.CommentType l_type = l_dialogData.commentsData [n].picturePosition;

				if (l_type == InGameMenu.MenuDialog.CommentType.NormalLeft || l_type == InGameMenu.MenuDialog.CommentType.NormalRight) {
					GameObject l_comment;

					if (l_type == InGameMenu.MenuDialog.CommentType.NormalLeft) {
						l_comment = Instantiate (commentLeft, l_dialog.transform);
					} else {
						l_comment = Instantiate (commentRight, l_dialog.transform);
					}

					InGameMenu_Comment l_commentScript = l_comment.GetComponent<InGameMenu_Comment> ();
					l_commentScript.Character = l_dialogData.commentsData [n].character;
					l_commentScript.Comment = l_dialogData.commentsData [n].comment;
					l_commentScript.SetSprite (l_dialogData.commentsData [n].picture);
                    l_commentScript.SoundEvent = l_dialogData.commentsData[n].soundEvent;
					l_comment.SetActive (false);
				}
			}
			numOfDialogs++;
			dialogs.Add (l_dialog);
		}
	}



}
