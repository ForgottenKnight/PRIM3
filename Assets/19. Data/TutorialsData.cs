﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(menuName = "TutorialsData")]
public class TutorialsData : ScriptableObject {

	public List<TutorialData> tutorials = new List<TutorialData>();

	[System.Serializable]
	public class TutorialData {
		public string name;
		public Sprite controlsSprite;
		public VideoClip tutorialVideo;

		[TextArea]
		public List<string> tutorialPagesData = new  List<string>();
	}
		
}
