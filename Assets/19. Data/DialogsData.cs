﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(menuName = "DialogsData")]
public class DialogsData : ScriptableObject {

	public List<DialogData> dialogs = new List<DialogData>();

	[System.Serializable]
	public class DialogData {
		public string name;

		public List<DialogCommentData> commentsData = new  List<DialogCommentData>();


		[System.Serializable]
		public class DialogCommentData {
			public string character;
			[TextArea]
			public string comment;
			public Sprite picture;
			public InGameMenu.MenuDialog.CommentType picturePosition;
			public string soundEvent;
		}
	}
		
}
