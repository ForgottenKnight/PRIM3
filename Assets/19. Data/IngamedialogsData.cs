﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(menuName = "Ingame dialog data")]
public class IngamedialogsData : ScriptableObject {

	public List<IngamedialogData> dialogs = new List<IngamedialogData>();

	[System.Serializable]
	public class IngamedialogData {
		public string name;
		[TextArea]
		public string comment;
		[SerializeField]
		public List<Dialog> dialogs;

		[System.Serializable]
		public class Dialog {
			public int character;
			[TextArea]
			public string dialog;
			public string soundEvent;
			public int unlockDialog = -1;
			public float duration = 2f;
			public bool dontWaitForNext;
			public Sprite dialogSprite;
		}
	}
		
}
