﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XboxCtrlrInput;
using System;
using AK.Wwise;
using UnityEngine.SceneManagement;

public class StatisticsController : MonoBehaviour {
	[Header("Configuration")]
	private GameObject charactersUI;
	public float timeToAppear;
	private bool showContinueButton = false;
	public bool readyToContinue;
	public InGameMenu menu;

	public GameObject continueButton;
	private Animator statisticsAnim;

	[Header("Sprites")]
	public Sprite mercury_avatar;
	public Sprite suphur_avatar;
	public Sprite salt_avatar;


	[Header("Players")]
	[HideInInspector]
	public Image[] pos1_image;
	[HideInInspector]
	public Image[] pos2_image;
	[HideInInspector]
	public Image[] pos3_image;

	public Text[] pos1_name;
	public Text[] pos2_name;
	public Text[] pos3_name;

	public Text[] pos1_percent;
	public Text[] pos2_percent;
	public Text[] pos3_percent;

	public GameObject[] stars;

	[Header("Daño causado")]
	public Text[] damage;

	[Header("Daño recibido")]
	public Text[] damage_received;

	[Header("Daño mitigado")]
	public Text[] damage_mitigated;

	[Header("Precision")]
	public Text[] precision;

	[Header("Energia consumida")]
	public Text[] energy_spent;

	[Header("Otras estadisticas")]
	public Text superAbilities_used;
	public Text gameDuration;

	[Header("Animation")]
	public GameObject[] playerMedals;
	public GameObject[] otherMedals;

    private string m_SceneName;

	// Use this for initialization
	void Start () {
		statisticsAnim = GetComponent<Animator> ();
		charactersUI = GameObject.Find ("UI Characters");

		for (int i = 0; i < playerMedals.Length; i++) {
			playerMedals [i].SetActive (false);
		}
		for (int i = 0; i < otherMedals.Length; i++) {
			otherMedals [i].SetActive (false);
		}


		for (int i = 0; i < StaticParemeters.numPlayers; i++) {
			playerMedals [i].SetActive (false);
		}

        Scene l_Scene = SceneManager.GetActiveScene();
        m_SceneName = l_Scene.name;
	}
	
	// Update is called once per frame
	void Update () {
		StaticStatistics.timer += Time.deltaTime;

		if (showContinueButton) {
			if (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.Space) || XCI.GetButtonDown (XboxButton.A)) {
				StaticStatistics.nextLevel = true;               

				//Pausa partida
				PauseGameController.Resume ();
			}
		}
	}

	public void ShowContinue() {
		showContinueButton = true;
		continueButton.SetActive (true);
	}

	public void ShowStatistics() {
		//Cierre forzoso de menu
		menu.CloseMenu ();
		menu.accesible = false;

		//Pausa partida
		PauseGameController.Pause ();

		//Gestiona estadisticas
		statisticsAnim.Play ("Appear");
		RecalculatePlayers ();
		LoadStatistics ();
		StartCoroutine (ShowStatisticsCoroutine ());
	}


	//Esconde o muestra los elementos de la UI en funcion del número de jugadores
	private void RecalculatePlayers() {
		for (int i = 0; i < playerMedals.Length; i++) {
			playerMedals [i].transform.parent.gameObject.SetActive (false);
			damage [i].transform.parent.gameObject.SetActive (false);
			damage_received [i].transform.parent.gameObject.SetActive (false);
			damage_mitigated [i].transform.parent.gameObject.SetActive (false);
			energy_spent [i].transform.parent.gameObject.SetActive (false);
			precision [i].transform.parent.gameObject.SetActive (false);

		}
		for (int i = 0; i < StaticParemeters.numPlayers; i++) {
			playerMedals [i].transform.parent.gameObject.SetActive (true);
			damage [i].transform.parent.gameObject.SetActive (true);
			damage_received [i].transform.parent.gameObject.SetActive (true);
			damage_mitigated [i].transform.parent.gameObject.SetActive (true);
			energy_spent [i].transform.parent.gameObject.SetActive (true);
			precision [i].transform.parent.gameObject.SetActive (true);
		}
	}

	private void LoadStatistics() {
		for (int i = 0; i < StaticParemeters.numPlayers; i++) {
			damage[i].text = StaticStatistics.damageDone [i].ToString ("F0");
			damage_received[i].text = StaticStatistics.damageRecieved [i].ToString ("F0");
			damage_mitigated[i].text = StaticStatistics.damageAvoided [i].ToString ("F0");
			energy_spent[i].text = StaticStatistics.energySpent [i].ToString ("F0");

			if ((StaticStatistics.attackHit[i] + StaticStatistics.attackMissed[i]) > 0)
			{
				precision[i].text = (StaticStatistics.attackHit[i] / (StaticStatistics.attackHit[i] + StaticStatistics.attackMissed[i]) * 100f).ToString("F0") + "%";
			} else{
				precision[i].text = "0%";
			}

			//Obtiene y ordena los porcentages de uso de cada personaje
			float l_totalTime = StaticStatistics.useTime [i, 0] + StaticStatistics.useTime [i, 1] + StaticStatistics.useTime [i, 2];

			List<KeyValuePair<string, float>> l_totalCharList = new List<KeyValuePair<string, float>>();

			KeyValuePair<string, float> l_totalCharSalt = new KeyValuePair<string, float> ("Salt", Mathf.Round (StaticStatistics.useTime [i, 0] / l_totalTime * 100));
			KeyValuePair<string, float> l_totalCharSulphur = new KeyValuePair<string, float> ("Sulphur", Mathf.Round (StaticStatistics.useTime [i, 1] / l_totalTime * 100));
			KeyValuePair<string, float> l_totalCharMercury= new KeyValuePair<string, float> ("Mercury", Mathf.Round (StaticStatistics.useTime [i, 2] / l_totalTime * 100));

			l_totalCharList.Add (l_totalCharSalt);
			l_totalCharList.Add (l_totalCharSulphur);
			l_totalCharList.Add (l_totalCharMercury);
			l_totalCharList.Sort ((x, y) => x.Value.CompareTo (y.Value));

			pos1_percent [i].text = l_totalCharList [2].Value.ToString() + "%";
			pos1_name [i].text = l_totalCharList [2].Key;

			pos2_percent [i].text = l_totalCharList [1].Value.ToString() + "%";
			pos2_name [i].text = l_totalCharList [1].Key;

			pos3_percent [i].text = l_totalCharList [0].Value.ToString()+ "%";
			pos3_name [i].text = l_totalCharList [0].Key;

				
		}
			
		string minutes = Mathf.Floor(StaticStatistics.timer / 60).ToString("00");
		string seconds = (StaticStatistics.timer % 60).ToString("00");

		gameDuration.text = minutes + ":" + seconds;
	}
		
	private IEnumerator ShowStatisticsCoroutine() {
		charactersUI.SetActive (false);


		for (int i = 0; i < StaticParemeters.numPlayers; i++) {
			playerMedals [i].SetActive (true);
			yield return new WaitForSecondsRealtime (timeToAppear);
		}

		for (int i = 0; i < otherMedals.Length; i++) {
			otherMedals [i].SetActive (true);
			yield return new WaitForSecondsRealtime (timeToAppear);
		}
	}
}
